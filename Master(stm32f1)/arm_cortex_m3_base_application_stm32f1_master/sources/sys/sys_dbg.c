/**
 ******************************************************************************
 * @Author: ThanNT
 * @Date:   05/09/2016
 ******************************************************************************
**/
#include "ak.h"

#include "app_flash.h"

#include "utils.h"

#include "sys_dbg.h"
#include "sys_ctrl.h"
#include "sys_irq.h"
#include "sys_svc.h"

#if defined(STM32L_PLATFORM)
#include "xprintf.h"
#include "io_cfg.h"
#include "sys_cfg.h"
#elif defined(STM32F10X_PLATFORM)
#include "xprintf.h"
#include "io_cfg.h"
#include "sys_cfg.h"
#else
#error Please choose platform for kernel.
#endif

#define DUMP_RAM_UNIT_SIZE			256

#if defined(NON_CLEAR_RAM_FATAL_LOG)

#else
static fatal_log_t fatal_log;
static exception_info_t t_exception_info;
#endif

void sys_dbg_fatal(const int8_t* s, uint8_t c) {
	unsigned char rev_c = 0;
	task_t*		ptemp_current_task;
	ak_msg_t*	ptemp_current_active_object;

#if defined(TIVA_PLATFORM)
	UARTprintf("%s\t%x\n", s, c);
#endif

#if defined(STM32L_PLATFORM) || defined(STM32F10X_PLATFORM)
	xprintf("%s\t%x\n", s, c);
#endif

#if defined(NON_CLEAR_RAM_FATAL_LOG)
	non_clear_ram_fatal_log.fatal_times ++;

	/* set fatal string */
	mem_set(non_clear_ram_fatal_log.string, 0, 10);
	str_cpy(non_clear_ram_fatal_log.string, s);

	/* set fatal code */
	non_clear_ram_fatal_log.code = c;

	/* get task fatal */
	ptemp_current_task = get_current_task_info();
	ptemp_current_task->id = get_current_task_id();

	/* get active object fatal */
	ptemp_current_active_object = get_current_active_object();

	/* get core register */
	non_clear_ram_fatal_log.m3_core_reg.ipsr		= __get_IPSR();
	non_clear_ram_fatal_log.m3_core_reg.primask	= __get_PRIMASK();
	non_clear_ram_fatal_log.m3_core_reg.faultmask	= __get_FAULTMASK();
	non_clear_ram_fatal_log.m3_core_reg.basepri	= __get_BASEPRI();
	non_clear_ram_fatal_log.m3_core_reg.control	= __get_CONTROL();

	mem_cpy(&non_clear_ram_fatal_log.current_task, ptemp_current_task, sizeof(task_t));
	mem_cpy(&non_clear_ram_fatal_log.current_active_object, ptemp_current_active_object, sizeof(ak_msg_t));
#else
	task_exit_interrupt();

	uint32_t	flash_sys_log_address = APP_FLASH_DBG_SECTOR_1;
	uint32_t	flash_irq_log_address = APP_FLASH_FATAL_IRQ_LOG_SECTOR;
	ak_msg_t	t_msg;

	/* read fatal data from flash */
	flash_read(APP_FLASH_FATAL_LOG_SECTOR, (uint8_t*)&fatal_log, sizeof(fatal_log_t));

	/* increase fatal time */
	fatal_log.fatal_times ++;

	/* set fatal string */
	mem_set(fatal_log.string, 0, 10);
	str_cpy(fatal_log.string, s);

	/* set fatal code */
	fatal_log.code = c;

	/* get task fatal */
	ptemp_current_task = get_current_task_info();

	/* get active object fatal */
	ptemp_current_active_object = get_current_active_object();

	mem_cpy(&fatal_log.current_task, ptemp_current_task, sizeof(task_t));
	mem_cpy(&fatal_log.current_active_object, ptemp_current_active_object, sizeof(ak_msg_t));

	/* flash_write fatal data to epprom */
	flash_erase_sector(APP_FLASH_FATAL_LOG_SECTOR);
	flash_write(APP_FLASH_FATAL_LOG_SECTOR, (uint8_t*)&fatal_log, sizeof(fatal_log_t));

	SYS_PRINT("start write irq info\n");
	flash_erase_sector(flash_irq_log_address);

	while(log_queue_len(&log_irq_queue)) {
		log_queue_get(&log_irq_queue, &t_exception_info);
		flash_write(flash_irq_log_address, (uint8_t*)&t_exception_info, sizeof(exception_info_t));
		flash_irq_log_address += sizeof(exception_info_t);
	}


	SYS_PRINT("start write system log to flash\n");
	flash_erase_sector(flash_sys_log_address);

	while(log_queue_len(&log_task_dbg_object_queue)) {
		log_queue_get(&log_task_dbg_object_queue, &t_msg);
		flash_write(flash_sys_log_address, (uint8_t*)&t_msg, sizeof(ak_msg_t));
		flash_sys_log_address += sizeof(ak_msg_t);

		xprintf("task_id: %d\tmsg_type:0x%x\tref_count:%d\tsig:%d\t\twait_time:%d\texe_time:%d\n"\
				, t_msg.des_task_id							\
				, (t_msg.ref_count & AK_MSG_TYPE_MASK)		\
				, (t_msg.ref_count & AK_MSG_REF_COUNT_MASK)	\
				, t_msg.sig									\
				, 10	\
				, 10);
	}
#endif

	sys_ctrl_delay_us(1000);

#if defined(RELEASE)
	sys_ctrl_reset();
#endif

	while(1) {
		/* reset watchdog */
		sys_ctrl_independent_watchdog_reset();
		sys_ctrl_soft_watchdog_reset();

		/* FATAL debug option */
		rev_c = sys_ctrl_shell_get_char();
		if (rev_c) {
			switch (rev_c) {
			case 'r':
				sys_ctrl_reset();
				break;

			default:
				break;
			}
		}

		/* led notify FATAL */
		led_life_on();
		sys_ctrl_delay_us(200000);
		led_life_off();
		sys_ctrl_delay_us(200000);
	}
}
