#ifndef __SYS_IRQ_H__
#define __SYS_IRQ_H__

#ifdef __cplusplus
extern "C"
{
#endif
#include <stdint.h>
#include "app.h"
#define SYS_IRQ_EXCEPTION_NUMBER_IRQ0_NUMBER_RESPECTIVE		16	/* exception number 16 ~~ IRQ0 */

extern void sys_irq_shell();
extern void sys_irq_timer_10ms();
extern void sys_irq_uart_rs485();
extern void sys_irq_uart_cpu_serial_if();
extern void sys_irq_uart_zigbee();
extern void tx_frame_post(uint8_t*, uint8_t);
#ifdef __cplusplus
}
#endif

#endif // __SYS_IRQ_H__
