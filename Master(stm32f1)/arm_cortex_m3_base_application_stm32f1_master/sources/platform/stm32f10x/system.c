/**
 ******************************************************************************
 * @Author: ThanNT
 * @Date:   05/09/2016
 ******************************************************************************
**/
#include <stdint.h>
#include <stdbool.h>

#include "ak.h"
#include "message.h"
#include "timer.h"


#include "app.h"

#include "sys_cfg.h"
#include "system.h"
#include "platform.h"

#include "system_stm32f10x.h"
#include "stm32f10x.h"
#include "stm32f10x_conf.h"
#include "core_cm3.h"

#include "sys_dbg.h"
#include "sys_ctrl.h"
#include "sys_irq.h"
#include "io_cfg.h"

#include "task_life.h"
#include "task_encoder_pid.h"
#include "xprintf.h"

/*****************************************************************************/
/* linker variable                                                           */
/*****************************************************************************/
extern uint32_t _ldata;
extern uint32_t _data;
extern uint32_t _edata;
extern uint32_t _bss;
extern uint32_t _ebss;
extern uint32_t _estack;

extern void (*__preinit_array_start[])();
extern void (*__preinit_array_end[])();
extern void (*__init_array_start[])();
extern void (*__init_array_end[])();
extern void _init();

/*****************************************************************************/
/* static function prototype                                                 */
/*****************************************************************************/
/*****************************/
/* system interrupt function */
/*****************************/
void default_handler();
void reset_handler();

/*****************************/
/* user interrupt function   */
/*****************************/
void timer2_irq(void);		/*interrupt timer*/
void timer4_irq(void);		/* watchdog timer*/
void uart3_irq(void);		/* UART rs485 interrupt */
void uart2_irq(void);		/* UART interface cpu interrupt */

/* cortex-M processor fault exceptions */
void nmi_handler()          __attribute__ ((weak));
void hard_fault_handler()   __attribute__ ((weak));
void mem_manage_handler()   __attribute__ ((weak));
void bus_fault_handler()    __attribute__ ((weak));
void usage_fault_handler()  __attribute__ ((weak));

/* cortex-M processor non-fault exceptions */
void svc_handler()          __attribute__ ((weak, alias("default_handler")));
void dg_monitor_handler()   __attribute__ ((weak, alias("default_handler")));
void pendsv_handler()       __attribute__ ((weak, alias("default_handler")));
void systick_handler();

/* external interrupts */
static void shell_handler();

/*****************************************************************************/
/* system variable                                                           */
/*****************************************************************************/
system_info_t system_info;

/*****************************************************************************/
/* interrupt vector table                                                    */
/*****************************************************************************/
__attribute__((section(".isr_vector")))
void (* const isr_vector[])() = {
		((void (*)())(uint32_t)&_estack)	,	//	The initial stack pointer
		reset_handler						,	//	The reset handler
		nmi_handler							,	//	The NMI handler
		hard_fault_handler					,	//	The hard fault handler
		mem_manage_handler					,	//	The MPU fault handler
		bus_fault_handler					,	//	The bus fault handler
		usage_fault_handler					,	//	The usage fault handler
		0									,	//	Reserved
		0									,	//	Reserved
		0									,	//	Reserved
		0									,	//	Reserved
		svc_handler							,	//	SVCall handler
		dg_monitor_handler					,	//	Debug monitor handler
		0									,	//	Reserved
		pendsv_handler						,	//	The PendSV handler
		systick_handler						,	//	The SysTick handler

		//External Interrupts
		default_handler						,	//	Window Watchdog
		default_handler						,	//	PVD through EXTI Line detect
		default_handler						,	//	Tamper
		default_handler						,	//	RTC
		default_handler						,	//	Flash
		default_handler						,	//	RCC
		default_handler						,	//	EXTI Line 0
		default_handler						,	//	EXTI Line 1
		default_handler						,	//	EXTI Line 2
		default_handler						,	//	EXTI Line 3
		default_handler						,	//	EXTI Line 4
		default_handler						,	//	DMA1 Channel 1
		default_handler						,	//	DMA1 Channel 2
		default_handler						,	//	DMA1 Channel 3
		default_handler						,	//	DMA1 Channel 4
		default_handler						,	//	DMA1 Channel 5
		default_handler						,	//	DMA1 Channel 6
		default_handler						,	//	DMA1 Channel 7
		default_handler						,	//	ADC1_2
		default_handler						,	//	USB High Priority or CAN1 TX
		default_handler						,	//	USB Low  Priority or CAN1 RX0
		default_handler						,	//	CAN1 RX1
		default_handler						,	//	CAN1 SCE
		default_handler						,	//	EXTI Line 9..5
		default_handler						,	//	TIM1 Break
		default_handler						,	//	TIM1 Update
		default_handler						,	//	TIM1 Trigger and Commutation
		default_handler						,	//	TIM1 Capture Compare
		default_handler						,	//	TIM2
		default_handler						,	//	TIM3
		timer4_irq							,	//	TIM4
		default_handler						,	//	I2C1 Event
		default_handler						,	//	I2C1 Error
		default_handler						,	//	I2C2 Event
		default_handler						,	//	I2C2 Error
		default_handler						,	//	SPI1
		default_handler						,	//	SPI2
		shell_handler						,	//	USART1
		uart2_irq							,	//	USART2
		uart3_irq							,	//	USART3
		default_handler						,	//	EXTI Line 15..10
		default_handler						,	//	RTC Alarm through EXTI Line
		default_handler						,	//	USB Wakeup from suspend
		};

void __attribute__((naked))
sys_ctrl_delay(volatile uint32_t count)
{
	(void)count;

	__asm("    subs    r0, #1\n"
	"    bne     sys_ctrl_delay\n"
	"    bx      lr");
}

static uint32_t millis_current  = 0;

uint32_t sys_ctrl_millis() {
	volatile uint32_t ret;
	ENTRY_CRITICAL();
	ret = millis_current;
	EXIT_CRITICAL();
	return ret;
}

void _init() {
	/* dummy */
}

/*****************************************************************************/
/* static function defination                                                */
/*****************************************************************************/
void default_handler() {
	FATAL("SY", 0xEE);
}

GPIO_InitTypeDef GPIO_InitStructure;

void reset_handler() {
	__disable_irq(); /* */

	uint32_t *pSrc	= &_ldata;
	uint32_t *pDest	= &_data;
	volatile unsigned i, cnt;

	/* copy init data from FLASH to SRAM */
	while(pDest < &_edata) {
		*pDest++ = *pSrc++;
	}

	/* zero bss */
	for (pDest = &_bss; pDest < &_ebss; pDest++) {
		*pDest = 0UL;
	}

	ENTRY_CRITICAL();

	/* JTAG-DP Disabled and SW-DP Enabled */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
	GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE);

	/* init system */
	SystemInit();

	sys_cfg_clock();
	sys_cfg_tick();     /* system tick 1ms */
	sys_cfg_console();  /* system console */

	/* invoke all static constructors */
	cnt = __preinit_array_end - __preinit_array_start;
	for (i = 0; i < cnt; i++)
		__preinit_array_start[i]();

	_init();

	cnt = __init_array_end - __init_array_start;
	for (i = 0; i < cnt; i++)
		__init_array_start[i]();

	/* wait configuration stable */
	sys_ctrl_delay(100);  /* wait 300 cycles clock */

	/* update system information */
	sys_cfg_update_info();

	/* entry app function */
	main_app();
}

/***************************************/
/* cortex-M processor fault exceptions */
/***************************************/
void nmi_handler() {
	FATAL("SY", 0x01);
}

void hard_fault_handler() {
	FATAL("SY", 0x02);
}

void mem_manage_handler() {
	FATAL("SY", 0x03);
}

void bus_fault_handler() {
	FATAL("SY", 0x04);
}

void usage_fault_handler() {
	FATAL("SY", 0x05);
}

/*******************************************/
/* cortex-M processor non-fault exceptions */
/*******************************************/
void systick_handler() {
	task_entry_interrupt();

	static uint32_t div_counter = 0;

	/* increasing millis counter */
	millis_current++;

	if (div_counter == 10) {
		div_counter = 0;
	}

	switch(div_counter) {
	case 0:
		/* trigger heart beat of system */
		timer_tick(10);
		break;

	case 1:
		sys_irq_timer_10ms();
		break;

	default:
		break;
	}

	div_counter++;

	task_exit_interrupt();
}

float ploi, dloi, iloi, Last_Bias;

void PID_DIGITAL(__IO float* Save_PID_Feeback, uint16_t Target, uint16_t Current, float kp, float ki, float kd) {
	static int32_t Bias;
	__IO float Pwm;

	Bias = Target - Current;

	ploi = Bias;

	dloi = Bias - Last_Bias;

	iloi += Bias;

	iloi = constrain_cus(iloi, -30, 30);

	Last_Bias = Bias;

	Pwm = kp*ploi + ki*iloi/0.01 + kd*dloi*0.01;

	Pwm = constrain_cus(Pwm, -1000, 1000);

	*Save_PID_Feeback = Pwm ;
}

float r_k = 0;
float inte, deri;
float pre_error;
__IO float pid_feeback1, pid_feeback2, pid_feeback3;

void PID_DeltaRobot(__IO float* Save_PID_Feeback, float deta_run, float t_run, float current, float kp, float ki, float kd) {
	float error;
	float s;
	__IO float pwm;

	// Tính độ dốc cần đạt được
	s = (float)deta_run / t_run;

	/* r_k trong trường hợp này sẽ là setpoint và được cộng
	dồn qua nhiều lần tương ứng với số bậc thang cần đạt được của PID */
	r_k += (s  * 0.005);

	 if (r_k > deta_run) {
		r_k = deta_run;
	 }

	// PID Controller
	error = r_k - current;
	inte += error / 0.01;	// integral
	deri = (error - pre_error) * 0.01; // derivative

	inte = constrain_cus(inte, -50, 50);
	// Output
	pwm = (kp * error) + (ki * inte) + (kd * deri);
	pwm = constrain_cus(pwm, -1000, 1000);
	*Save_PID_Feeback = pwm;

	pre_error = error;

	xprintf("error: %d			", (int32_t)error);
	xprintf("r_k: %d			", (int32_t)r_k);
	xprintf("current: %d			", (int32_t)current);
	xprintf("Cnt_feedback1: %d			", (int32_t)enc_fb1.Pos_encoder_feedback);
	xprintf("pid_feeback1: %d			\n", (int32_t)pwm);
}

/************************/
/* external interrupts  */
/************************/
void timer2_irq() {
	task_entry_interrupt();

	if (TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET) {

		//PID_DIGITAL(&pid_feeback1, 10800, enc_fb1.Cnt_feedback, 0.2, 0, 10);
		PID_DeltaRobot(&pid_feeback1, degree, 1, enc_fb1.Pos_encoder_feedback, 2.5, 0.1, 0);


		TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
	}

	task_exit_interrupt();
}

void shell_handler(void) {
	task_entry_interrupt();

	if (USART_GetITStatus(USARTx, USART_IT_RXNE) == SET) {
		sys_irq_shell();
	}

	task_exit_interrupt();
}

void timer4_irq(void) {
	if (TIM_GetITStatus(TIM4, TIM_IT_Update) != RESET) {
		TIM_ClearITPendingBit(TIM4, TIM_IT_Update);
		sys_ctrl_soft_watchdog_increase_counter();
	}
}

void uart2_irq(void) {
	task_entry_interrupt();

	if (USART_GetITStatus(USART2, USART_IT_RXNE) == SET) {
		sys_irq_uart_zigbee();
	}

	task_exit_interrupt();
}

void uart3_irq(void) {
	task_entry_interrupt();

	if (USART_GetITStatus(USART3, USART_IT_RXNE) == SET) {
		sys_irq_uart_rs485();
	}

	task_exit_interrupt();
}

