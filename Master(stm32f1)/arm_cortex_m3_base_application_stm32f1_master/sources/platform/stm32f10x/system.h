/**
 ******************************************************************************
 * @Author: ThanNT
 * @Date:   05/09/2016
 ******************************************************************************
**/
#ifndef __SYSTEM_H__
#define __SYSTEM_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include "system_stm32f10x.h"
#include "stm32f10x.h"
#include "stm32f10x_conf.h"
#include "core_cm3.h"

typedef struct {
	uint32_t cpu_clock;
	uint32_t tick;
	uint32_t console_baudrate;
	uint32_t flash_used;
	uint32_t ram_used;
	uint32_t data_used;
	uint32_t stack_used;
	uint32_t heap_size;
} system_info_t;

extern system_info_t system_info;


#define min_cus(a, b)  ({  __typeof__ (a) _a = (a); __typeof__ (b) _b = (b); _a > _b ? _b : _a; })

#define max_cus(a, b) 	({  __typeof__ (a) _a = (a);	__typeof__ (b) _b = (b); _a > _b ? _a : _b; })

#define constrain_cus(x, lower_limit, upper_limit)   ({ __typeof__ (x) _x = (x); __typeof__ (lower_limit) _lower_limit = (lower_limit);  __typeof__ (upper_limit) _upper_limit = (upper_limit);  min_cus(max_cus(_x, _lower_limit), _upper_limit); })

extern __IO float pid_feeback1, pid_feeback2, pid_feeback3;

void PID_DeltaRobot(__IO float* Save_PID_Feeback, float deta_run, float t_run, float current, float kp, float ki, float kd);
void PID_DIGITAL(__IO float* Save_PID_Feeback, uint16_t Target, uint16_t Current, float kp, float ki, float kd);

#ifdef __cplusplus
}
#endif

#endif //__SYSTEM_H__
