#ifndef __KALMAN_H__
#define __KALMAN_H__

#include <stdint.h>

class KALMAN
{
public:
	void begin(float);
	uint16_t filter(uint16_t);

private:
	uint8_t counter;
	float A,H,Q,R,X,P;
	float value_return;
};

#endif //__KALMAN_H__
