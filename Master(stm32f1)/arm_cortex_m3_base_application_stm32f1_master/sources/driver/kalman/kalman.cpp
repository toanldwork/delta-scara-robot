#include "kalman.h"

void KALMAN::begin(float value_base) {
	A = 1;
	H = 1;
	Q = 0.32;
	R = 0.2514285714;
	X = value_base;

	counter = 1;
	value_return = 0;
}

uint16_t KALMAN::filter(uint16_t signal_in) {
	float XP,PP;
	float K;
	float temp_float;

	uint16_t ret = 0;

	if (counter == 1) {
		counter = 2;

		XP = A*X;
		PP = A*P*A + Q;

		K = PP*H;
		K /=(H*H*PP)+R;

		temp_float = (float)(signal_in-H*XP);
		value_return = XP + K*temp_float;

		temp_float = H*PP;
		P = PP-K*temp_float;
		ret = (uint16_t)value_return;
	}
	else {
		XP = A*value_return;
		PP = A*P*A + Q;

		K = PP*H;
		K /=(H*H*PP)+R;

		temp_float = (float)(signal_in-H*XP);
		value_return = XP + K*temp_float;

		temp_float = (float)H*PP;
		P = PP-K*temp_float;
		ret = (uint16_t)value_return;
	}

	return ret;
}
