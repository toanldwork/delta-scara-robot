#include "fsm.h"
#include "timer.h"
#include "port.h"
#include "message.h"

#include "sys_ctrl.h"
#include "sys_dbg.h"
#include "xprintf.h"

#include "app.h"
#include "app_dbg.h"

#include "task_list.h"
#include <math.h>

#include "io_cfg.h"
#include "task_encoder_pid.h"
#include "task_cpu_serial_if.h"
#include "system.h"
#include "sys_irq.h"
#include "task_life.h"


encoder_t enc_fb1, enc_fb2, enc_fb3 ;
float test = 0;
float speed = 0;
uint16_t Cnt_feedback_pre = 0;
uint16_t degree = 0;
uint8_t button_status[7] = {0, 0, 0, 0, 0, 0, 0};
uint8_t button_flat[7] = {0, 0, 0, 0, 0, 0, 0};

void task_encoder_pid(ak_msg_t* msg) {
	switch (msg->sig) {

	case SL_DELTA_BUTTON_REQ: {
		button_status[1] = read_button1_pin();
		button_status[2] = read_button2_pin();
		button_status[3] = read_button3_pin();
		button_status[4] = read_button4_pin();
		button_status[5] = read_button5_pin();
		button_status[6] = read_button6_pin();

		if (button_status[1] == 0) {
			if (button_flat[1] == 0) {
				xprintf("START_BUTTON\n");
				button_flat[1] = 1;
#if (CONCEPT_SCARA_ROBOT == 1)
				task_post_pure_msg(SL_TASK_ENCODER_PID_ID, SL_DELTA_WRITE_BROADCAST_CONCEPT1_REQ);
#elif (CONCEPT_DELTA_ROBOT == 1)
				task_post_pure_msg(SL_TASK_ENCODER_PID_ID, SL_DELTA_WRITE_BROADCAST_CONCEPT1_DELTA_REQ);
#endif
			}
		}
		else {
			if (button_flat[1] == 1) {
				button_flat[1] = 0;
			}
		}

		if (button_status[2] == 0) {
			if (button_flat[2] == 0) {
				xprintf("STOP_BUTTON\n");
				button_flat[2] = 1;
				cmd_brc_req(DELTA_ROBOT_IF_RESET_DATA);
			}
		}
		else {
			if (button_flat[2] == 1) {
				button_flat[2] = 0;
			}
		}

		if (button_status[3] == 0) {
			if (button_flat[3] == 0) {
				xprintf("LEFT_BUTTON\n");
				button_flat[3] = 1;
#if (CONCEPT_SCARA_ROBOT == 1)
				task_post_pure_msg(SL_TASK_ENCODER_PID_ID, SL_DELTA_WRITE_BROADCAST_CONCEPT7_REQ);
#elif (CONCEPT_DELTA_ROBOT == 1)
				task_post_pure_msg(SL_TASK_ENCODER_PID_ID, SL_DELTA_WRITE_BROADCAST_CONCEPT2_DELTA_REQ);
#endif
			}
		}
		else {
			if (button_flat[3] == 1) {
				button_flat[3] = 0;
			}
		}

		if (button_status[4] == 0) {
			if (button_flat[4] == 0) {
				xprintf("RIGHT_BUTTON\n");
				button_flat[4] = 1;
#if (CONCEPT_SCARA_ROBOT == 1)
				task_post_pure_msg(SL_TASK_ENCODER_PID_ID, SL_DELTA_WRITE_BROADCAST_CONCEPT8_REQ);
#elif (CONCEPT_DELTA_ROBOT == 1)
				task_post_pure_msg(SL_TASK_ENCODER_PID_ID, SL_DELTA_WRITE_BROADCAST_CONCEPT3_DELTA_REQ);
#endif
			}
		}
		else {
			if (button_flat[4] == 1) {
				button_flat[4] = 0;
			}
		}

		if (read_button5_pin() == 0) {
			if (button_flat[5] == 0) {
				xprintf("FORWARD_BUTTON\n");
				button_flat[5] = 1;
#if (CONCEPT_SCARA_ROBOT == 1)
				task_post_pure_msg(SL_TASK_ENCODER_PID_ID, SL_DELTA_WRITE_BROADCAST_CONCEPT9_REQ);
#elif (CONCEPT_DELTA_ROBOT == 1)
				task_post_pure_msg(SL_TASK_ENCODER_PID_ID, SL_DELTA_WRITE_BROADCAST_CONCEPT6_DELTA_REQ);
#endif
			}
		}
		else {
			if (button_flat[5] == 1) {
				button_flat[5] = 0;
			}
		}

		if (read_button6_pin() == 0) {
			if (button_flat[6] == 0) {
				xprintf("BACKWARD_BUTTON\n");
				button_flat[6] = 1;
#if (CONCEPT_SCARA_ROBOT == 1)
				task_post_pure_msg(SL_TASK_ENCODER_PID_ID, SL_DELTA_WRITE_BROADCAST_CONCEPT10_REQ);
#elif (CONCEPT_DELTA_ROBOT == 1)
				task_post_pure_msg(SL_TASK_ENCODER_PID_ID, SL_DELTA_WRITE_BROADCAST_CONCEPT7_DELTA_REQ);
#endif
			}
		}
		else {
			if (button_flat[6] == 1) {
				button_flat[6] = 0;
			}
		}
	}
		break;

		/*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*						WRITE BROADCAST							*
*						SCARA ROBOT								*
*						6 CONCEPT								*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*/

	case SL_DELTA_WRITE_BROADCAST_CONCEPT1_REQ: {
		APP_PRINT("SL_DELTA_WRITE_BROADCAST_CONCEPT1_REQ\n");
		delta_if_t delta_data;

		ak_msg_common_if_t app_if_msg;
		memset(&app_if_msg, 0, sizeof(ak_msg_common_if_t));
		memset(&delta_data, 0, sizeof(delta_if_t));

		delta_data.cmd = DELTA_ROBOT_IF_WRITE_BROADCAST;
		delta_data.len = 20;

		delta_data.data[0] = POS_DIR;	// Direction for Motor1
		delta_data.data[1] = 0;			// HIBYTE of Degree for Motor1
		delta_data.data[2] = 32;		// LOBYTE of Degree for Motor1
		delta_data.data[3] = 3;			// Setting time for Motor1
		delta_data.data[4] = 1;			// Division Setting time for Motor1

		delta_data.data[5] = POS_DIR;	// Direction for Motor2
		delta_data.data[6] = 0;			// HIBYTE of Degree for Motor2
		delta_data.data[7] = 0;		// LOBYTE of Degree for Motor2
		delta_data.data[8] = 5;			// Setting time for Motor2
		delta_data.data[9] = 1;			// Division Setting time for Motor2

		delta_data.data[10]	= NEG_DIR;	// Direction for Motor3
		delta_data.data[11]	= 0;		// HIBYTE of Degree for Motor3
		delta_data.data[12] = 78;		// LOBYTE of Degree for Motor3
		delta_data.data[13] = 3;		// Setting time for Motor3
		delta_data.data[14] = 1;		// Division Setting time for Motor3

		delta_data.data[15]	= NEG_DIR;	// Direction for Motor4
		delta_data.data[16] = 0;		// HIBYTE of Degree for Motor4
		delta_data.data[17] = 0;		// LOBYTE of Degree for Motor4
		delta_data.data[18] = 1;		// Setting time for Motor4
		delta_data.data[19] = 1;		// Division Setting time for Motor4

		/* assign if message */
		app_if_msg.header.type			= 0xC0;
		app_if_msg.header.if_src_type	= 1;
		app_if_msg.header.if_des_type	= 1;
		app_if_msg.header.sig			= DELTA_ROBOT_IF_WRITE_BROADCAST;
		app_if_msg.header.src_task_id	= 1;
		app_if_msg.header.des_task_id	= SL_TASK_ENCODER_PID_ID;

		app_if_msg.len = sizeof(delta_if_t);
		memcpy((uint8_t*)&app_if_msg.data[0], (uint8_t*)&delta_data, app_if_msg.len);
		tx_frame_post((uint8_t*)&app_if_msg, sizeof(ak_msg_common_if_t));

		sys_ctrl_delay_ms(500);
		cmd_brc_req(DELTA_ROBOT_IF_START_BROADCAST);
		timer_set(SL_TASK_ENCODER_PID_ID, SL_DELTA_RESET_CONCEPT1_DATA_REQ, 7000, TIMER_ONE_SHOT);
	}
		break;

	case SL_DELTA_WRITE_BROADCAST_CONCEPT2_REQ: {
		APP_PRINT("SL_DELTA_WRITE_BROADCAST_CONCEPT2_REQ\n");
		delta_if_t delta_data;

		ak_msg_common_if_t app_if_msg;
		memset(&app_if_msg, 0, sizeof(ak_msg_common_if_t));
		memset(&delta_data, 0, sizeof(delta_if_t));

		delta_data.cmd = DELTA_ROBOT_IF_WRITE_BROADCAST;
		delta_data.len = 20;

		delta_data.data[0] = NEG_DIR;	// Direction for Motor1
		delta_data.data[1] = 1;			// HIBYTE of Degree for Motor1
		delta_data.data[2] = 68;		// LOBYTE of Degree for Motor1
		delta_data.data[3] = 1;			// Setting time for Motor1
		delta_data.data[4] = 1;			// Division Setting time for Motor1

		delta_data.data[5] = NEG_DIR;	// Direction for Motor2
		delta_data.data[6] = 0;			// HIBYTE of Degree for Motor2
		delta_data.data[7] = 150;			// LOBYTE of Degree for Motor2
		delta_data.data[8] = 5;			// Setting time for Motor2
		delta_data.data[9] = 1;			// Division Setting time for Motor2

		delta_data.data[10]	= POS_DIR;	// Direction for Motor3
		delta_data.data[11]	= 0;		// HIBYTE of Degree for Motor3
		delta_data.data[12] = 100;		// LOBYTE of Degree for Motor3
		delta_data.data[13] = 5;		// Setting time for Motor3
		delta_data.data[14] = 1;		// Division Setting time for Motor3

		delta_data.data[15]	= POS_DIR;	// Direction for Motor4
		delta_data.data[16] = 28;		// HIBYTE of Degree for Motor4
		delta_data.data[17] = 32;		// LOBYTE of Degree for Motor4
		delta_data.data[18] = 1;		// Setting time for Motor4
		delta_data.data[19] = 1;		// Division Setting time for Motor4

		/* assign if message */
		app_if_msg.header.type			= 0xC0;
		app_if_msg.header.if_src_type	= 1;
		app_if_msg.header.if_des_type	= 1;
		app_if_msg.header.sig			= DELTA_ROBOT_IF_WRITE_BROADCAST;
		app_if_msg.header.src_task_id	= 1;
		app_if_msg.header.des_task_id	= SL_TASK_ENCODER_PID_ID;

		app_if_msg.len = sizeof(delta_if_t);
		memcpy((uint8_t*)&app_if_msg.data[0], (uint8_t*)&delta_data, app_if_msg.len);
		tx_frame_post((uint8_t*)&app_if_msg, sizeof(ak_msg_common_if_t));

		sys_ctrl_delay_ms(500);
		cmd_brc_req(DELTA_ROBOT_IF_START_BROADCAST);
		timer_set(SL_TASK_ENCODER_PID_ID, SL_DELTA_RESET_CONCEPT2_DATA_REQ, 7000, TIMER_ONE_SHOT);
	}
		break;

	case SL_DELTA_WRITE_BROADCAST_CONCEPT3_REQ: {
		APP_PRINT("SL_DELTA_WRITE_BROADCAST_CONCEPT3_REQ\n");
		delta_if_t delta_data;

		ak_msg_common_if_t app_if_msg;
		memset(&app_if_msg, 0, sizeof(ak_msg_common_if_t));
		memset(&delta_data, 0, sizeof(delta_if_t));

		delta_data.cmd = DELTA_ROBOT_IF_WRITE_BROADCAST;
		delta_data.len = 20;

		delta_data.data[0] = POS_DIR;	// Direction for Motor1
		delta_data.data[1] = 1;			// HIBYTE of Degree for Motor1
		delta_data.data[2] = 68;			// LOBYTE of Degree for Motor1
		delta_data.data[3] = 1;			// Setting time for Motor1
		delta_data.data[4] = 1;			// Division Setting time for Motor1

		delta_data.data[5] = POS_DIR;	// Direction for Motor2
		delta_data.data[6] = 0;			// HIBYTE of Degree for Motor2
		delta_data.data[7] = 60;		// LOBYTE of Degree for Motor2
		delta_data.data[8] = 2;			// Setting time for Motor2
		delta_data.data[9] = 1;			// Division Setting time for Motor2

		delta_data.data[10]	= NEG_DIR;	// Direction for Motor3
		delta_data.data[11]	= 0;		// HIBYTE of Degree for Motor3
		delta_data.data[12] = 60;		// LOBYTE of Degree for Motor3
		delta_data.data[13] = 2;		// Setting time for Motor3
		delta_data.data[14] = 1;		// Division Setting time for Motor3

		delta_data.data[15]	= NEG_DIR;	// Direction for Motor4
		delta_data.data[16] = 10;		// HIBYTE of Degree for Motor4
		delta_data.data[17] = 240;		// LOBYTE of Degree for Motor4
		delta_data.data[18] = 1;		// Setting time for Motor4
		delta_data.data[19] = 1;		// Division Setting time for Motor4

		/* assign if message */
		app_if_msg.header.type			= 0xC0;
		app_if_msg.header.if_src_type	= 1;
		app_if_msg.header.if_des_type	= 1;
		app_if_msg.header.sig			= DELTA_ROBOT_IF_WRITE_BROADCAST;
		app_if_msg.header.src_task_id	= 1;
		app_if_msg.header.des_task_id	= SL_TASK_ENCODER_PID_ID;

		app_if_msg.len = sizeof(delta_if_t);
		memcpy((uint8_t*)&app_if_msg.data[0], (uint8_t*)&delta_data, app_if_msg.len);
		tx_frame_post((uint8_t*)&app_if_msg, sizeof(ak_msg_common_if_t));

		sys_ctrl_delay_ms(500);
		cmd_brc_req(DELTA_ROBOT_IF_START_BROADCAST);
		timer_set(SL_TASK_ENCODER_PID_ID, SL_DELTA_RESET_CONCEPT3_DATA_REQ, 4000, TIMER_ONE_SHOT);
	}
		break;

	case SL_DELTA_WRITE_BROADCAST_CONCEPT4_REQ: {
		APP_PRINT("SL_DELTA_WRITE_BROADCAST_CONCEPT4_REQ\n");
		delta_if_t delta_data;

		ak_msg_common_if_t app_if_msg;
		memset(&app_if_msg, 0, sizeof(ak_msg_common_if_t));
		memset(&delta_data, 0, sizeof(delta_if_t));

		delta_data.cmd = DELTA_ROBOT_IF_WRITE_BROADCAST;
		delta_data.len = 20;

		delta_data.data[0] = NEG_DIR;	// Direction for Motor1
		delta_data.data[1] = 1;			// HIBYTE of Degree for Motor1
		delta_data.data[2] = 68;			// LOBYTE of Degree for Motor1
		delta_data.data[3] = 1;			// Setting time for Motor1
		delta_data.data[4] = 1;			// Division Setting time for Motor1

		delta_data.data[5] = NEG_DIR;	// Direction for Motor2
		delta_data.data[6] = 0;			// HIBYTE of Degree for Motor2
		delta_data.data[7] = 60;			// LOBYTE of Degree for Motor2
		delta_data.data[8] = 2;			// Setting time for Motor2
		delta_data.data[9] = 1;			// Division Setting time for Motor2

		delta_data.data[10]	= POS_DIR;	// Direction for Motor3
		delta_data.data[11]	= 0;		// HIBYTE of Degree for Motor3
		delta_data.data[12] = 60;		// LOBYTE of Degree for Motor3
		delta_data.data[13] = 2;		// Setting time for Motor3
		delta_data.data[14] = 1;		// Division Setting time for Motor3

		delta_data.data[15]	= POS_DIR;	// Direction for Motor4
		delta_data.data[16] = 10;		// HIBYTE of Degree for Motor4
		delta_data.data[17] = 240;		// LOBYTE of Degree for Motor4
		delta_data.data[18] = 1;		// Setting time for Motor4
		delta_data.data[19] = 1;		// Division Setting time for Motor4

		/* assign if message */
		app_if_msg.header.type			= 0xC0;
		app_if_msg.header.if_src_type	= 1;
		app_if_msg.header.if_des_type	= 1;
		app_if_msg.header.sig			= DELTA_ROBOT_IF_WRITE_BROADCAST;
		app_if_msg.header.src_task_id	= 1;
		app_if_msg.header.des_task_id	= SL_TASK_ENCODER_PID_ID;

		app_if_msg.len = sizeof(delta_if_t);
		memcpy((uint8_t*)&app_if_msg.data[0], (uint8_t*)&delta_data, app_if_msg.len);
		tx_frame_post((uint8_t*)&app_if_msg, sizeof(ak_msg_common_if_t));

		sys_ctrl_delay_ms(500);
		cmd_brc_req(DELTA_ROBOT_IF_START_BROADCAST);
		timer_set(SL_TASK_ENCODER_PID_ID, SL_DELTA_RESET_CONCEPT4_DATA_REQ, 4000, TIMER_ONE_SHOT);
	}
		break;
	case SL_DELTA_WRITE_BROADCAST_CONCEPT5_REQ: {
		APP_PRINT("SL_DELTA_WRITE_BROADCAST_CONCEPT5_REQ\n");
		delta_if_t delta_data;

		ak_msg_common_if_t app_if_msg;
		memset(&app_if_msg, 0, sizeof(ak_msg_common_if_t));
		memset(&delta_data, 0, sizeof(delta_if_t));

		delta_data.cmd = DELTA_ROBOT_IF_WRITE_BROADCAST;
		delta_data.len = 20;

		delta_data.data[0] = POS_DIR;	// Direction for Motor1
		delta_data.data[1] = 1;			// HIBYTE of Degree for Motor1
		delta_data.data[2] = 68;		// LOBYTE of Degree for Motor1
		delta_data.data[3] = 1;			// Setting time for Motor1
		delta_data.data[4] = 1;			// Division Setting time for Motor1

		delta_data.data[5] = NEG_DIR;	// Direction for Motor2
		delta_data.data[6] = 0;			// HIBYTE of Degree for Motor2
		delta_data.data[7] = 60;			// LOBYTE of Degree for Motor2
		delta_data.data[8] = 2;			// Setting time for Motor2
		delta_data.data[9] = 1;			// Division Setting time for Motor2

		delta_data.data[10]	= POS_DIR;	// Direction for Motor3
		delta_data.data[11]	= 0;		// HIBYTE of Degree for Motor3
		delta_data.data[12] = 60;		// LOBYTE of Degree for Motor3
		delta_data.data[13] = 2;		// Setting time for Motor3
		delta_data.data[14] = 1;		// Division Setting time for Motor3

		delta_data.data[15]	= POS_DIR;	// Direction for Motor4
		delta_data.data[16] = 10;		// HIBYTE of Degree for Motor4
		delta_data.data[17] = 240;		// LOBYTE of Degree for Motor4
		delta_data.data[18] = 1;		// Setting time for Motor4
		delta_data.data[19] = 1;		// Division Setting time for Motor4

		/* assign if message */
		app_if_msg.header.type			= 0xC0;
		app_if_msg.header.if_src_type	= 1;
		app_if_msg.header.if_des_type	= 1;
		app_if_msg.header.sig			= DELTA_ROBOT_IF_WRITE_BROADCAST;
		app_if_msg.header.src_task_id	= 1;
		app_if_msg.header.des_task_id	= SL_TASK_ENCODER_PID_ID;

		app_if_msg.len = sizeof(delta_if_t);
		memcpy((uint8_t*)&app_if_msg.data[0], (uint8_t*)&delta_data, app_if_msg.len);
		tx_frame_post((uint8_t*)&app_if_msg, sizeof(ak_msg_common_if_t));

		sys_ctrl_delay_ms(500);
		cmd_brc_req(DELTA_ROBOT_IF_START_BROADCAST);
		timer_set(SL_TASK_ENCODER_PID_ID, SL_DELTA_RESET_CONCEPT5_DATA_REQ, 4000, TIMER_ONE_SHOT);
	}
		break;

	case SL_DELTA_WRITE_BROADCAST_CONCEPT6_REQ: {
		APP_PRINT("SL_DELTA_WRITE_BROADCAST_CONCEPT6_REQ\n");
		delta_if_t delta_data;

		ak_msg_common_if_t app_if_msg;
		memset(&app_if_msg, 0, sizeof(ak_msg_common_if_t));
		memset(&delta_data, 0, sizeof(delta_if_t));

		delta_data.cmd = DELTA_ROBOT_IF_WRITE_BROADCAST;
		delta_data.len = 20;

		delta_data.data[0] = NEG_DIR;	// Direction for Motor1
		delta_data.data[1] = 1;			// HIBYTE of Degree for Motor1
		delta_data.data[2] = 68;		// LOBYTE of Degree for Motor1
		delta_data.data[3] = 1;			// Setting time for Motor1
		delta_data.data[4] = 1;			// Division Setting time for Motor1

		delta_data.data[5] = NEG_DIR;	// Direction for Motor2
		delta_data.data[6] = 0;			// HIBYTE of Degree for Motor2
		delta_data.data[7] = 60;			// LOBYTE of Degree for Motor2
		delta_data.data[8] = 2;			// Setting time for Motor2
		delta_data.data[9] = 1;			// Division Setting time for Motor2

		delta_data.data[10]	= POS_DIR;	// Direction for Motor3
		delta_data.data[11]	= 0;		// HIBYTE of Degree for Motor3
		delta_data.data[12] = 60;		// LOBYTE of Degree for Motor3
		delta_data.data[13] = 2;		// Setting time for Motor3
		delta_data.data[14] = 1;		// Division Setting time for Motor3

		delta_data.data[15]	= POS_DIR;	// Direction for Motor4
		delta_data.data[16] = 10;		// HIBYTE of Degree for Motor4
		delta_data.data[17] = 240;		// LOBYTE of Degree for Motor4
		delta_data.data[18] = 1;		// Setting time for Motor4
		delta_data.data[19] = 1;		// Division Setting time for Motor4

		/* assign if message */
		app_if_msg.header.type			= 0xC0;
		app_if_msg.header.if_src_type	= 1;
		app_if_msg.header.if_des_type	= 1;
		app_if_msg.header.sig			= DELTA_ROBOT_IF_WRITE_BROADCAST;
		app_if_msg.header.src_task_id	= 1;
		app_if_msg.header.des_task_id	= SL_TASK_ENCODER_PID_ID;

		app_if_msg.len = sizeof(delta_if_t);
		memcpy((uint8_t*)&app_if_msg.data[0], (uint8_t*)&delta_data, app_if_msg.len);
		tx_frame_post((uint8_t*)&app_if_msg, sizeof(ak_msg_common_if_t));

		sys_ctrl_delay_ms(500);
		cmd_brc_req(DELTA_ROBOT_IF_START_BROADCAST);
		timer_set(SL_TASK_ENCODER_PID_ID, SL_DELTA_RESET_CONCEPT6_DATA_REQ, 4000, TIMER_ONE_SHOT);
	}
		break;

		/*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
 *				END WRITE BROADCAST								*
 *					SCARA ROBOT									*
 *	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*/

	case SL_DELTA_WRITE_BROADCAST_CONCEPT7_REQ: {
		APP_PRINT("SL_DELTA_WRITE_BROADCAST_CONCEPT7_REQ\n");
		delta_if_t delta_data;

		ak_msg_common_if_t app_if_msg;
		memset(&app_if_msg, 0, sizeof(ak_msg_common_if_t));
		memset(&delta_data, 0, sizeof(delta_if_t));

		delta_data.cmd = DELTA_ROBOT_IF_WRITE_BROADCAST;
		delta_data.len = 20;

		delta_data.data[0] = POS_DIR;	// Direction for Motor1
		delta_data.data[1] = 0;			// HIBYTE of Degree for Motor1
		delta_data.data[2] = 0;		// LOBYTE of Degree for Motor1
		delta_data.data[3] = 2;			// Setting time for Motor1
		delta_data.data[4] = 1;			// Division Setting time for Motor1

		delta_data.data[5] = POS_DIR;	// Direction for Motor2
		delta_data.data[6] = 0;			// HIBYTE of Degree for Motor2
		delta_data.data[7] = 0;			// LOBYTE of Degree for Motor2
		delta_data.data[8] = 2;			// Setting time for Motor2
		delta_data.data[9] = 1;			// Division Setting time for Motor2

		delta_data.data[10]	= POS_DIR;	// Direction for Motor3
		delta_data.data[11]	= 0;		// HIBYTE of Degree for Motor3
		delta_data.data[12] = 0;		// LOBYTE of Degree for Motor3
		delta_data.data[13] = 2;		// Setting time for Motor3
		delta_data.data[14] = 1;		// Division Setting time for Motor3

		delta_data.data[15]	= POS_DIR;	// Direction for Motor4
		delta_data.data[16] = 14;		// HIBYTE of Degree for Motor4
		delta_data.data[17] = 16;		// LOBYTE of Degree for Motor4
		delta_data.data[18] = 3;		// Setting time for Motor4
		delta_data.data[19] = 1;		// Division Setting time for Motor4

		/* assign if message */
		app_if_msg.header.type			= 0xC0;
		app_if_msg.header.if_src_type	= 1;
		app_if_msg.header.if_des_type	= 1;
		app_if_msg.header.sig			= DELTA_ROBOT_IF_WRITE_BROADCAST;
		app_if_msg.header.src_task_id	= 1;
		app_if_msg.header.des_task_id	= SL_TASK_ENCODER_PID_ID;

		app_if_msg.len = sizeof(delta_if_t);
		memcpy((uint8_t*)&app_if_msg.data[0], (uint8_t*)&delta_data, app_if_msg.len);
		tx_frame_post((uint8_t*)&app_if_msg, sizeof(ak_msg_common_if_t));

		sys_ctrl_delay_ms(500);
		cmd_brc_req(DELTA_ROBOT_IF_START_BROADCAST);
		timer_set(SL_TASK_ENCODER_PID_ID, SL_DELTA_RESET_CONCEPT7_DATA_REQ, 7000, TIMER_ONE_SHOT);
	}
		break;

	case SL_DELTA_WRITE_BROADCAST_CONCEPT8_REQ: {
		APP_PRINT("SL_DELTA_WRITE_BROADCAST_CONCEPT8_REQ\n");
		delta_if_t delta_data;

		ak_msg_common_if_t app_if_msg;
		memset(&app_if_msg, 0, sizeof(ak_msg_common_if_t));
		memset(&delta_data, 0, sizeof(delta_if_t));

		delta_data.cmd = DELTA_ROBOT_IF_WRITE_BROADCAST;
		delta_data.len = 20;

		delta_data.data[0] = NEG_DIR;	// Direction for Motor1
		delta_data.data[1] = 0;			// HIBYTE of Degree for Motor1
		delta_data.data[2] = 0;		// LOBYTE of Degree for Motor1
		delta_data.data[3] = 2;			// Setting time for Motor1
		delta_data.data[4] = 1;			// Division Setting time for Motor1

		delta_data.data[5] = NEG_DIR;	// Direction for Motor2
		delta_data.data[6] = 0;			// HIBYTE of Degree for Motor2
		delta_data.data[7] = 0;			// LOBYTE of Degree for Motor2
		delta_data.data[8] = 2;			// Setting time for Motor2
		delta_data.data[9] = 1;			// Division Setting time for Motor2

		delta_data.data[10]	= NEG_DIR;	// Direction for Motor3
		delta_data.data[11]	= 0;		// HIBYTE of Degree for Motor3
		delta_data.data[12] = 0;		// LOBYTE of Degree for Motor3
		delta_data.data[13] = 2;		// Setting time for Motor3
		delta_data.data[14] = 1;		// Division Setting time for Motor3

		delta_data.data[15]	= NEG_DIR;	// Direction for Motor4
		delta_data.data[16] = 14;		// HIBYTE of Degree for Motor4
		delta_data.data[17] = 16;		// LOBYTE of Degree for Motor4
		delta_data.data[18] = 3;		// Setting time for Motor4
		delta_data.data[19] = 1;		// Division Setting time for Motor4

		/* assign if message */
		app_if_msg.header.type			= 0xC0;
		app_if_msg.header.if_src_type	= 1;
		app_if_msg.header.if_des_type	= 1;
		app_if_msg.header.sig			= DELTA_ROBOT_IF_WRITE_BROADCAST;
		app_if_msg.header.src_task_id	= 1;
		app_if_msg.header.des_task_id	= SL_TASK_ENCODER_PID_ID;

		app_if_msg.len = sizeof(delta_if_t);
		memcpy((uint8_t*)&app_if_msg.data[0], (uint8_t*)&delta_data, app_if_msg.len);
		tx_frame_post((uint8_t*)&app_if_msg, sizeof(ak_msg_common_if_t));

		sys_ctrl_delay_ms(500);
		cmd_brc_req(DELTA_ROBOT_IF_START_BROADCAST);
		timer_set(SL_TASK_ENCODER_PID_ID, SL_DELTA_RESET_CONCEPT7_DATA_REQ, 7000, TIMER_ONE_SHOT);
	}
		break;

	case SL_DELTA_WRITE_BROADCAST_CONCEPT9_REQ: {
		APP_PRINT("SL_DELTA_WRITE_BROADCAST_CONCEPT9_REQ\n");
		delta_if_t delta_data;

		ak_msg_common_if_t app_if_msg;
		memset(&app_if_msg, 0, sizeof(ak_msg_common_if_t));
		memset(&delta_data, 0, sizeof(delta_if_t));

		delta_data.cmd = DELTA_ROBOT_IF_WRITE_BROADCAST;
		delta_data.len = 20;

		delta_data.data[0] = POS_DIR;	// Direction for Motor1
		delta_data.data[1] = 0;			// HIBYTE of Degree for Motor1
		delta_data.data[2] = 32;		// LOBYTE of Degree for Motor1
		delta_data.data[3] = 3;			// Setting time for Motor1
		delta_data.data[4] = 1;			// Division Setting time for Motor1

		delta_data.data[5] = POS_DIR;	// Direction for Motor2
		delta_data.data[6] = 0;			// HIBYTE of Degree for Motor2
		delta_data.data[7] = 0;			// LOBYTE of Degree for Motor2
		delta_data.data[8] = 1;			// Setting time for Motor2
		delta_data.data[9] = 1;			// Division Setting time for Motor2

		delta_data.data[10]	= NEG_DIR;	// Direction for Motor3
		delta_data.data[11]	= 0;		// HIBYTE of Degree for Motor3
		delta_data.data[12] = 78;		// LOBYTE of Degree for Motor3
		delta_data.data[13] = 3;		// Setting time for Motor3
		delta_data.data[14] = 1;		// Division Setting time for Motor3

		delta_data.data[15]	= POS_DIR;	// Direction for Motor4
		delta_data.data[16] = 0;		// HIBYTE of Degree for Motor4
		delta_data.data[17] = 0;		// LOBYTE of Degree for Motor4
		delta_data.data[18] = 5;		// Setting time for Motor4
		delta_data.data[19] = 1;		// Division Setting time for Motor4

		/* assign if message */
		app_if_msg.header.type			= 0xC0;
		app_if_msg.header.if_src_type	= 1;
		app_if_msg.header.if_des_type	= 1;
		app_if_msg.header.sig			= DELTA_ROBOT_IF_WRITE_BROADCAST;
		app_if_msg.header.src_task_id	= 1;
		app_if_msg.header.des_task_id	= SL_TASK_ENCODER_PID_ID;

		app_if_msg.len = sizeof(delta_if_t);
		memcpy((uint8_t*)&app_if_msg.data[0], (uint8_t*)&delta_data, app_if_msg.len);
		tx_frame_post((uint8_t*)&app_if_msg, sizeof(ak_msg_common_if_t));

		sys_ctrl_delay_ms(500);
		cmd_brc_req(DELTA_ROBOT_IF_START_BROADCAST);
		//timer_set(SL_TASK_ENCODER_PID_ID, SL_DELTA_RESET_CONCEPT7_DATA_REQ, 4000, TIMER_ONE_SHOT);
	}
		break;

	case SL_DELTA_WRITE_BROADCAST_CONCEPT10_REQ: {
		APP_PRINT("SL_DELTA_WRITE_BROADCAST_CONCEPT10_REQ\n");
		delta_if_t delta_data;

		ak_msg_common_if_t app_if_msg;
		memset(&app_if_msg, 0, sizeof(ak_msg_common_if_t));
		memset(&delta_data, 0, sizeof(delta_if_t));

		delta_data.cmd = DELTA_ROBOT_IF_WRITE_BROADCAST;
		delta_data.len = 20;

		delta_data.data[0] = NEG_DIR;	// Direction for Motor1
		delta_data.data[1] = 0;			// HIBYTE of Degree for Motor1
		delta_data.data[2] = 32;		// LOBYTE of Degree for Motor1
		delta_data.data[3] = 3;			// Setting time for Motor1
		delta_data.data[4] = 1;			// Division Setting time for Motor1

		delta_data.data[5] = NEG_DIR;	// Direction for Motor2
		delta_data.data[6] = 0;			// HIBYTE of Degree for Motor2
		delta_data.data[7] = 0;			// LOBYTE of Degree for Motor2
		delta_data.data[8] = 1;			// Setting time for Motor2
		delta_data.data[9] = 1;			// Division Setting time for Motor2

		delta_data.data[10]	= POS_DIR;	// Direction for Motor3
		delta_data.data[11]	= 0;		// HIBYTE of Degree for Motor3
		delta_data.data[12] = 78;		// LOBYTE of Degree for Motor3
		delta_data.data[13] = 3;		// Setting time for Motor3
		delta_data.data[14] = 1;		// Division Setting time for Motor3

		delta_data.data[15]	= NEG_DIR;	// Direction for Motor4
		delta_data.data[16] = 0;		// HIBYTE of Degree for Motor4
		delta_data.data[17] = 0;		// LOBYTE of Degree for Motor4
		delta_data.data[18] = 5;		// Setting time for Motor4
		delta_data.data[19] = 1;		// Division Setting time for Motor4

		/* assign if message */
		app_if_msg.header.type			= 0xC0;
		app_if_msg.header.if_src_type	= 1;
		app_if_msg.header.if_des_type	= 1;
		app_if_msg.header.sig			= DELTA_ROBOT_IF_WRITE_BROADCAST;
		app_if_msg.header.src_task_id	= 1;
		app_if_msg.header.des_task_id	= SL_TASK_ENCODER_PID_ID;

		app_if_msg.len = sizeof(delta_if_t);
		memcpy((uint8_t*)&app_if_msg.data[0], (uint8_t*)&delta_data, app_if_msg.len);
		tx_frame_post((uint8_t*)&app_if_msg, sizeof(ak_msg_common_if_t));

		sys_ctrl_delay_ms(500);
		cmd_brc_req(DELTA_ROBOT_IF_START_BROADCAST);
		//timer_set(SL_TASK_ENCODER_PID_ID, SL_DELTA_RESET_CONCEPT7_DATA_REQ, 4000, TIMER_ONE_SHOT);
	}
		break;

/*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
 *									RESET CONCEPT										*
 *									SCARA ROBOT											*
 *	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*/
	case SL_DELTA_RESET_CONCEPT1_DATA_REQ: {
		APP_PRINT("SL_DELTA_RESET_CONCEPT1_DATA_REQ\n");
		cmd_brc_req(DELTA_ROBOT_IF_RESET_DATA);
		sys_ctrl_delay_ms(500);
		task_post_pure_msg(SL_TASK_ENCODER_PID_ID, SL_DELTA_WRITE_BROADCAST_CONCEPT2_REQ);
	}
		break;

	case SL_DELTA_RESET_CONCEPT2_DATA_REQ: {
		APP_PRINT("SL_DELTA_RESET_CONCEPT2_DATA_REQ\n");
		cmd_brc_req(DELTA_ROBOT_IF_RESET_DATA);
		sys_ctrl_delay_ms(500);
		task_post_pure_msg(SL_TASK_ENCODER_PID_ID, SL_DELTA_WRITE_BROADCAST_CONCEPT1_REQ);
	}
		break;

	case SL_DELTA_RESET_CONCEPT3_DATA_REQ: {
		APP_PRINT("SL_DELTA_RESET_CONCEPT3_DATA_REQ\n");
		cmd_brc_req(DELTA_ROBOT_IF_RESET_DATA);
		sys_ctrl_delay_ms(500);
		task_post_pure_msg(SL_TASK_ENCODER_PID_ID, SL_DELTA_WRITE_BROADCAST_CONCEPT4_REQ);
	}
		break;

	case SL_DELTA_RESET_CONCEPT4_DATA_REQ: {
		APP_PRINT("SL_DELTA_RESET_CONCEPT4_DATA_REQ\n");
		cmd_brc_req(DELTA_ROBOT_IF_RESET_DATA);
		sys_ctrl_delay_ms(500);
		task_post_pure_msg(SL_TASK_ENCODER_PID_ID, SL_DELTA_WRITE_BROADCAST_CONCEPT5_REQ);
	}
		break;

	case SL_DELTA_RESET_CONCEPT5_DATA_REQ: {
		APP_PRINT("SL_DELTA_RESET_CONCEPT5_DATA_REQ\n");
		cmd_brc_req(DELTA_ROBOT_IF_RESET_DATA);
		sys_ctrl_delay_ms(500);
		task_post_pure_msg(SL_TASK_ENCODER_PID_ID, SL_DELTA_WRITE_BROADCAST_CONCEPT6_REQ);
	}
		break;

	case SL_DELTA_RESET_CONCEPT6_DATA_REQ: {
		APP_PRINT("SL_DELTA_RESET_CONCEPT6_DATA_REQ\n");
		cmd_brc_req(DELTA_ROBOT_IF_RESET_DATA);
		sys_ctrl_delay_ms(500);
		task_post_pure_msg(SL_TASK_ENCODER_PID_ID, SL_DELTA_WRITE_BROADCAST_CONCEPT1_REQ);
	}
		break;

	case SL_DELTA_RESET_CONCEPT7_DATA_REQ: {
		APP_PRINT("SL_DELTA_RESET_CONCEPT7_DATA_REQ\n");
		cmd_brc_req(DELTA_ROBOT_IF_RESET_DATA);
		sys_ctrl_delay_ms(500);
	}
		break;
/*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
 *								END RESET CONCEPT										*
 *									SCARA ROBOT											*
 *	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*/

/*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
 *								WRITE BROADCAST											*
 *								DELTA ROBOT												*
 *								6 CONCEPT												*
 *	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*/

/*	DELTA: HOME --> A */
	case SL_DELTA_WRITE_BROADCAST_CONCEPT1_DELTA_REQ: {
		APP_PRINT("SL_DELTA_WRITE_BROADCAST_CONCEPT1_DELTA_REQ\n");
		delta_if_t delta_data;

		ak_msg_common_if_t app_if_msg;
		memset(&app_if_msg, 0, sizeof(ak_msg_common_if_t));
		memset(&delta_data, 0, sizeof(delta_if_t));

		delta_data.cmd = DELTA_ROBOT_IF_WRITE_BROADCAST;
		delta_data.len = 20;

		delta_data.data[0] = NEG_DIR;	// Direction for Motor1
		delta_data.data[1] = 0;			// HIBYTE of Degree for Motor1
		delta_data.data[2] = 23;		// LOBYTE of Degree for Motor1
		delta_data.data[3] = 1;			// Setting time for Motor1
		delta_data.data[4] = 1;			// Division Setting time for Motor1

		delta_data.data[5] = POS_DIR;	// Direction for Motor2
		delta_data.data[6] = 0;			// HIBYTE of Degree for Motor2
		delta_data.data[7] = 32;			// LOBYTE of Degree for Motor2
		delta_data.data[8] = 1;			// Setting time for Motor2
		delta_data.data[9] = 1;			// Division Setting time for Motor2

		delta_data.data[10]	= POS_DIR;	// Direction for Motor3
		delta_data.data[11]	= 0;		// HIBYTE of Degree for Motor3
		delta_data.data[12] = 32;		// LOBYTE of Degree for Motor3
		delta_data.data[13] = 1;		// Setting time for Motor3
		delta_data.data[14] = 1;		// Division Setting time for Motor3

		delta_data.data[15]	= NEG_DIR;	// Direction for Motor4
		delta_data.data[16] = 0;		// HIBYTE of Degree for Motor4
		delta_data.data[17] = 0;		// LOBYTE of Degree for Motor4
		delta_data.data[18] = 1;		// Setting time for Motor4
		delta_data.data[19] = 1;		// Division Setting time for Motor4

		/* assign if message */
		app_if_msg.header.type			= 0xC0;
		app_if_msg.header.if_src_type	= 1;
		app_if_msg.header.if_des_type	= 1;
		app_if_msg.header.sig			= DELTA_ROBOT_IF_WRITE_BROADCAST;
		app_if_msg.header.src_task_id	= 1;
		app_if_msg.header.des_task_id	= SL_TASK_ENCODER_PID_ID;

		app_if_msg.len = sizeof(delta_if_t);
		memcpy((uint8_t*)&app_if_msg.data[0], (uint8_t*)&delta_data, app_if_msg.len);
		tx_frame_post((uint8_t*)&app_if_msg, sizeof(ak_msg_common_if_t));

		sys_ctrl_delay_ms(300);
		cmd_brc_req(DELTA_ROBOT_IF_START_BROADCAST);
		//timer_set(SL_TASK_ENCODER_PID_ID, SL_DELTA_RESET_CONCEPT1_DATA_DELTA_REQ, 2500, TIMER_ONE_SHOT);
	}
		break;

/*	DELTA: A --> B */
	case SL_DELTA_WRITE_BROADCAST_CONCEPT2_DELTA_REQ: {
		APP_PRINT("SL_DELTA_WRITE_BROADCAST_CONCEPT2_DELTA_REQ\n");
		delta_if_t delta_data;

		ak_msg_common_if_t app_if_msg;
		memset(&app_if_msg, 0, sizeof(ak_msg_common_if_t));
		memset(&delta_data, 0, sizeof(delta_if_t));

		delta_data.cmd = DELTA_ROBOT_IF_WRITE_BROADCAST;
		delta_data.len = 20;

		delta_data.data[0] = NEG_DIR;	// Direction for Motor1
		delta_data.data[1] = 0;			// HIBYTE of Degree for Motor1
		delta_data.data[2] = 24;		// LOBYTE of Degree for Motor1
		delta_data.data[3] = 1;			// Setting time for Motor1
		delta_data.data[4] = 1;			// Division Setting time for Motor1

		delta_data.data[5] = NEG_DIR;	// Direction for Motor2
		delta_data.data[6] = 0;			// HIBYTE of Degree for Motor2
		delta_data.data[7] = 78;			// LOBYTE of Degree for Motor2
		delta_data.data[8] = 1;			// Setting time for Motor2
		delta_data.data[9] = 1;			// Division Setting time for Motor2

		delta_data.data[10]	= NEG_DIR;	// Direction for Motor3
		delta_data.data[11]	= 0;		// HIBYTE of Degree for Motor3
		delta_data.data[12] = 78;		// LOBYTE of Degree for Motor3
		delta_data.data[13] = 1;		// Setting time for Motor3
		delta_data.data[14] = 1;		// Division Setting time for Motor3

		delta_data.data[15]	= NEG_DIR;	// Direction for Motor4
		delta_data.data[16] = 0;		// HIBYTE of Degree for Motor4
		delta_data.data[17] = 0;		// LOBYTE of Degree for Motor4
		delta_data.data[18] = 5;		// Setting time for Motor4
		delta_data.data[19] = 1;		// Division Setting time for Motor4

		/* assign if message */
		app_if_msg.header.type			= 0xC0;
		app_if_msg.header.if_src_type	= 1;
		app_if_msg.header.if_des_type	= 1;
		app_if_msg.header.sig			= DELTA_ROBOT_IF_WRITE_BROADCAST;
		app_if_msg.header.src_task_id	= 1;
		app_if_msg.header.des_task_id	= SL_TASK_ENCODER_PID_ID;

		app_if_msg.len = sizeof(delta_if_t);
		memcpy((uint8_t*)&app_if_msg.data[0], (uint8_t*)&delta_data, app_if_msg.len);
		tx_frame_post((uint8_t*)&app_if_msg, sizeof(ak_msg_common_if_t));

		sys_ctrl_delay_ms(300);
		cmd_brc_req(DELTA_ROBOT_IF_START_BROADCAST);
		//timer_set(SL_TASK_ENCODER_PID_ID, SL_DELTA_RESET_CONCEPT2_DATA_DELTA_REQ, 2500, TIMER_ONE_SHOT);
	}
		break;

/*	DELTA: B --> C */
	case SL_DELTA_WRITE_BROADCAST_CONCEPT3_DELTA_REQ: {
		APP_PRINT("SL_DELTA_WRITE_BROADCAST_CONCEPT3_DELTA_REQ\n");
		delta_if_t delta_data;

		ak_msg_common_if_t app_if_msg;
		memset(&app_if_msg, 0, sizeof(ak_msg_common_if_t));
		memset(&delta_data, 0, sizeof(delta_if_t));

		delta_data.cmd = DELTA_ROBOT_IF_WRITE_BROADCAST;
		delta_data.len = 20;

		delta_data.data[0] = POS_DIR;	// Direction for Motor1
		delta_data.data[1] = 0;			// HIBYTE of Degree for Motor1
		delta_data.data[2] = 90;		// LOBYTE of Degree for Motor1
		delta_data.data[3] = 1;			// Setting time for Motor1
		delta_data.data[4] = 1;			// Division Setting time for Motor1

		delta_data.data[5] = POS_DIR;	// Direction for Motor2
		delta_data.data[6] = 0;			// HIBYTE of Degree for Motor2
		delta_data.data[7] = 44;			// LOBYTE of Degree for Motor2
		delta_data.data[8] = 1;			// Setting time for Motor2
		delta_data.data[9] = 1;			// Division Setting time for Motor2

		delta_data.data[10]	= POS_DIR;	// Direction for Motor3
		delta_data.data[11]	= 0;		// HIBYTE of Degree for Motor3
		delta_data.data[12] = 44;		// LOBYTE of Degree for Motor3
		delta_data.data[13] = 1;		// Setting time for Motor3
		delta_data.data[14] = 1;		// Division Setting time for Motor3

		delta_data.data[15]	= NEG_DIR;	// Direction for Motor4
		delta_data.data[16] = 0;		// HIBYTE of Degree for Motor4
		delta_data.data[17] = 0;		// LOBYTE of Degree for Motor4
		delta_data.data[18] = 5;		// Setting time for Motor4
		delta_data.data[19] = 1;		// Division Setting time for Motor4

		/* assign if message */
		app_if_msg.header.type			= 0xC0;
		app_if_msg.header.if_src_type	= 1;
		app_if_msg.header.if_des_type	= 1;
		app_if_msg.header.sig			= DELTA_ROBOT_IF_WRITE_BROADCAST;
		app_if_msg.header.src_task_id	= 1;
		app_if_msg.header.des_task_id	= SL_TASK_ENCODER_PID_ID;

		app_if_msg.len = sizeof(delta_if_t);
		memcpy((uint8_t*)&app_if_msg.data[0], (uint8_t*)&delta_data, app_if_msg.len);
		tx_frame_post((uint8_t*)&app_if_msg, sizeof(ak_msg_common_if_t));

		sys_ctrl_delay_ms(300);
		cmd_brc_req(DELTA_ROBOT_IF_START_BROADCAST);
		//timer_set(SL_TASK_ENCODER_PID_ID, SL_DELTA_RESET_CONCEPT3_DATA_DELTA_REQ, 2500, TIMER_ONE_SHOT);
	}
		break;

/*	DELTA: C --> B */
	case SL_DELTA_WRITE_BROADCAST_CONCEPT4_DELTA_REQ: {
		APP_PRINT("SL_DELTA_WRITE_BROADCAST_CONCEPT4_DELTA_REQ\n");
		delta_if_t delta_data;

		ak_msg_common_if_t app_if_msg;
		memset(&app_if_msg, 0, sizeof(ak_msg_common_if_t));
		memset(&delta_data, 0, sizeof(delta_if_t));

		delta_data.cmd = DELTA_ROBOT_IF_WRITE_BROADCAST;
		delta_data.len = 20;

		delta_data.data[0] = NEG_DIR;	// Direction for Motor1
		delta_data.data[1] = 0;			// HIBYTE of Degree for Motor1
		delta_data.data[2] = 90;		// LOBYTE of Degree for Motor1
		delta_data.data[3] = 1;			// Setting time for Motor1
		delta_data.data[4] = 1;			// Division Setting time for Motor1

		delta_data.data[5] = NEG_DIR;	// Direction for Motor2
		delta_data.data[6] = 0;			// HIBYTE of Degree for Motor2
		delta_data.data[7] = 44;			// LOBYTE of Degree for Motor2
		delta_data.data[8] = 1;			// Setting time for Motor2
		delta_data.data[9] = 1;			// Division Setting time for Motor2

		delta_data.data[10]	= NEG_DIR;	// Direction for Motor3
		delta_data.data[11]	= 0;		// HIBYTE of Degree for Motor3
		delta_data.data[12] = 44;		// LOBYTE of Degree for Motor3
		delta_data.data[13] = 1;		// Setting time for Motor3
		delta_data.data[14] = 1;		// Division Setting time for Motor3

		delta_data.data[15]	= NEG_DIR;	// Direction for Motor4
		delta_data.data[16] = 0;		// HIBYTE of Degree for Motor4
		delta_data.data[17] = 0;		// LOBYTE of Degree for Motor4
		delta_data.data[18] = 5;		// Setting time for Motor4
		delta_data.data[19] = 1;		// Division Setting time for Motor4

		/* assign if message */
		app_if_msg.header.type			= 0xC0;
		app_if_msg.header.if_src_type	= 1;
		app_if_msg.header.if_des_type	= 1;
		app_if_msg.header.sig			= DELTA_ROBOT_IF_WRITE_BROADCAST;
		app_if_msg.header.src_task_id	= 1;
		app_if_msg.header.des_task_id	= SL_TASK_ENCODER_PID_ID;

		app_if_msg.len = sizeof(delta_if_t);
		memcpy((uint8_t*)&app_if_msg.data[0], (uint8_t*)&delta_data, app_if_msg.len);
		tx_frame_post((uint8_t*)&app_if_msg, sizeof(ak_msg_common_if_t));

		sys_ctrl_delay_ms(300);
		cmd_brc_req(DELTA_ROBOT_IF_START_BROADCAST);
		//timer_set(SL_TASK_ENCODER_PID_ID, SL_DELTA_RESET_CONCEPT4_DATA_DELTA_REQ, 2500, TIMER_ONE_SHOT);
	}
		break;

/*	DELTA: B --> A */
	case SL_DELTA_WRITE_BROADCAST_CONCEPT5_DELTA_REQ: {
		APP_PRINT("SL_DELTA_WRITE_BROADCAST_CONCEPT5_DELTA_REQ\n");
		delta_if_t delta_data;

		ak_msg_common_if_t app_if_msg;
		memset(&app_if_msg, 0, sizeof(ak_msg_common_if_t));
		memset(&delta_data, 0, sizeof(delta_if_t));

		delta_data.cmd = DELTA_ROBOT_IF_WRITE_BROADCAST;
		delta_data.len = 20;

		delta_data.data[0] = POS_DIR;	// Direction for Motor1
		delta_data.data[1] = 0;			// HIBYTE of Degree for Motor1
		delta_data.data[2] = 24;		// LOBYTE of Degree for Motor1
		delta_data.data[3] = 1;			// Setting time for Motor1
		delta_data.data[4] = 1;			// Division Setting time for Motor1

		delta_data.data[5] = POS_DIR;	// Direction for Motor2
		delta_data.data[6] = 0;			// HIBYTE of Degree for Motor2
		delta_data.data[7] = 78;			// LOBYTE of Degree for Motor2
		delta_data.data[8] = 1;			// Setting time for Motor2
		delta_data.data[9] = 1;			// Division Setting time for Motor2

		delta_data.data[10]	= POS_DIR;	// Direction for Motor3
		delta_data.data[11]	= 0;		// HIBYTE of Degree for Motor3
		delta_data.data[12] = 78;		// LOBYTE of Degree for Motor3
		delta_data.data[13] = 1;		// Setting time for Motor3
		delta_data.data[14] = 1;		// Division Setting time for Motor3

		delta_data.data[15]	= POS_DIR;	// Direction for Motor4
		delta_data.data[16] = 0;		// HIBYTE of Degree for Motor4
		delta_data.data[17] = 0;		// LOBYTE of Degree for Motor4
		delta_data.data[18] = 5;		// Setting time for Motor4
		delta_data.data[19] = 1;		// Division Setting time for Motor4

		/* assign if message */
		app_if_msg.header.type			= 0xC0;
		app_if_msg.header.if_src_type	= 1;
		app_if_msg.header.if_des_type	= 1;
		app_if_msg.header.sig			= DELTA_ROBOT_IF_WRITE_BROADCAST;
		app_if_msg.header.src_task_id	= 1;
		app_if_msg.header.des_task_id	= SL_TASK_ENCODER_PID_ID;

		app_if_msg.len = sizeof(delta_if_t);
		memcpy((uint8_t*)&app_if_msg.data[0], (uint8_t*)&delta_data, app_if_msg.len);
		tx_frame_post((uint8_t*)&app_if_msg, sizeof(ak_msg_common_if_t));

		sys_ctrl_delay_ms(300);
		cmd_brc_req(DELTA_ROBOT_IF_START_BROADCAST);
		//timer_set(SL_TASK_ENCODER_PID_ID, SL_DELTA_RESET_CONCEPT5_DATA_DELTA_REQ, 2500, TIMER_ONE_SHOT);
	}
		break;

/*	HAND SETUP DELTA: C --> B */
	case SL_DELTA_WRITE_BROADCAST_CONCEPT6_DELTA_REQ: {
		APP_PRINT("SL_DELTA_WRITE_BROADCAST_CONCEPT6_DELTA_REQ\n");
		delta_if_t delta_data;

		ak_msg_common_if_t app_if_msg;
		memset(&app_if_msg, 0, sizeof(ak_msg_common_if_t));
		memset(&delta_data, 0, sizeof(delta_if_t));

		delta_data.cmd = DELTA_ROBOT_IF_WRITE_BROADCAST;
		delta_data.len = 20;

		delta_data.data[0] = NEG_DIR;	// Direction for Motor1
		delta_data.data[1] = 0;			// HIBYTE of Degree for Motor1
		delta_data.data[2] = 90;		// LOBYTE of Degree for Motor1
		delta_data.data[3] = 1;			// Setting time for Motor1
		delta_data.data[4] = 1;			// Division Setting time for Motor1

		delta_data.data[5] = NEG_DIR;	// Direction for Motor2
		delta_data.data[6] = 0;			// HIBYTE of Degree for Motor2
		delta_data.data[7] = 44;			// LOBYTE of Degree for Motor2
		delta_data.data[8] = 1;			// Setting time for Motor2
		delta_data.data[9] = 1;			// Division Setting time for Motor2

		delta_data.data[10]	= NEG_DIR;	// Direction for Motor3
		delta_data.data[11]	= 0;		// HIBYTE of Degree for Motor3
		delta_data.data[12] = 44;		// LOBYTE of Degree for Motor3
		delta_data.data[13] = 1;		// Setting time for Motor3
		delta_data.data[14] = 1;		// Division Setting time for Motor3

		delta_data.data[15]	= NEG_DIR;	// Direction for Motor4
		delta_data.data[16] = 0;		// HIBYTE of Degree for Motor4
		delta_data.data[17] = 0;		// LOBYTE of Degree for Motor4
		delta_data.data[18] = 5;		// Setting time for Motor4
		delta_data.data[19] = 1;		// Division Setting time for Motor4

		/* assign if message */
		app_if_msg.header.type			= 0xC0;
		app_if_msg.header.if_src_type	= 1;
		app_if_msg.header.if_des_type	= 1;
		app_if_msg.header.sig			= DELTA_ROBOT_IF_WRITE_BROADCAST;
		app_if_msg.header.src_task_id	= 1;
		app_if_msg.header.des_task_id	= SL_TASK_ENCODER_PID_ID;

		app_if_msg.len = sizeof(delta_if_t);
		memcpy((uint8_t*)&app_if_msg.data[0], (uint8_t*)&delta_data, app_if_msg.len);
		tx_frame_post((uint8_t*)&app_if_msg, sizeof(ak_msg_common_if_t));

		sys_ctrl_delay_ms(300);
		cmd_brc_req(DELTA_ROBOT_IF_START_BROADCAST);
		//timer_set(SL_TASK_ENCODER_PID_ID, SL_DELTA_RESET_CONCEPT6_DATA_DELTA_REQ, 2500, TIMER_ONE_SHOT);
	}
		break;

/*	HAND SETUP DELTA: B --> A */
	case SL_DELTA_WRITE_BROADCAST_CONCEPT7_DELTA_REQ: {
		APP_PRINT("SL_DELTA_WRITE_BROADCAST_CONCEPT7_DELTA_REQ\n");
		delta_if_t delta_data;

		ak_msg_common_if_t app_if_msg;
		memset(&app_if_msg, 0, sizeof(ak_msg_common_if_t));
		memset(&delta_data, 0, sizeof(delta_if_t));

		delta_data.cmd = DELTA_ROBOT_IF_WRITE_BROADCAST;
		delta_data.len = 20;

		delta_data.data[0] = POS_DIR;	// Direction for Motor1
		delta_data.data[1] = 0;			// HIBYTE of Degree for Motor1
		delta_data.data[2] = 24;		// LOBYTE of Degree for Motor1
		delta_data.data[3] = 1;			// Setting time for Motor1
		delta_data.data[4] = 1;			// Division Setting time for Motor1

		delta_data.data[5] = POS_DIR;	// Direction for Motor2
		delta_data.data[6] = 0;			// HIBYTE of Degree for Motor2
		delta_data.data[7] = 78;			// LOBYTE of Degree for Motor2
		delta_data.data[8] = 1;			// Setting time for Motor2
		delta_data.data[9] = 1;			// Division Setting time for Motor2

		delta_data.data[10]	= POS_DIR;	// Direction for Motor3
		delta_data.data[11]	= 0;		// HIBYTE of Degree for Motor3
		delta_data.data[12] = 78;		// LOBYTE of Degree for Motor3
		delta_data.data[13] = 1;		// Setting time for Motor3
		delta_data.data[14] = 1;		// Division Setting time for Motor3

		delta_data.data[15]	= POS_DIR;	// Direction for Motor4
		delta_data.data[16] = 0;		// HIBYTE of Degree for Motor4
		delta_data.data[17] = 0;		// LOBYTE of Degree for Motor4
		delta_data.data[18] = 5;		// Setting time for Motor4
		delta_data.data[19] = 1;		// Division Setting time for Motor4

		/* assign if message */
		app_if_msg.header.type			= 0xC0;
		app_if_msg.header.if_src_type	= 1;
		app_if_msg.header.if_des_type	= 1;
		app_if_msg.header.sig			= DELTA_ROBOT_IF_WRITE_BROADCAST;
		app_if_msg.header.src_task_id	= 1;
		app_if_msg.header.des_task_id	= SL_TASK_ENCODER_PID_ID;

		app_if_msg.len = sizeof(delta_if_t);
		memcpy((uint8_t*)&app_if_msg.data[0], (uint8_t*)&delta_data, app_if_msg.len);
		tx_frame_post((uint8_t*)&app_if_msg, sizeof(ak_msg_common_if_t));

		sys_ctrl_delay_ms(300);
		cmd_brc_req(DELTA_ROBOT_IF_START_BROADCAST);
		//timer_set(SL_TASK_ENCODER_PID_ID, SL_DELTA_RESET_CONCEPT6_DATA_DELTA_REQ, 2500, TIMER_ONE_SHOT);
	}
		break;

/*	HAND SETUP DELTA: A --> HOME */
	case SL_DELTA_WRITE_BROADCAST_CONCEPT8_DELTA_REQ: {
		APP_PRINT("SL_DELTA_WRITE_BROADCAST_CONCEPT8_DELTA_REQ\n");
		delta_if_t delta_data;

		ak_msg_common_if_t app_if_msg;
		memset(&app_if_msg, 0, sizeof(ak_msg_common_if_t));
		memset(&delta_data, 0, sizeof(delta_if_t));

		delta_data.cmd = DELTA_ROBOT_IF_WRITE_BROADCAST;
		delta_data.len = 20;

		delta_data.data[0] = POS_DIR;	// Direction for Motor1
		delta_data.data[1] = 0;			// HIBYTE of Degree for Motor1
		delta_data.data[2] = 23;		// LOBYTE of Degree for Motor1
		delta_data.data[3] = 1;			// Setting time for Motor1
		delta_data.data[4] = 1;			// Division Setting time for Motor1

		delta_data.data[5] = NEG_DIR;	// Direction for Motor2
		delta_data.data[6] = 0;			// HIBYTE of Degree for Motor2
		delta_data.data[7] = 32;			// LOBYTE of Degree for Motor2
		delta_data.data[8] = 1;			// Setting time for Motor2
		delta_data.data[9] = 1;			// Division Setting time for Motor2

		delta_data.data[10]	= NEG_DIR;	// Direction for Motor3
		delta_data.data[11]	= 0;		// HIBYTE of Degree for Motor3
		delta_data.data[12] = 32;		// LOBYTE of Degree for Motor3
		delta_data.data[13] = 1;		// Setting time for Motor3
		delta_data.data[14] = 1;		// Division Setting time for Motor3

		delta_data.data[15]	= NEG_DIR;	// Direction for Motor4
		delta_data.data[16] = 0;		// HIBYTE of Degree for Motor4
		delta_data.data[17] = 0;		// LOBYTE of Degree for Motor4
		delta_data.data[18] = 5;		// Setting time for Motor4
		delta_data.data[19] = 1;		// Division Setting time for Motor4

		/* assign if message */
		app_if_msg.header.type			= 0xC0;
		app_if_msg.header.if_src_type	= 1;
		app_if_msg.header.if_des_type	= 1;
		app_if_msg.header.sig			= DELTA_ROBOT_IF_WRITE_BROADCAST;
		app_if_msg.header.src_task_id	= 1;
		app_if_msg.header.des_task_id	= SL_TASK_ENCODER_PID_ID;

		app_if_msg.len = sizeof(delta_if_t);
		memcpy((uint8_t*)&app_if_msg.data[0], (uint8_t*)&delta_data, app_if_msg.len);
		tx_frame_post((uint8_t*)&app_if_msg, sizeof(ak_msg_common_if_t));

		sys_ctrl_delay_ms(500);
		cmd_brc_req(DELTA_ROBOT_IF_START_BROADCAST);
		//timer_set(SL_TASK_ENCODER_PID_ID, SL_DELTA_RESET_CONCEPT6_DATA_DELTA_REQ, 2500, TIMER_ONE_SHOT);
	}
		break;
/*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
 *							END WRITE BROADCAST											*
 *								DELTA ROBOT												*
 *								6 CONCEPT												*
 *	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*/

/*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
 *									RESET CONCEPT										*
 *									DELTA ROBOT											*
 *	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*/

/* RESET DELTA: HOME --> A */
	case SL_DELTA_RESET_CONCEPT1_DATA_DELTA_REQ: {
		APP_PRINT("SL_DELTA_RESET_CONCEPT1_DATA_DELTA_REQ\n");
		cmd_brc_req(DELTA_ROBOT_IF_RESET_DATA);
		sys_ctrl_delay_ms(500);
		task_post_pure_msg(SL_TASK_ENCODER_PID_ID, SL_DELTA_WRITE_BROADCAST_CONCEPT2_DELTA_REQ);
	}
		break;

/* RESET DELTA: A --> B */
	case SL_DELTA_RESET_CONCEPT2_DATA_DELTA_REQ: {
		APP_PRINT("SL_DELTA_RESET_CONCEPT2_DATA_DELTA_REQ\n");
		cmd_brc_req(DELTA_ROBOT_IF_RESET_DATA);
		sys_ctrl_delay_ms(500);
		task_post_pure_msg(SL_TASK_ENCODER_PID_ID, SL_DELTA_WRITE_BROADCAST_CONCEPT3_DELTA_REQ);
	}
		break;

/* RESET DELTA: B --> C */
	case SL_DELTA_RESET_CONCEPT3_DATA_DELTA_REQ: {
		APP_PRINT("SL_DELTA_RESET_CONCEPT3_DATA_DELTA_REQ\n");
		cmd_brc_req(DELTA_ROBOT_IF_RESET_DATA);
		sys_ctrl_delay_ms(500);
		task_post_pure_msg(SL_TASK_ENCODER_PID_ID, SL_DELTA_WRITE_BROADCAST_CONCEPT4_DELTA_REQ);
	}
		break;

/* RESET DELTA: C --> B */
	case SL_DELTA_RESET_CONCEPT4_DATA_DELTA_REQ: {
		APP_PRINT("SL_DELTA_RESET_CONCEPT4_DATA_DELTA_REQ\n");
		cmd_brc_req(DELTA_ROBOT_IF_RESET_DATA);
		sys_ctrl_delay_ms(500);
		task_post_pure_msg(SL_TASK_ENCODER_PID_ID, SL_DELTA_WRITE_BROADCAST_CONCEPT5_DELTA_REQ);
	}
		break;

/* RESET DELTA: B --> A */
	case SL_DELTA_RESET_CONCEPT5_DATA_DELTA_REQ: {
		APP_PRINT("SL_DELTA_RESET_CONCEPT4_DATA_DELTA_REQ\n");
		cmd_brc_req(DELTA_ROBOT_IF_RESET_DATA);
		sys_ctrl_delay_ms(500);
		task_post_pure_msg(SL_TASK_ENCODER_PID_ID, SL_DELTA_WRITE_BROADCAST_CONCEPT2_DELTA_REQ);
	}
		break;

	case SL_DELTA_RESET_CONCEPT6_DATA_DELTA_REQ: {
		APP_PRINT("SL_DELTA_RESET_CONCEPT4_DATA_DELTA_REQ\n");
		cmd_brc_req(DELTA_ROBOT_IF_RESET_DATA);
		sys_ctrl_delay_ms(500);
		//task_post_pure_msg(SL_TASK_ENCODER_PID_ID, SL_DELTA_WRITE_BROADCAST_CONCEPT2_DELTA_REQ);
	}
		break;
/*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
 *								END RESET CONCEPT										*
 *									DELTA ROBOT											*
 *	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*/

	default:
		break;
	}
}
