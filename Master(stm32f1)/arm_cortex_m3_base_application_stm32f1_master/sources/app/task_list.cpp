#include "timer.h"

#include "task_list.h"

task_t app_task_table[] = {
	/*************************************************************************/
	/* SYSTEM TASK */
	/*************************************************************************/
	{TASK_TIMER_TICK_ID			,	TASK_PRI_LEVEL_7,		task_timer_tick		},

	/*************************************************************************/
	/* APP TASK */
	/*************************************************************************/
	{SL_TASK_SHELL_ID			,	TASK_PRI_LEVEL_2	,	task_shell			},
	{SL_TASK_LIFE_ID			,	TASK_PRI_LEVEL_6	,	task_life			},
	{SL_TASK_IF_ID				,	TASK_PRI_LEVEL_4	,	task_if				},
	{SL_TASK_SENSOR_ID			,	TASK_PRI_LEVEL_2	,	task_sensor			},
	{SL_TASK_SETTING_ID			,	TASK_PRI_LEVEL_2	,	task_setting		},
	{SL_TASK_CPU_SERIAL_IF_ID	,	TASK_PRI_LEVEL_5	,	task_cpu_serial_if	},
	{SL_TASK_FWU_ID				,	TASK_PRI_LEVEL_2	,	task_fwu			},
	{SL_TASK_KEEPALIVE_ID		,	TASK_PRI_LEVEL_3	,	task_keepalive		},
	{SL_TASK_TEST_ID			,	TASK_PRI_LEVEL_2	,	task_test			},
	{SL_TASK_ZIGBEE_ID			,	TASK_PRI_LEVEL_3	,	task_zigbee			},
	{SL_TASK_ENCODER_PID_ID		,	TASK_PRI_LEVEL_2	,	task_encoder_pid	},
	/*************************************************************************/
	/* END OF TABLE */
	/*************************************************************************/
	{AK_TASK_EOT_ID				,	TASK_PRI_LEVEL_0,		(pf_task)0			}
};
