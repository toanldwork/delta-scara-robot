#include "fsm.h"
#include "port.h"
#include "message.h"
#include "timer.h"

#include "app.h"
#include "app_data.h"
#include "app_if.h"
#include "app_dbg.h"

#include "task_list.h"
#include "task_list_if.h"
#include "task_keepalive.h"

#include "sys_ctrl.h"
#include "sys_io.h"

void task_keepalive(ak_msg_t* msg) {
	switch (msg->sig) {
	case SL_KEEPALIVE_PING: {
		APP_DBG("SL_KEEPALIVE_PING\n");

		ak_msg_t* s_msg = get_pure_msg();
		set_if_src_task_id(s_msg, SL_TASK_KEEPALIVE_ID);
		set_if_des_task_id(s_msg, MT_TASK_KEEPALIVE_ID);
		set_if_src_type(s_msg, IF_TYPE_CPU_SERIAL_SL);
		set_if_des_type(s_msg, IF_TYPE_CPU_SERIAL_MT);
		set_if_sig(s_msg, MT_KEEPALIVE_PONG);

		set_msg_sig(s_msg, SL_IF_PURE_MSG_OUT);
		task_post(SL_TASK_IF_ID, s_msg);

		timer_set(SL_TASK_KEEPALIVE_ID, SL_KEEPALIVE_PING, SL_KEEPALIVE_PING_INTERVAL, TIMER_ONE_SHOT);
	}
		break;

	case SL_KEEPALIVE_PONG: {
		APP_DBG("SL_KEEPALIVE_PONG\n");

		timer_set(SL_TASK_KEEPALIVE_ID, SL_KEEPALIVE_TO, SL_KEEPALIVE_PONG_MAX_TO_INTERVAL, TIMER_ONE_SHOT);
	}
		break;

	case SL_KEEPALIVE_TO: {
		APP_DBG("SL_KEEPALIVE_TO\n");

		rst_master_off();
		sys_ctrl_delay_ms(1000);
		rst_master_on();

		timer_set(SL_TASK_KEEPALIVE_ID, SL_KEEPALIVE_TO, SL_KEEPALIVE_WAIT_MT_RESET, TIMER_ONE_SHOT);
	}
		break;

	default:
		break;
	}
}
