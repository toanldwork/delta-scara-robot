#include "fsm.h"
#include "port.h"
#include "message.h"

#include "app.h"
#include "app_if.h"
#include "app_dbg.h"
#include "app_flash.h"

#include "task_list.h"
#include "task_shell.h"
#include "task_sensor.h"
#include "task_life.h"
#include "task_setting.h"
#include "task_if.h"

#include "utils.h"

#include "sys_ctrl.h"
#include "sys_dbg.h"

fatal_log_t non_clear_ram_fatal_log __attribute__ ((section ("non_clear_ram")));

sl_control_t non_clear_ram_sl_control __attribute__ ((section ("non_clear_ram")));

void flash_read_slave_info(sl_info_t* m_info) {
	strcpy(m_info->firmware_version, SL_FIRMWARE_VERSION);
	strcpy(m_info->hardware_version, SL_HARDWARE_VERSION);
}

uint8_t flash_write_slave_info() {
	return FLASH_WRITE_OK;
}
