#include "fsm.h"
#include "port.h"
#include "message.h"
#include "timer.h"

#include "app.h"
#include "app_if.h"
#include "app_dbg.h"
#include "app_data.h"

#include "task_if.h"
#include "task_list.h"
#include "task_list_if.h"
#include "task_sensor.h"

#include "utils.h"

#include "sys_dbg.h"
#include "sys_irq.h"
#include "sys_io.h"

static void if_des_type_rf24_sl_handler(ak_msg_t* msg);
static void if_des_type_cpu_serial_sl_handler(ak_msg_t* msg);

void task_if(ak_msg_t* msg) {
	if (msg->if_des_type == IF_TYPE_RF24_AC ||
			msg->if_des_type == IF_TYPE_RF24_GW) {
		if_des_type_rf24_sl_handler(msg);
	}
	else if (msg->if_des_type == IF_TYPE_CPU_SERIAL_MT ||
			 msg->if_des_type == IF_TYPE_CPU_SERIAL_SL) {
		if_des_type_cpu_serial_sl_handler(msg);
	}
}

void if_des_type_rf24_sl_handler(ak_msg_t* msg) {
	switch (msg->sig) {
	case SL_IF_PURE_MSG_IN: {
		APP_DBG("SL_IF_PURE_MSG_IN\n");
		msg_inc_ref_count(msg);
		set_msg_sig(msg, msg->if_sig);
		set_msg_src_task_id(msg, msg->if_src_task_id);
		task_post(msg->if_des_task_id, msg);
	}
		break;

	case SL_IF_COMMON_MSG_IN: {
		APP_DBG("SL_IF_COMMON_MSG_IN\n");
		msg_inc_ref_count(msg);
		set_msg_sig(msg, msg->if_sig);
		set_msg_src_task_id(msg, msg->if_src_task_id);
		task_post(msg->if_des_task_id, msg);
	}
		break;

	case SL_IF_PURE_MSG_OUT: {
	}
		break;

	case SL_IF_COMMON_MSG_OUT: {
	}
		break;

	default:
		break;
	}
}

void if_des_type_cpu_serial_sl_handler(ak_msg_t* msg) {
	switch (msg->sig) {
	case SL_IF_PURE_MSG_IN:	{
		APP_DBG("SL_IF_PURE_MSG_IN\n");
		msg_inc_ref_count(msg);
		set_msg_sig(msg, msg->if_sig);
		set_msg_src_task_id(msg, msg->if_src_task_id);
		task_post(msg->if_des_task_id, msg);
	}
		break;

	case SL_IF_COMMON_MSG_IN: {
		APP_DBG("SL_IF_COMMON_MSG_IN\n");
		msg_inc_ref_count(msg);
		set_msg_sig(msg, msg->if_sig);
		set_msg_src_task_id(msg, msg->if_src_task_id);
		task_post(msg->if_des_task_id, msg);
	}
		break;

	case SL_IF_DYNAMIC_MSG_IN: {
		APP_DBG_SIG("SL_IF_DYNAMIC_MSG_IN\n");
		msg_inc_ref_count(msg);
		set_msg_sig(msg, msg->if_sig);
		set_msg_src_task_id(msg, msg->if_src_task_id);
		task_post(msg->if_des_task_id, msg);
	}
		break;

	case SL_IF_PURE_MSG_OUT: {
		APP_DBG("SL_IF_PURE_MSG_OUT\n");
		msg_inc_ref_count(msg);
		set_msg_sig(msg, SL_CPU_SERIAL_IF_PURE_MSG_OUT);
		task_post(SL_TASK_CPU_SERIAL_IF_ID, msg);
	}
		break;

	case SL_IF_COMMON_MSG_OUT: {
		APP_DBG("SL_IF_COMMON_MSG_OUT\n");
		msg_inc_ref_count(msg);
		set_msg_sig(msg, SL_CPU_SERIAL_IF_COMMON_MSG_OUT);
		task_post(SL_TASK_CPU_SERIAL_IF_ID, msg);
	}
		break;

	case SL_IF_DYNAMIC_MSG_OUT: {
		APP_DBG_SIG("SL_IF_DYNAMIC_MSG_OUT\n");
		msg_inc_ref_count(msg);
		set_msg_sig(msg, SL_CPU_SERIAL_IF_DYNAMIC_MSG_OUT);
		task_post(SL_TASK_CPU_SERIAL_IF_ID, msg);
	}
		break;

	default:
		break;
	}
}
