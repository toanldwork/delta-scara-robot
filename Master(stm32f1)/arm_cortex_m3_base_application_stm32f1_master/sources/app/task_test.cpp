#include "fsm.h"
#include "timer.h"
#include "port.h"
#include "message.h"

#include "app.h"
#include "app_dbg.h"
#include "app_data.h"

#include "task_list.h"
#include "task_test.h"
#include <math.h>
#include "sys_ctrl.h"
#include "sys_io.h"
#include "io_cfg.h"

#define SPEED_PULL		250
#define SPEED_ROTATION	1000
#define FORWARD			0
#define BACKWARD		1

uint8_t zigbee_error_flag = 0;

led_t led_status;
solar_data_t solar_report_data;

uint8_t zb_data_stop = 0;
uint8_t zb_data_start = 0;

uint8_t motor_direct = 0;
uint8_t mode_flag = 0;
uint8_t mode_protect_ctr_flag = 0;

void task_test(ak_msg_t* msg) {
	switch (msg->sig) {
	case SL_SOLAR_STOP_EMER_REQ: {
		//		uint8_t stop_button = read_stop_emer_button_pin();
		//		uint8_t mode_button = read_start_mode_button_pin();
		//		APP_PRINT("stop_button: %d - mode_button: %d\n", stop_button, mode_button);

		if (zb_data_stop == 11) {
			APP_PRINT("MODE START SYSTEM!!!\n");
			if (zb_data_start == 9) {
				APP_PRINT("MODE START - AUTORUN SYSTEM\n");
				mode_flag = 1;
				task_post_pure_msg(SL_TASK_TEST_ID, SL_SOLAR_AUTO_RUN_REQ);
			}
			else if (zb_data_start == 10) {
				APP_PRINT("MODE START - CONTROL SYSTEM\n");
				if (mode_flag == 1) {
					mode_flag = 0;
					motor_2_control(FORWARD, 0);
					motor_3_control(FORWARD, 0);
					pump_motor_off();
				}
				task_post_pure_msg(SL_TASK_TEST_ID, SL_SOLAR_CONTROL_REQ);
			}
		}
		else if (zb_data_stop == 12) {
			APP_PRINT("MODE STOP SYSTEM!!!\n");
			task_post_pure_msg(SL_TASK_TEST_ID, SL_SOLAR_STOP_MOTOR_REQ);
		}
	}
		break;

	case SL_SOLAR_AUTO_RUN_REQ: {
		APP_PRINT("SL_SOLAR_AUTO_RUN_REQ\n");
		uint8_t e18_control;
		uint8_t e18_ir_1 = read_E18_ir_sensor_1_pin();
		uint8_t e18_ir_2 = read_E18_ir_sensor_2_pin();
		e18_control = ((e18_ir_1 << 1) | e18_ir_2);
		APP_PRINT("e18_ir_1: %d\n", e18_ir_1);
		APP_PRINT("e18_ir_2: %d\n", e18_ir_2);
		APP_PRINT("e18_control: %d\n", e18_control);

		switch (e18_control) {
		case IR1_IN_IR2_IN: {
			APP_PRINT("IR1_IN_IR2_IN\n");
			motor_direct = 0;
			motor_2_control(FORWARD, 0);
			motor_3_control(FORWARD, 0);
			pump_motor_off();
		}
			break;
		case IR1_OUT_IR2_OUT: {
			APP_PRINT("IR1_OUT_IR2_OUT\n");
			APP_PRINT("motor_direct: %d\n", motor_direct);
			motor_2_control(motor_direct, SPEED_PULL);
			motor_3_control(!motor_direct, SPEED_ROTATION);
			pump_motor_on();
		}
			break;

		case IR1_OUT_IR2_IN: {
			APP_PRINT("IR1_OUT_IR2_IN\n");
			motor_direct = BACKWARD;
			APP_PRINT("motor_direct: %d\n", motor_direct);
			motor_2_control(motor_direct, SPEED_PULL);
			pump_motor_on();
		}
			break;

		case IR1_IN_IR2_OUT: {
			APP_PRINT("IR1_IN_IR2_OUT\n");
			motor_direct = FORWARD;
			APP_PRINT("motor_direct: %d\n", motor_direct);
			motor_2_control(motor_direct, SPEED_PULL);
			pump_motor_on();
		}
			break;

		default:
			break;
		}

	}
		break;

	case SL_SOLAR_STOP_MOTOR_REQ: {
		APP_PRINT("SL_SOLAR_STOP_MOTOR_REQ\n");
		motor_2_control(FORWARD, 0);
		motor_3_control(FORWARD, 0);
	}
		break;

	case SL_SOLAR_RECEIVE_PACKET_CONTROL_REQ: {
		APP_PRINT("SL_SOLAR_RECEIVE_PACKET_CONTROL_REQ\n");
		memcpy((void *)&solar_report_data, (void *)get_data_dynamic_msg(msg), sizeof(solar_data_t));
		APP_DBG("solar_report_data.data[0]: %d\n", solar_report_data.data[0]);
		APP_DBG("solar_report_data.data[1]: %d\n", solar_report_data.data[1]);
		APP_DBG("solar_report_data.data[2]: %d\n", solar_report_data.data[2]);

		if (solar_report_data.data[1] == 9 || solar_report_data.data[1] == 10) {
			zb_data_start = solar_report_data.data[1];
		}
		if (solar_report_data.data[2] == 11 || solar_report_data.data[2] == 12) {
			zb_data_stop = solar_report_data.data[2];
		}

		if (solar_report_data.data[0] == 1) {
			APP_DBG("FORWARD\n");
		}
		else if (solar_report_data.data[0] == 2) {
			APP_DBG("BACKWARD\n");
		}
		else if (solar_report_data.data[0] == 3) {
			APP_DBG("STOP RUN\n");
		}
		else if (solar_report_data.data[0] == 4) {
			APP_DBG("POSITIVE ROTATION\n");
		}
		else if (solar_report_data.data[0] == 5) {
			APP_DBG("INVERSE ROTATION\n");
		}
		else if (solar_report_data.data[0] == 6) {
			APP_DBG("STOP ROTATION\n");
		}
		else if (solar_report_data.data[0] == 7) {
			APP_DBG("RUN PUMP\n");
		}
		else if (solar_report_data.data[0] == 8) {
			APP_DBG("STOP PUMP\n");
		}
		else if (solar_report_data.data[1] == 9) {
			APP_DBG("MODE AUTORUN\n");
		}
		else if (solar_report_data.data[1] == 10) {
			APP_DBG("MODE CONTROL\n");
		}
		else if (solar_report_data.data[2] == 11) {
			APP_DBG("MODE START SYSTEM\n");
		}
		else if (solar_report_data.data[2] == 12) {
			APP_DBG("MODE STOP SYSTEM\n");
		}
	}
		break;

	case SL_SOLAR_CONTROL_REQ: {
		APP_PRINT("SL_SOLAR_CONTROL_REQ\n");
		uint8_t e18_control;
		uint8_t e18_ir_1 = read_E18_ir_sensor_1_pin();
		uint8_t e18_ir_2 = read_E18_ir_sensor_2_pin();
		e18_control = ((e18_ir_1 << 1) | e18_ir_2);

		switch (e18_control) {
		case IR1_IN_IR2_IN: {
			APP_PRINT("IR1_IN_IR2_IN\n");
			motor_2_control(FORWARD, 0);
			motor_3_control(FORWARD, 0);
			pump_motor_off();
		}
			break;

		case IR1_OUT_IR2_OUT: {
			APP_PRINT("IR1_OUT_IR2_OUT\n");
			if (mode_protect_ctr_flag == 0) {
				mode_protect_ctr_flag = 1;
				motor_2_control(FORWARD, 0);
			}
			if (solar_report_data.data[0] == 1) {
				APP_DBG("FORWARD\n");
				motor_2_control(FORWARD, SPEED_PULL + 50);
			}
			else if (solar_report_data.data[0] == 2) {
				APP_DBG("BACKWARD\n");
				motor_2_control(BACKWARD, SPEED_PULL + 50);
			}
			else if (solar_report_data.data[0] == 3) {
				APP_DBG("STOP RUN\n");
				motor_2_control(BACKWARD, 0);
			}
			else if (solar_report_data.data[0] == 4) {
				APP_DBG("POSITIVE ROTATION\n");
				motor_3_control(FORWARD, SPEED_ROTATION);
			}
			else if (solar_report_data.data[0] == 5) {
				APP_DBG("INVERSE ROTATION\n");
				motor_3_control(BACKWARD, SPEED_ROTATION);
			}
			else if (solar_report_data.data[0] == 6) {
				APP_DBG("STOP ROTATION\n");
				motor_3_control(BACKWARD, 0);
			}
			else if (solar_report_data.data[0] == 7) {
				APP_DBG("RUN PUMP\n");
				pump_motor_on();
			}
			else if (solar_report_data.data[0] == 8) {
				APP_DBG("STOP PUMP\n");
				pump_motor_off();
			}
		}
			break;

		case IR1_OUT_IR2_IN: {
			APP_PRINT("IR1_OUT_IR2_IN\n");
			if (mode_protect_ctr_flag == 1) {
				mode_protect_ctr_flag = 0;
				motor_2_control(BACKWARD, 1000);
			}
		}
			break;

		case IR1_IN_IR2_OUT: {
			APP_PRINT("IR1_IN_IR2_OUT\n");
			if (mode_protect_ctr_flag == 1) {
				mode_protect_ctr_flag = 0;
				motor_2_control(FORWARD, 1000);
			}
		}
			break;

		default:
			break;
		}
	}
		break;

	case TEST_LED: {
		APP_DBG("TEST_LED\n");
		led_zigbee_on();
	}
		break;

	default:
		break;
	}
}
