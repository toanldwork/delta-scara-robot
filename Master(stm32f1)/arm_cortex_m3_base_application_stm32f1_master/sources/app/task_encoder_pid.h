#ifndef __TASK_ENCODER_PID__
#define __TASK_ENCODER_PID__

#include <stdint.h>
#include "app.h"

#define IFCPU_DELTA_ROBOT_IF_DATA_SIZE		20

#define POS_DIR								1
#define NEG_DIR								0

#define CONCEPT_SCARA_ROBOT					0
#define CONCEPT_DELTA_ROBOT					1

/*
 * DESCRIPTION SCARA ROBOT:
 * POS_DIR: + DC1: CW
 *			+ DC2: CCW
 *			+ DC3: CCW
 *			+ DC4: UP
 *
 * NEG_DIR: + DC1: CCW
 *			+ DC2: CW
 *			+ DC3: CW
 *			+ DC4: DOWN
 */

/* DESCRIPTION DELTA ROBOT:
* POS_DIR: DOWM
* NEG_DIR: UP
*/
typedef struct {
	uint16_t Dir_feedback;
	uint16_t Cnt_feedback;
	uint16_t Cnt_feedback_pre;
	float  Pos_encoder_feedback;
	float Spe_encoder_feedback;
} encoder_t;

typedef struct {
	uint8_t cmd;
	uint8_t len;
	uint8_t data[IFCPU_DELTA_ROBOT_IF_DATA_SIZE];
} delta_if_t;

enum {
	DELTA_ROBOT_IF_START_BROADCAST = 10,
	DELTA_ROBOT_IF_STOP_BROADCAST,
	DELTA_ROBOT_IF_WRITE_BROADCAST,
	DELTA_ROBOT_IF_START_MOTOR_1,
	DELTA_ROBOT_IF_START_MOTOR_2,
	DELTA_ROBOT_IF_START_MOTOR_3,
	DELTA_ROBOT_IF_START_MOTOR_4,
	DELTA_ROBOT_IF_RESET_DATA
};

extern encoder_t enc_fb1, enc_fb2, enc_fb3 ;
extern uint16_t degree;
#endif //__TASK_ENCODER_PID__
