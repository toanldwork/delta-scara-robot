CFLAGS		+= -I./sources/app
CPPFLAGS	+= -I./sources/app

VPATH += sources/app

# CPP source files
SOURCES_CPP += sources/app/app.cpp
SOURCES_CPP += sources/app/app_data.cpp
SOURCES_CPP += sources/app/app_flash.cpp
SOURCES_CPP += sources/app/app_non_clear_ram.cpp
SOURCES_CPP += sources/app/shell.cpp
SOURCES_CPP += sources/app/app_bsp.cpp

SOURCES_CPP += sources/app/task_shell.cpp
SOURCES_CPP += sources/app/task_life.cpp
SOURCES_CPP += sources/app/task_list.cpp
SOURCES_CPP += sources/app/task_sensor.cpp
SOURCES_CPP += sources/app/task_setting.cpp
SOURCES_CPP += sources/app/task_if.cpp
SOURCES_CPP += sources/app/task_cpu_serial_if.cpp
SOURCES_CPP += sources/app/task_fwu.cpp
SOURCES_CPP += sources/app/task_keepalive.cpp
SOURCES_CPP += sources/app/task_test.cpp
SOURCES_CPP += sources/app/task_zigbee.cpp
SOURCES_CPP += sources/app/task_encoder_pid.cpp

