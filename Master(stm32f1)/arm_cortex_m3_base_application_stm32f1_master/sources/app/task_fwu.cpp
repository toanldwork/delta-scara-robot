#include "fsm.h"
#include "port.h"
#include "message.h"

#include "app.h"
#include "app_dbg.h"

#include "task_list.h"
#include "task_fwu.h"

#include "sys_ctrl.h"
#include "sys_io.h"

void task_fwu(ak_msg_t* msg) {
	switch (msg->sig) {
	case SL_FWU_INIT: {
		APP_DBG("SL_FWU_INIT\n");
		firmware_header_t cr_fw_header;

		sys_boot_t sb;
		sys_boot_get(&sb);

		sys_ctrl_get_firmware_info(&cr_fw_header);

		/* check and update current app header field of boot share data */
		if (memcmp(&cr_fw_header, &(sb.current_fw_app_header), sizeof(firmware_header_t)) != 0) {
			memcpy(&(sb.current_fw_app_header), &cr_fw_header, sizeof(firmware_header_t));
		}

		/* reset update command */
		sb.fw_app_cmd.cmd = SYS_BOOT_CMD_NONE;

		/* reset update command */
		sb.fw_boot_cmd.cmd = SYS_BOOT_CMD_NONE;

		/* clear update app && boot firmware header */
		memset(&(sb.update_fw_app_header), 0, sizeof(firmware_header_t));
		memset(&(sb.update_fw_boot_header), 0, sizeof(firmware_header_t));

		sys_boot_t cr_sb;
		sys_boot_get(&cr_sb);

		if (memcmp(&cr_sb, &sb, sizeof(sys_boot_t)) != 0) {
			sys_boot_set(&sb);
		}
	}
		break;

	case SL_FWU_REQ: {
		APP_DBG("SL_FWU_REQ\n");

		sys_boot_t sb;
		sys_boot_get(&sb);

		/* cmd update request */
		sb.fw_app_cmd.cmd			= SYS_BOOT_CMD_UPDATE_REQ;
		sb.fw_app_cmd.container		= SYS_BOOT_CONTAINER_DIRECTLY;
		sb.fw_app_cmd.io_driver		= SYS_BOOT_IO_DRIVER_UART;
		sb.fw_app_cmd.des_addr		= APP_START_ADDR;
		sb.fw_app_cmd.src_addr		= 0;
		sys_boot_set(&sb);

		APP_DBG("sys_ctrl_reset()\n");
		sys_ctrl_reset();
	}
		break;

	default:
		break;
	}
}
