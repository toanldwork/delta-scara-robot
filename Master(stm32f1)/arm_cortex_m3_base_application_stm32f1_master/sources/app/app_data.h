#ifndef __APP_DATA_H__
#define __APP_DATA_H__

#include <stdint.h>

#include "port.h"

#include "app.h"

#include "sys_dbg.h"

/******************************************************************************
* interface type
*******************************************************************************/
/** RF24 interface for modules
 * IF_TYPE_RF24_GW using to transfer data to gateway.
 * IF_TYPE_RF24_AC using to transfer data to air_condition module.
 * IF_TYPE_RF24_WR using to transfer data to warning_sensors module.
 * IF_TYPE_RF24_DR using to transfer data to door_manager module.
*/
#define IF_TYPE_RF24_GW						(0)
#define IF_TYPE_RF24_AC						(1)
#define IF_TYPE_RF24_WR						(2)
#define IF_TYPE_RF24_DR						(3)
#define IF_TYPE_RF24_MAX					(4)

/******************************************************************************
* Data type of RF24Network
*******************************************************************************/
#define RF24_DATA_COMMON_MSG_TYPE			(1)
#define RF24_DATA_PURE_MSG_TYPE				(2)
#define RF24_DATA_REMOTE_CMD_TYPE			(3)

/** APP interface, communication via socket interface
 *
 */
#define IF_TYPE_APP_GMNG					(100)
#define IF_TYPE_APP_MT						(101)
#define IF_TYPE_APP_GU						(102)


/** CPU SERIAL interface, communication via uart serial interface
 *
 */
#define IF_TYPE_CPU_SERIAL_MT				(120)
#define IF_TYPE_CPU_SERIAL_SL				(121)

/******************************************************************************
* Common define
*******************************************************************************/
#define IF_RETRY_COUNTER_MAX		3

/******************************************************************************
* Commom data structure for transceiver data
*******************************************************************************/
enum {
	SL_VALVE_MODE_AUTO = 1,
	SL_VALVE_MODE_MANUAL,
};

enum {
	SL_OUTPUT_OFF,
	SL_OUTPUT_ON,
	SL_OUTPUT_AUTO,
	SL_OUTPUT_NC,
};

enum {
	SL_CODE_NONE_ERROR = 1,
	SL_CODE_ERROR
};

#define FIRMWARE_PSK						0x1A2B3C4D
#define FIRMWARE_LOK						0x1234ABCD

#define SL_TOTAL_CT_SENSOR					(8)
#define SL_TOTAL_TH_SENSOR					(4)
#define SL_TOTAL_VALVE_CHANEL				(4)
#define SL_TOTAL_RELAY_CHANEL				(4)
#define SL_TOTAL_AUDIO_CHANEL				(4)
#define SL_TOTAL_CT_CHANEL					(2)
#define SL_TOTAL_TIMER						(8)

#define GW_TOTAL_AUDIO_CHANEL				(4)

#define TOTAL_SAMPLE_AUDIO_SENSOR			(15)

/*valve data struct define*/
typedef struct {
	uint16_t milestone_first;
	uint16_t milestone_second;
	uint8_t action;
} __AK_PACKETED valve_timer_t;

typedef struct {
	uint8_t mode;
	uint8_t temperature_low;
	uint8_t temperature_high;
	uint8_t total_timer;

	valve_timer_t timer[SL_TOTAL_TIMER];
} __AK_PACKETED valve_chanel_t;

typedef struct {
	uint8_t chanel;
	valve_chanel_t valve;
} __AK_PACKETED valve_chanel_settings_t;

typedef struct {
	uint8_t chanel;
	uint8_t mode;
	uint8_t action;
} __AK_PACKETED valve_chanel_control_t;

/*relay data struct define*/
typedef struct {
	uint8_t action;
	uint16_t milestone;
} __AK_PACKETED normal_timer_t;

typedef struct {
	uint8_t total_timer;
	normal_timer_t timer[SL_TOTAL_TIMER];
} __AK_PACKETED relay_chanel_t;

typedef struct {
	uint8_t chanel;
	relay_chanel_t relay;
} __AK_PACKETED relay_chanel_settings_t;

/*slave report data struct define*/
typedef struct {
	char firmware_version[10];
	char hardware_version[10];
} __AK_PACKETED sl_info_t;

/*slave report data struct define*/
typedef struct {
	valve_chanel_t valve[SL_TOTAL_VALVE_CHANEL];
	relay_chanel_t relay[SL_TOTAL_RELAY_CHANEL];
} __AK_PACKETED sl_settings_t;

typedef struct {
	uint8_t valve[SL_TOTAL_VALVE_CHANEL];
	uint8_t relay[SL_TOTAL_RELAY_CHANEL];
} __AK_PACKETED sl_control_t;

typedef struct {
	uint8_t temperature[SL_TOTAL_TH_SENSOR];
	uint8_t humidity[SL_TOTAL_TH_SENSOR];

	uint8_t valve[SL_TOTAL_VALVE_CHANEL];
	uint8_t relay[SL_TOTAL_RELAY_CHANEL];
} __AK_PACKETED sl_common_sensors_t;

typedef struct {
	uint16_t audio[SL_TOTAL_CT_SENSOR][TOTAL_SAMPLE_AUDIO_SENSOR];
} __AK_PACKETED sl_audio_sensors_t;

typedef struct {
	char	ip[20];
	char	mac[20];
}__AK_PACKETED gw_network_info_t;

typedef struct {
	uint8_t is_power_on_reset;
} boot_app_share_data_t;

typedef struct {
	uint16_t decode_type;
	uint8_t cmd_len;
	uint8_t cmd[30];
} ir_data_t;

typedef struct {
	uint8_t data[3];
} solar_data_t;

#endif //__APP_DATA_H__
