/**
 ******************************************************************************
 * @Author: ThanNT
 * @Date:   13/08/2016
 ******************************************************************************
**/

/* kernel include */
#include "ak.h"
#include "message.h"
#include "timer.h"
#include "fsm.h"
#include "tsm.h"
#include "task.h"

/* app include */
#include "app.h"
#include "app_dbg.h"
#include "app_flash.h"
#include "app_bsp.h"
#include "app_non_clear_ram.h"

/* task include */
#include "task_list.h"
#include "task_shell.h"
#include "task_sensor.h"
#include "task_life.h"
#include "task_test.h"
#include "task_list_if.h"
#include "task_setting.h"
#include "task_sensor.h"
#include "task_if.h"
#include "io_cfg.h"

/* common include */
#include "utils.h"

/* driver include */
#include "led.h"
// #include "WEMOS_SHT3X.h"

/* sys include */
#include "sys_boot.h"
#include "sys_irq.h"
#include "sys_io.h"
#include "sys_ctrl.h"
#include "sys_dbg.h"
#include "sys_arduino.h"

static void app_start_timer();
static void app_init_state_machine();
static void app_task_init();

/*****************************************************************************/
/* app main function.
 */
/*****************************************************************************/
int main_app() {
	APP_PRINT("main_app() entry OK\n");

	sys_soft_reboot_counter++;

	/******************************************************************************
	* init active kernel
	*******************************************************************************/
	ENTRY_CRITICAL();
	task_init();
	task_create(app_task_table);
	EXIT_CRITICAL();
	/******************************************************************************
	* init applications
	*******************************************************************************/
	/*********************
	* hardware configure *
	**********************/
	/* init watch dog timer */
	sys_ctrl_independent_watchdog_init();	/* 30s */
	sys_ctrl_soft_watchdog_init(150);		/* 15s */

	/* life led init */
	led_init(&led_life, led_life_init, led_life_on, led_life_off);

	/* life zigbee init */
	// led_zigbee_init();

	/* button join init */
	// button_init(&btn_mode,	10,	BUTTON_MODE_ID,	io_button_mode_init,	io_button_mode_read,	btn_mode_callback);
	// button_enable(&btn_mode);

	/* get app settings value */
	flash_read_slave_info(&slave_info);

#if defined(NON_CLEAR_RAM_FATAL_LOG)
	non_clear_ram_fatal_log.restart_times ++;
#else

#endif
	/*	------------------------------------ SOLAR PANEL CLEANING FUNCTION ---------------------------------------------*/
//	/* uart zigbee init */
//	io_uart_interface_cfg();

//	/* state button init */
//	state_button_init();

//	/* E18 ir sensor init */
//	E18_ir_sensor_input_init();

//	/* motor direct init */
//	motor_output_init();

//	/* pwm timer3 init */
//	io_pwm_timer3_control_cfg();

//	/* pump motor init */
//	pump_motor_init();
	/*	---------------------------------------------------------------------------------------------------------------*/

	/*	------------------------------------ ROBOT DELTA - SCARA FUNCTION ---------------------------------------------*/

	/* timer1 export pwm to robot init */
	//timer1_control_pwm_cfg();

	/* timer2 10ms init */
	//timer2_10ms_cfg();

	/* timer3 read encoder init */
	//timer3_read_encoder_cfg();

	/* control direct motor init */
	//motor_dir_output_init();

	/* button control init */
	button_control_init();

	/* uart for transfer data init */
	io_uart_interface_cfg();

	/*	---------------------------------------------------------------------------------------------------------------*/

	/*enable interrupts*/
	EXIT_CRITICAL();

	/* start timer for application */
	app_init_state_machine();
	app_start_timer();

	/******************************************************************************
	* app task initial
	*******************************************************************************/
	app_task_init();

	/******************************************************************************
	* run applications
	*******************************************************************************/
	return task_run();
}

/*****************************************************************************/
/* app initial function.
 */
/*****************************************************************************/
/* start software timer for application
 * used for app tasks
 */
void app_start_timer() {
	/* start timer to toggle life led */
	timer_set(SL_TASK_LIFE_ID, SL_LIFE_SYSTEM_CHECK, SL_LIFE_TASK_TIMER_LED_LIFE_INTERVAL, TIMER_PERIODIC);

//	timer_set(SL_TASK_ZIGBEE_ID, SL_ZIGBEE_START_COODINATOR, 1000, TIMER_ONE_SHOT);
//	timer_set(SL_TASK_TEST_ID, SL_SOLAR_STOP_EMER_REQ, SL_SOLAR_CHECKING_REQ_INTERVAL, TIMER_PERIODIC);
// 	timer_set(SL_TASK_SENSOR_ID, SL_SENSOR_SHT30_READ_REQ, 1000, TIMER_PERIODIC);

	/* start timer to Robot Delta task */
	timer_set(SL_TASK_ENCODER_PID_ID, SL_DELTA_BUTTON_REQ, 100, TIMER_PERIODIC);

}

/* init state machine for tasks
 * used for app tasks
 */
void app_init_state_machine() {
	/* init state for task SM */
}

/* send first message to trigger start tasks
 * used for app tasks
 */
void app_task_init() {
	task_post_pure_msg(SL_TASK_ZIGBEE_ID, SL_ZIGBEE_INIT);
}

/*****************************************************************************/
/* app common function
 */
/*****************************************************************************/
/* hardware timer interrupt 10ms
 * used for led, button polling
 */
void sys_irq_timer_10ms() {
	//button_timer_polling(&btn_mode);
}
