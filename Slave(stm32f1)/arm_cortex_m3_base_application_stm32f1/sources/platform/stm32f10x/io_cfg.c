/**
 ******************************************************************************
 * @Author: ThanNT
 * @Date:   05/09/2016
 * @Update:
 * @AnhHH: Add io function for sth11 sensor.
 ******************************************************************************
**/
#include <stdint.h>
#include <stdbool.h>

#include "io_cfg.h"
#include "platform.h"

#include "utils.h"

#include "sys_dbg.h"
#include "sys_ctrl.h"

#include "xprintf.h"

#include "led.h"

/******************************************************************************
* led status function
*******************************************************************************/
void led_life_init() {
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(LED_LIFE_IO_CLOCK , ENABLE);

	GPIO_InitStructure.GPIO_Pin		= LED_LIFE_IO_PIN;
	GPIO_InitStructure.GPIO_Mode	= GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed	= GPIO_Speed_2MHz;
	GPIO_Init(LED_LIFE_IO_PORT, &GPIO_InitStructure);
}

void led_life_on() {
	GPIO_ResetBits(LED_LIFE_IO_PORT, LED_LIFE_IO_PIN);
}

void led_life_off() {
	GPIO_SetBits(LED_LIFE_IO_PORT, LED_LIFE_IO_PIN);
}

void led_zigbee_init() {
	//led_zigbee_off();

	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(LED_STATUS_IO_CLOCK , ENABLE);
	GPIO_InitStructure.GPIO_Pin		= LED_STATUS_IO_PIN;
	GPIO_InitStructure.GPIO_Mode	= GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed	= GPIO_Speed_2MHz;
	GPIO_Init(LED_STATUS_IO_PORT, &GPIO_InitStructure);
}

void led_zigbee_off() {
	GPIO_ResetBits(LED_STATUS_IO_PORT, LED_STATUS_IO_PIN);
}

void led_zigbee_on() {
	GPIO_SetBits(LED_STATUS_IO_PORT, LED_STATUS_IO_PIN);
}

/******************************************************************************
* button join function
*******************************************************************************/
void io_button_mode_init() {
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(BUTTON_JOIN_IO_CLOCK, ENABLE);

	GPIO_InitStructure.GPIO_Pin   = BUTTON_JOIN_IO_PIN;
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IPU;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(BUTTON_JOIN_IO_PORT, &GPIO_InitStructure);
}

uint8_t io_button_mode_read() {
	return  GPIO_ReadInputDataBit(BUTTON_JOIN_IO_PORT, BUTTON_JOIN_IO_PIN);
}

/******************************************************************************
* hc-sr04 io function
*******************************************************************************/
void echo_init() {
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);

	GPIO_InitStructure.GPIO_Pin		= GPIO_Pin_2;
	GPIO_InitStructure.GPIO_Mode	= GPIO_Mode_IPD;
	GPIO_InitStructure.GPIO_Speed	= GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
}

void trig_init() {
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);

	GPIO_InitStructure.GPIO_Pin		= GPIO_Pin_1;
	GPIO_InitStructure.GPIO_Mode	= GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed	= GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
}

uint32_t hcsr05_read()
{
	uint32_t local_time = 0;
	GPIO_ResetBits(GPIOA, GPIO_Pin_1);
	sys_ctrl_delay_us(2);
	GPIO_SetBits(GPIOA, GPIO_Pin_1); // pull the TRIG pin HIGH
	sys_ctrl_delay_us(10);
	GPIO_ResetBits(GPIOA, GPIO_Pin_1); // pull the TRIG LOW

	while(!GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_2));

	while(GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_2))
	{
		local_time++;
		sys_ctrl_delay_us(1);
	}
	return local_time*2;
}

void initMeasureTimer() {
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
	RCC_ClocksTypeDef RCC_ClocksStatus;
	RCC_GetClocksFreq(&RCC_ClocksStatus);
	uint16_t prescaler = RCC_ClocksStatus.SYSCLK_Frequency / 1000000 - 1; //1 tick = 1us (1 tick = 0.165mm resolution)

	TIM_DeInit(US_TIMER);
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
	TIM_TimeBaseInitStruct.TIM_Prescaler = prescaler;
	TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInitStruct.TIM_Period = 0xFFFF;
	TIM_TimeBaseInitStruct.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInit(US_TIMER, &TIM_TimeBaseInitStruct);

	TIM_OCInitTypeDef TIM_OCInitStruct;
	TIM_OCStructInit(&TIM_OCInitStruct);
	TIM_OCInitStruct.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStruct.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStruct.TIM_Pulse = 15; //us
	TIM_OCInitStruct.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OC3Init(US_TIMER, &TIM_OCInitStruct);

	TIM_ICInitTypeDef TIM_ICInitStruct;
	TIM_ICInitStruct.TIM_Channel = TIM_Channel_1;
	TIM_ICInitStruct.TIM_ICPolarity = TIM_ICPolarity_Rising;
	TIM_ICInitStruct.TIM_ICSelection = TIM_ICSelection_DirectTI;
	TIM_ICInitStruct.TIM_ICPrescaler = TIM_ICPSC_DIV1;
	TIM_ICInitStruct.TIM_ICFilter = 0;

	TIM_PWMIConfig(US_TIMER, &TIM_ICInitStruct);
	TIM_SelectInputTrigger(US_TIMER, US_TIMER_TRIG_SOURCE);
	TIM_SelectMasterSlaveMode(US_TIMER, TIM_MasterSlaveMode_Enable);

	TIM_CtrlPWMOutputs(US_TIMER, ENABLE);

	TIM_ClearFlag(US_TIMER, TIM_FLAG_Update);
}

void initPins() {
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin = US_TRIG_PIN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(US_TRIG_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = US_ECHO_PIN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(US_ECHO_PORT, &GPIO_InitStructure);
}

int32_t HCSR04GetDistance() {
	(US_TIMER)->CNT = 0;
	TIM_Cmd(US_TIMER, ENABLE);
	while(!TIM_GetFlagStatus(US_TIMER, TIM_FLAG_Update));
	TIM_Cmd(US_TIMER, DISABLE);
	TIM_ClearFlag(US_TIMER, TIM_FLAG_Update);
	return (TIM_GetCapture2(US_TIMER)-TIM_GetCapture1(US_TIMER))*165/1000;
}


/*	-------------------		SOLAR PANEL CLEANING ROBOT	--------------------------*/
/*	------------------------------------------------------------------------------*/

/*****************************************************************************
 *  E18-D80NK NPN IR Sensor pin
******************************************************************************/
void E18_ir_sensor_input_init() {
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(E18_IR_SENSOR_IO_CLOCK , ENABLE);

	GPIO_InitStructure.GPIO_Pin		= E18_IR_SENSOR_1_IO_PIN;
	GPIO_InitStructure.GPIO_Mode	= GPIO_Mode_IN_FLOATING;
	GPIO_InitStructure.GPIO_Speed	= GPIO_Speed_50MHz;
	GPIO_Init(E18_IR_SENSOR_1_IO_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin		= E18_IR_SENSOR_2_IO_PIN;
	GPIO_InitStructure.GPIO_Mode	= GPIO_Mode_IN_FLOATING;
	GPIO_InitStructure.GPIO_Speed	= GPIO_Speed_50MHz;
	GPIO_Init(E18_IR_SENSOR_2_IO_PORT, &GPIO_InitStructure);
}

uint8_t read_E18_ir_sensor_1_pin() {
	return GPIO_ReadInputDataBit(E18_IR_SENSOR_1_IO_PORT, E18_IR_SENSOR_1_IO_PIN);
}

uint8_t read_E18_ir_sensor_2_pin() {
	return GPIO_ReadInputDataBit(E18_IR_SENSOR_2_IO_PORT, E18_IR_SENSOR_2_IO_PIN);
}

/*****************************************************************************
 *  State Button pin
******************************************************************************/
void state_button_init() {
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(STATE_BUTTON_IO_CLOCK , ENABLE);

	GPIO_InitStructure.GPIO_Pin		= STOP_EMERGENCY_BUTTON_IO_PIN;
	GPIO_InitStructure.GPIO_Mode	= GPIO_Mode_IPU;
	GPIO_InitStructure.GPIO_Speed	= GPIO_Speed_50MHz;
	GPIO_Init(STOP_EMERGENCY_BUTTON_IO_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin		= START_MODE_BUTTON_IO_PIN;
	GPIO_InitStructure.GPIO_Mode	= GPIO_Mode_IPU;
	GPIO_InitStructure.GPIO_Speed	= GPIO_Speed_50MHz;
	GPIO_Init(START_MODE_BUTTON_IO_PORT, &GPIO_InitStructure);
}

uint8_t read_stop_emer_button_pin() {
	return GPIO_ReadInputDataBit(STOP_EMERGENCY_BUTTON_IO_PORT, STOP_EMERGENCY_BUTTON_IO_PIN);
}

uint8_t read_start_mode_button_pin() {
	return GPIO_ReadInputDataBit(START_MODE_BUTTON_IO_PORT, START_MODE_BUTTON_IO_PIN);
}

/*****************************************************************************
 *  Pump Motor pin
******************************************************************************/
void pump_motor_init() {
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(PUMP_MOTOR_IO_CLOCK , ENABLE);

	GPIO_InitStructure.GPIO_Pin		= PUMP_MOTOR_IO_PIN;
	GPIO_InitStructure.GPIO_Mode	= GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed	= GPIO_Speed_50MHz;
	GPIO_Init(PUMP_MOTOR_IO_PORT, &GPIO_InitStructure);
}

void pump_motor_on() {
	GPIO_SetBits(PUMP_MOTOR_IO_PORT, PUMP_MOTOR_IO_PIN);
}

void pump_motor_off() {
	GPIO_ResetBits(PUMP_MOTOR_IO_PORT, PUMP_MOTOR_IO_PIN);
}

/*****************************************************************************
 *  Control pwm cfg pin
******************************************************************************/
void io_pwm_timer3_control_cfg() {
	GPIO_InitTypeDef GPIO_InitStructure;
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
	TIM_OCInitTypeDef TIM_OCInitStructure;

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
	RCC_APB2PeriphClockCmd(MOTOR_1_IO_CLOCK | MOTOR_3_IO_CLOCK, ENABLE);

	GPIO_InitStructure.GPIO_Pin = MOTOR_1_IO_PIN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(MOTOR_1_IO_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = MOTOR_2_IO_PIN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(MOTOR_2_IO_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = MOTOR_3_IO_PIN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(MOTOR_3_IO_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = MOTOR_4_IO_PIN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(MOTOR_4_IO_PORT, &GPIO_InitStructure);

	/*
	 * TIM_Prescaler = (Timer input clock/Counter frequency) - 1
	 * Counter frequency = Frequency Required x Pwm Resolution(Steps)
	 * Note: + Timer input clock for Timer 1, 2, 3, 4 is 72MHz
			 + TIM_Prescaler, TIM_Period, Pwm Resolution is a 16-bit resistor so their value must not exceed 65535.
	 * Ex: TIM_Prescaler = (72MHz / (1KHz * 1000(picked a random value))) - 1 = 71
		   TIM_Period (Pwm Resolution) = 999 (count 0 - 999)*/
	TIM_DeInit(TIM3);
	TIM_TimeBaseStructure.TIM_Prescaler = 72 - 1 ;
	TIM_TimeBaseStructure.TIM_Period = 1000 - 1;
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);

	/* Channel 1*/
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OCInitStructure.TIM_Pulse = 0;
	TIM_OC1Init(TIM3, &TIM_OCInitStructure);
	TIM_OC1PreloadConfig(TIM3, TIM_OCPreload_Enable);

	/* Channel 2*/
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OCInitStructure.TIM_Pulse = 0;
	TIM_OC2Init(TIM3, &TIM_OCInitStructure);
	TIM_OC2PreloadConfig(TIM3, TIM_OCPreload_Enable);

	/* Channel 3*/
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OCInitStructure.TIM_Pulse = 0;
	TIM_OC3Init(TIM3, &TIM_OCInitStructure);
	TIM_OC3PreloadConfig(TIM3, TIM_OCPreload_Enable);

	/* Channel 4*/
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OCInitStructure.TIM_Pulse = 0;
	TIM_OC4Init(TIM3, &TIM_OCInitStructure);
	TIM_OC4PreloadConfig(TIM3, TIM_OCPreload_Enable);

	TIM_ARRPreloadConfig(TIM3, ENABLE);

	TIM_Cmd(TIM3, ENABLE);
	TIM_CtrlPWMOutputs(TIM3, ENABLE);
	TIM3->CCR1 = 1000;
	TIM3->CCR2 = 1000;
	TIM3->CCR3 = 1000;
	TIM3->CCR4 = 1000;
}
/*****************************************************************************
 *  Func for Direct and Speed Motor pin
******************************************************************************/
void motor_1_control(uint8_t direct, uint16_t speed) {
	switch (direct) {
	/* Motor1 run forward */
	case 0: {
		GPIO_SetBits(MOTOR_1_IO_PORT_DIRECT, MOTOR_1_IO_PIN_DIRECT);
		TIM3->CCR1 = 1000 - speed;
	}
		break;

		/* Motor1 run backward */
	case 1: {
		GPIO_ResetBits(MOTOR_1_IO_PORT_DIRECT, MOTOR_1_IO_PIN_DIRECT);
		TIM3->CCR1 = 1000 - speed;
	}
		break;

	default:
		break;
	}
}

void motor_2_control(uint8_t direct, uint16_t speed) {
	switch (direct) {
	case 0: {
		GPIO_SetBits(MOTOR_2_IO_PORT_DIRECT, MOTOR_2_IO_PIN_DIRECT);
		TIM3->CCR2 = 1000 - speed;
	}
		break;
	case 1: {
		GPIO_ResetBits(MOTOR_2_IO_PORT_DIRECT, MOTOR_2_IO_PIN_DIRECT);
		TIM3->CCR2 = 1000 - speed;
	}
		break;

	default:
		break;
	}
}

void motor_3_control(uint8_t direct, uint16_t speed) {
	switch (direct) {
	case 0: {
		GPIO_SetBits(MOTOR_3_IO_PORT_DIRECT, MOTOR_3_IO_PIN_DIRECT);
		TIM3->CCR3 = 1000 - speed;
	}
		break;
	case 1: {
		GPIO_ResetBits(MOTOR_3_IO_PORT_DIRECT, MOTOR_3_IO_PIN_DIRECT);
		TIM3->CCR3 = 1000 - speed;
	}
		break;

	default:
		break;
	}
}
/*-------------------------	END SOLAR PANEL CLEANING ROBOT	-------------------*/
/*-----------------------------------------------------------------------------*/

/******************************************************************************
* adc function
* + CT sensor
* + themistor sensor
* Note: MUST be enable internal clock for adc module.
*******************************************************************************/
#ifdef VER1
void io_adc_ct_cfg() {
	ADC_InitTypeDef ADC_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_ADCCLKConfig(RCC_PCLK2_Div6);

	/* Enable ADC1 clock so that we can talk to it */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
	RCC_APB2PeriphClockCmd(CT_ADC_IO_CLOCK , ENABLE);

	ADC_DeInit(ADC1);
	ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;
	ADC_InitStructure.ADC_ScanConvMode = DISABLE;
	ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_NbrOfChannel = 1;
	ADC_Init(ADC1, &ADC_InitStructure);
	ADC_Cmd(ADC1, ENABLE);

	/* Enable ADC1 reset calibaration register */
	ADC_ResetCalibration(ADC1);
	while(ADC_GetResetCalibrationStatus(ADC1));

	/* Start ADC1 calibaration */
	ADC_StartCalibration(ADC1);
	while(ADC_GetCalibrationStatus(ADC1));

	GPIO_InitStructure.GPIO_Pin = CT1_ADC_PIN | CT2_ADC_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
	GPIO_Init(CT_ADC_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = CT1_SL_PIN | CT2_SL_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(CT_ADC_PORT, &GPIO_InitStructure);
}

uint16_t io_adc_ct_read(uint8_t chanel) {
	ADC_RegularChannelConfig(ADC1, chanel, 1, ADC_SampleTime_13Cycles5);

	ADC_SoftwareStartConvCmd(ADC1, ENABLE);
	while(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET);

	return ADC_GetConversionValue(ADC1);
}

void io_adc_ct_select_chanel(uint8_t chanel) {
	switch (chanel) {
	case 0:
		GPIO_ResetBits(CT_ADC_PORT, CT1_SL_PIN);
		GPIO_ResetBits(CT_ADC_PORT, CT2_SL_PIN);
		break;

	case 1:
		GPIO_SetBits(CT_ADC_PORT, CT1_SL_PIN);
		GPIO_ResetBits(CT_ADC_PORT, CT2_SL_PIN);
		break;

	case 2:
		GPIO_ResetBits(CT_ADC_PORT, CT1_SL_PIN);
		GPIO_SetBits(CT_ADC_PORT, CT2_SL_PIN);
		break;

	case 3:
		GPIO_SetBits(CT_ADC_PORT, CT1_SL_PIN);
		GPIO_SetBits(CT_ADC_PORT, CT2_SL_PIN);
		break;

	default:
		break;
	}

	sys_ctrl_delay_ms(1);
}
#else
void io_adc_ct_cfg() {
	ADC_InitTypeDef ADC_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_ADCCLKConfig(RCC_PCLK2_Div6);

	/* Enable ADC1 clock so that we can talk to it */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);

	ADC_DeInit(ADC1);
	ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;
	ADC_InitStructure.ADC_ScanConvMode = DISABLE;
	ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_NbrOfChannel = 1;
	ADC_Init(ADC1, &ADC_InitStructure);
	ADC_Cmd(ADC1, ENABLE);

	/* Enable ADC1 reset calibaration register */
	ADC_ResetCalibration(ADC1);
	while(ADC_GetResetCalibrationStatus(ADC1));

	/* Start ADC1 calibaration */
	ADC_StartCalibration(ADC1);
	while(ADC_GetCalibrationStatus(ADC1));

	RCC_APB2PeriphClockCmd(CT1_IO_CLOCK , ENABLE);
	GPIO_InitStructure.GPIO_Pin = CT1_IO_PIN | CT2_IO_PIN | CT3_IO_PIN | CT5_IO_PIN | CT6_IO_PIN | CT7_IO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
	GPIO_Init(CT1_IO_PORT, &GPIO_InitStructure);

	RCC_APB2PeriphClockCmd(CT4_IO_CLOCK , ENABLE);
	GPIO_InitStructure.GPIO_Pin = CT4_IO_PIN | CT8_IO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
	GPIO_Init(CT4_IO_PORT, &GPIO_InitStructure);
}

uint16_t io_adc_ct_read(uint8_t chanel) {
	ADC_RegularChannelConfig(ADC1, chanel, 1, ADC_SampleTime_13Cycles5);

	ADC_SoftwareStartConvCmd(ADC1, ENABLE);
	while(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET);

	return ADC_GetConversionValue(ADC1);
}

void io_adc_ct_select_chanel(uint8_t chanel) {(void)(chanel);};
#endif

/*****************************************************************************
 *io uart for if_sl_cpu
******************************************************************************/
void io_uart_interface_cfg() {
	USART_InitTypeDef USART_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	/* Enable GPIO clock */
	RCC_APB2PeriphClockCmd(USART_IF_TX_GPIO_CLK | RCC_APB2Periph_AFIO, ENABLE);

	/* Enable USART clock */
	RCC_APB1PeriphClockCmd(USART_IF_CLK, ENABLE);

	/* Configure USART Tx and Rx as alternate function push-pull */
	GPIO_InitStructure.GPIO_Mode	= GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed	= GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Pin		= USART_IF_TX_PIN;
	GPIO_Init(USART_IF_TX_GPIO_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Mode	= GPIO_Mode_IPU;
	GPIO_InitStructure.GPIO_Pin		= USART_IF_RX_PIN;
	GPIO_Init(USART_IF_RX_GPIO_PORT, &GPIO_InitStructure);

	/* USART2 configuration */
	USART_DeInit(USART_IF);
	USART_InitStructure.USART_BaudRate		= 115200;
	USART_InitStructure.USART_WordLength	= USART_WordLength_8b;
	USART_InitStructure.USART_StopBits		= USART_StopBits_1;
	USART_InitStructure.USART_Parity		= USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART_IF, &USART_InitStructure);

	//GPIO_PinRemapConfig(GPIO_Remap_USART2, ENABLE);

	/* NVIC configuration */
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);
	NVIC_InitStructure.NVIC_IRQChannel = USART_IF_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	USART_ClearITPendingBit(USART_IF, USART_IT_RXNE);
	USART_ITConfig(USART_IF, USART_IT_RXNE, ENABLE);

	/* Enable USART */
	USART_Cmd(USART_IF, ENABLE);
}

void io_uart_interface_transfer(uint8_t c) {
	/* wait tx data buf empty */
	while (USART_GetFlagStatus(USART_IF, USART_FLAG_TXE) == RESET);

	USART_SendData(USART_IF, (uint8_t)c);

	/* wait tx data transfer completed */
	while (USART_GetFlagStatus(USART_IF, USART_FLAG_TC) == RESET);
}

uint8_t io_uart_interface_receiver() {
	uint8_t c = 0;

	if(USART_GetITStatus(USART_IF, USART_IT_RXNE) == SET) {
		USART_ClearITPendingBit(USART_IF,USART_IT_RXNE);
		c = (uint8_t)USART_ReceiveData(USART_IF);


	}

	return c;
}

/*****************************************************************************
 *io uart for RS485
******************************************************************************/
void io_uart_rs485_cfg(long baudrate) {
	USART_InitTypeDef USART_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	/* Enable GPIO clock */
	RCC_APB2PeriphClockCmd(USART_RS485_TX_GPIO_CLK, ENABLE);

	/* Enable USART clock */
	RCC_APB1PeriphClockCmd(USART_RS485_CLK, ENABLE);

	/* Configure USART Tx and Rx as alternate function push-pull */
	GPIO_InitStructure.GPIO_Mode	= GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed	= GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Pin		= USART_RS485_TX_PIN;
	GPIO_Init(USART_RS485_TX_GPIO_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin		= USART_RS485_RX_PIN;
	GPIO_InitStructure.GPIO_Mode	= GPIO_Mode_IPU;
	GPIO_Init(USART_RS485_RX_GPIO_PORT, &GPIO_InitStructure);

	/* USART2 configuration */
	USART_DeInit(USART_RS485);
	USART_InitStructure.USART_BaudRate		= baudrate;
	USART_InitStructure.USART_WordLength	= USART_WordLength_8b;
	USART_InitStructure.USART_StopBits		= USART_StopBits_1;
	USART_InitStructure.USART_Parity		= USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART_RS485, &USART_InitStructure);

	/* NVIC configuration */
	/* Configure the Priority Group to 4 bits */
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);
	NVIC_InitStructure.NVIC_IRQChannel = USART_RS485_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	USART_ClearITPendingBit(USART_RS485, USART_IT_RXNE);
	USART_ITConfig(USART_RS485, USART_IT_RXNE, ENABLE);

	/* Enable USART */
	USART_Cmd(USART_RS485, ENABLE);
}

void io_uart_rs485_transfer(uint8_t c) {
	/* wait tx data buf empty */
	while (USART_GetFlagStatus(USART_RS485, USART_FLAG_TXE) == RESET);

	USART_SendData(USART_RS485, c);

	/* wait tx data transfer completed */
	while (USART_GetFlagStatus(USART_RS485, USART_FLAG_TC) == RESET);
}

uint8_t io_uart_rs485_receiver() {
	uint8_t c = 0;

	if(USART_GetITStatus(USART_RS485, USART_IT_RXNE) == SET) {
		USART_ClearITPendingBit(USART_RS485,USART_IT_RXNE);
		c = (uint8_t)USART_ReceiveData(USART_RS485);
	}

	return c;
}

/*****************************************************************************
 *io uart for rs485
******************************************************************************/
void io_rs485_dir_mode_input() {
	/* Configure rs485 dir mode input */
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd( RS485_DIR_IO_CLOCK, ENABLE);
	GPIO_InitStructure.GPIO_Mode	= GPIO_Mode_IN_FLOATING;
	GPIO_InitStructure.GPIO_Speed	= GPIO_Speed_10MHz;
	GPIO_InitStructure.GPIO_Pin		= RS485_DIR_IO_PIN;
	GPIO_Init(RS485_DIR_IO_PORT, &GPIO_InitStructure);
}

void io_rs485_dir_mode_output() {
	/* Configure rs485 dir mode input */
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd( RS485_DIR_IO_CLOCK, ENABLE);
	GPIO_InitStructure.GPIO_Mode	= GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed	= GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Pin		= RS485_DIR_IO_PIN;
	GPIO_Init(RS485_DIR_IO_PORT, &GPIO_InitStructure);
}

void io_rs485_dir_low() {
	GPIO_ResetBits(RS485_DIR_IO_PORT, RS485_DIR_IO_PIN);
}

void io_rs485_dir_high() {
	GPIO_SetBits(RS485_DIR_IO_PORT, RS485_DIR_IO_PIN);
}

/*****************************************************************************
 *io timer2 for interrupt
******************************************************************************/
void TIM2_Configuration() // 10ms -> interrupt
{
	/*Configure using library*/
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;

	NVIC_InitTypeDef NVIC_InitStructure;
	/*Enable TIM2 clock*/
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
	/*TIM2 Configuration at 2Hz*/
	// Update Event (Hz) = time_clock / ((TIM_Prescaler + 1) * (TIM_Period  + 1))
	// Update Event (Hz) = 72MHz / ((3599 + 1) * (9999 + 1)) = 2Hz (0.5s)
	// Fc(xung clock cua timer) = SystemCoreClock / TIM_Prescaler;
	TIM_DeInit(TIM2);
	TIM_TimeBaseInitStructure.TIM_Prescaler = 719;
	TIM_TimeBaseInitStructure.TIM_Period = 9999;
	TIM_TimeBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseInitStructure);

	TIM_ClearITPendingBit(TIM2, TIM_IT_Update);  // clear update flag
	TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE); // enable Update Interrupt
	TIM_Cmd(TIM2, ENABLE); //enable timer 2

	/*Configure Interrupt request for TIM2*/
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);
	NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}

void TIM2_IRQHandler()
{
	if(TIM_GetITStatus(TIM2, TIM_IT_Update)!=RESET) //if update flag turns on
	{
		// led toggle at 0.5s
		//led_test_on();
		TIM_ClearITPendingBit(TIM2, TIM_IT_Update); //clear update flag
	}
}

void timer2_enable() {
	ENTRY_CRITICAL();
	TIM_Cmd(TIM2, ENABLE);
	EXIT_CRITICAL();
}

void timer2_disable() {
	ENTRY_CRITICAL();
	TIM_Cmd(TIM2, DISABLE);
	EXIT_CRITICAL();
}


void timer_1000ms_init()
{
	/*Configure using library*/
	TIM_TimeBaseInitTypeDef timer_1000ms;

	NVIC_InitTypeDef NVIC_InitStructure;
	/*Enable TIM2 clock*/
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
	/*TIM2 Configuration at 2Hz*/
	// Update Event (Hz) = time_clock / ((TIM_Prescaler + 1) * (TIM_Period  + 1))
	// Update Event (Hz) = 72MHz / ((3599 + 1) * (9999 + 1)) = 2Hz (0.5s)
	// Fc(xung clock cua timer) = SystemCoreClock / TIM_Prescaler;
	TIM_DeInit(TIM3);
	timer_1000ms.TIM_Prescaler = 7199;
	timer_1000ms.TIM_Period = 9999;
	timer_1000ms.TIM_ClockDivision = TIM_CKD_DIV1;
	timer_1000ms.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM3, &timer_1000ms);

	TIM_ClearITPendingBit(TIM3, TIM_IT_Update);  // clear update flag
	TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE); // enable Update Interrupt
	TIM_Cmd(TIM3, ENABLE); //enable timer 2

	/*Configure Interrupt request for TIM2*/
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);
	NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}

void timer_1000ms_irq()
{
	if(TIM_GetITStatus(TIM3, TIM_IT_Update)!=RESET) //if update flag turns on
	{
		//led_test_on(); // led toggle at 1s
		TIM_ClearITPendingBit(TIM3, TIM_IT_Update); //clear update flag
	}
}

void timer_1000ms_enable() {
	ENTRY_CRITICAL();
	TIM_Cmd(TIM3, ENABLE);
	EXIT_CRITICAL();
}

void timer_1000ms_disable() {
	ENTRY_CRITICAL();
	TIM_Cmd(TIM3, DISABLE);
	EXIT_CRITICAL();
}

/*****************************************************************************
 *io uart for rs485
******************************************************************************/
/*io sht clk config*/
void io_sht_clk_mode_input() {
	/* Configure rs485 dir mode input */
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd( SHT_SCK_CLOCK, ENABLE);
	GPIO_InitStructure.GPIO_Mode	= GPIO_Mode_IN_FLOATING;
	GPIO_InitStructure.GPIO_Speed	= GPIO_Speed_10MHz;
	GPIO_InitStructure.GPIO_Pin		= SHT_SCK_PIN;
	GPIO_Init(SHT_SCK_PORT, &GPIO_InitStructure);
}

void io_sht_clk_mode_output() {
	/* Configure rs485 dir mode input */
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd( SHT_SCK_CLOCK, ENABLE);
	GPIO_InitStructure.GPIO_Mode	= GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed	= GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Pin		= SHT_SCK_PIN;
	GPIO_Init(SHT_SCK_PORT, &GPIO_InitStructure);
}

void io_sht_clk_low() {
	GPIO_ResetBits(SHT_SCK_PORT, SHT_SCK_PIN);
}

void io_sht_clk_high() {
	GPIO_SetBits(SHT_SCK_PORT, SHT_SCK_PIN);
}

int io_sht_clk_read() {
	return (int)GPIO_ReadInputDataBit(SHT_SCK_PORT, SHT_SCK_PIN);
}

/*io sht data 1 config*/
void io_sht_data1_mode_input() {
	/* Configure rs485 dir mode input */
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd( SHT_DATA_CLOCK, ENABLE);
	GPIO_InitStructure.GPIO_Mode	= GPIO_Mode_IPU;
	GPIO_InitStructure.GPIO_Speed	= GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Pin		= SHT_DATA1_PIN;
	GPIO_Init(SHT_DATA_PORT, &GPIO_InitStructure);
}

void io_sht_data1_mode_output() {
	/* Configure rs485 dir mode input */
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd( SHT_DATA_CLOCK, ENABLE);
	GPIO_InitStructure.GPIO_Mode	= GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed	= GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Pin		= SHT_DATA1_PIN;
	GPIO_Init(SHT_DATA_PORT, &GPIO_InitStructure);
}

void io_sht_data1_low() {
	GPIO_ResetBits(SHT_DATA_PORT, SHT_DATA1_PIN);
}

void io_sht_data1_high() {
	GPIO_SetBits(SHT_DATA_PORT, SHT_DATA1_PIN);
}

int io_sht_data1_read() {
	return (int)GPIO_ReadInputDataBit(SHT_DATA_PORT, SHT_DATA1_PIN);
}

/*io sht data 2 config*/
void io_sht_data2_mode_input() {
	/* Configure rs485 dir mode input */
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd( SHT_DATA_CLOCK, ENABLE);
	GPIO_InitStructure.GPIO_Mode	= GPIO_Mode_IN_FLOATING;
	GPIO_InitStructure.GPIO_Speed	= GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Pin		= SHT_DATA2_PIN;
	GPIO_Init(SHT_DATA_PORT, &GPIO_InitStructure);
}

void io_sht_data2_mode_output() {
	/* Configure rs485 dir mode input */
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd( SHT_DATA_CLOCK, ENABLE);
	GPIO_InitStructure.GPIO_Mode	= GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed	= GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Pin		= SHT_DATA2_PIN;
	GPIO_Init(SHT_DATA_PORT, &GPIO_InitStructure);
}

void io_sht_data2_low() {
	GPIO_ResetBits(SHT_DATA_PORT, SHT_DATA2_PIN);
}

void io_sht_data2_high() {
	GPIO_SetBits(SHT_DATA_PORT, SHT_DATA2_PIN);
}

int io_sht_data2_read() {
	return (int)GPIO_ReadInputDataBit(SHT_DATA_PORT, SHT_DATA2_PIN);
}

/*io sht data 3 config*/
void io_sht_data3_mode_input() {
	/* Configure rs485 dir mode input */
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd( SHT_DATA_CLOCK, ENABLE);
	GPIO_InitStructure.GPIO_Mode	= GPIO_Mode_IN_FLOATING;
	GPIO_InitStructure.GPIO_Speed	= GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Pin		= SHT_DATA3_PIN;
	GPIO_Init(SHT_DATA_PORT, &GPIO_InitStructure);
}

void io_sht_data3_mode_output() {
	/* Configure rs485 dir mode input */
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd( SHT_DATA_CLOCK, ENABLE);
	GPIO_InitStructure.GPIO_Mode	= GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed	= GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Pin		= SHT_DATA3_PIN;
	GPIO_Init(SHT_DATA_PORT, &GPIO_InitStructure);
}

void io_sht_data3_low() {
	GPIO_ResetBits(SHT_DATA_PORT, SHT_DATA3_PIN);
}

void io_sht_data3_high() {
	GPIO_SetBits(SHT_DATA_PORT, SHT_DATA3_PIN);
}

int io_sht_data3_read() {
	return (int)GPIO_ReadInputDataBit(SHT_DATA_PORT, SHT_DATA3_PIN);
}

/*io sht data 4 config*/
void io_sht_data4_mode_input() {
	/* Configure rs485 dir mode input */
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd( SHT_DATA_CLOCK, ENABLE);
	GPIO_InitStructure.GPIO_Mode	= GPIO_Mode_IN_FLOATING;
	GPIO_InitStructure.GPIO_Speed	= GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Pin		= SHT_DATA4_PIN;
	GPIO_Init(SHT_DATA_PORT, &GPIO_InitStructure);
}

void io_sht_data4_mode_output() {
	/* Configure rs485 dir mode input */
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd( SHT_DATA_CLOCK, ENABLE);
	GPIO_InitStructure.GPIO_Mode	= GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed	= GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Pin		= SHT_DATA4_PIN;
	GPIO_Init(SHT_DATA_PORT, &GPIO_InitStructure);
}

void io_sht_data4_low() {
	GPIO_ResetBits(SHT_DATA_PORT, SHT_DATA4_PIN);
}

void io_sht_data4_high() {
	GPIO_SetBits(SHT_DATA_PORT, SHT_DATA4_PIN);
}

int io_sht_data4_read() {
	return (int)GPIO_ReadInputDataBit(SHT_DATA_PORT, SHT_DATA4_PIN);
}

/******************************************************************************
* internal flash function
*******************************************************************************/
#define CR_PG_Set_                ((uint32_t)0x00000001)
#define CR_PG_Reset_              ((uint32_t)0x00001FFE)
void erase_internal_flash(uint32_t address, uint32_t len) {
	uint32_t page_number;
	uint32_t index;

	page_number = len / 256;

	if ((page_number * 256) < len) {
		page_number++;
	}

	for (index = 0; index < page_number; index++) {
		FLASH_ErasePage(address + (index * 256));
	}
}

void internal_flash_unlock() {
	FLASH_Unlock();
	FLASH_ClearFlag(FLASH_FLAG_PGERR|FLASH_FLAG_WRPRTERR | FLASH_FLAG_EOP);
}

void internal_flash_lock() {
	FLASH_Lock();
}

void internal_flash_erase_pages_cal(uint32_t address, uint32_t len) {
	uint32_t page_number;
	uint32_t index;

	page_number = len / 256;

	if ((page_number * 256) < len) {
		page_number++;
	}

	for (index = 0; index < page_number; index++) {
		FLASH_ErasePage(address + (index * 256));
	}
	/* set bit PG for new flash program */
	FLASH->CR |= CR_PG_Set_;
}

uint8_t internal_flash_write_cal(uint32_t address, uint8_t* data, uint32_t len) {
	uint32_t temp;
	uint32_t index = 0;
	FLASH_Status flash_status = FLASH_BUSY;

	while (index < len) {
		temp = 0;

		memcpy(&temp, &data[index], (len - index) >= sizeof(uint32_t) ? sizeof(uint32_t) : (len - index));

		flash_status = FLASH_ProgramWord(address + index, temp);

		if(flash_status == FLASH_COMPLETE) {
			index += sizeof(uint32_t);
		}
		else {
			/* reset bit PG for fail flash program */
			FLASH->CR |= CR_PG_Reset_;
			FLASH_ClearFlag(FLASH_FLAG_PGERR|FLASH_FLAG_WRPRTERR | FLASH_FLAG_EOP);
		}
	}

	return 1;
}

uint32_t sys_ctr_get_vbat_voltage(/*stm32l1xx*/) {
#define VREFINT_CAL_ADDR (uint16_t*)(0x1FF80078)
	uint16_t vref_data = 0, vref_cal;
	uint32_t vbatX1000;

	ADC_TempSensorVrefintCmd(ENABLE);
	ADC_RegularChannelConfig(ADC1, ADC_Channel_Vrefint, 1, ADC_SampleTime_239Cycles5);

	while (ADC_GetFlagStatus(ADC1, ADC_CR2_ADON) == RESET);
	sys_ctrl_delay_ms(10);

	ADC_SoftwareStartConvCmd(ADC1, ENABLE);
	while (ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET);

	vref_data += ADC_GetConversionValue(ADC1);

	vref_cal = *VREFINT_CAL_ADDR;
	vbatX1000 = (uint32_t)(3000.0 * (float)(vref_cal) / (float)vref_data);

	return vbatX1000;
}

void flash_cs_low() {
}

void flash_cs_high() {
}

uint8_t flash_transfer(uint8_t data) {
	return 0;
}

/******************************************************************************
* led status function
*******************************************************************************/
void rst_master_init() {
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(RST_MASTER_IO_CLOCK , ENABLE);

	rst_master_on();
	GPIO_InitStructure.GPIO_Pin		= RST_MASTER_IO_PIN;
	GPIO_InitStructure.GPIO_Mode	= GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed	= GPIO_Speed_2MHz;
	GPIO_Init(RST_MASTER_IO_PORT, &GPIO_InitStructure);
}

void rst_master_on() {
	GPIO_SetBits(RST_MASTER_IO_PORT, RST_MASTER_IO_PIN);
}

void rst_master_off() {
	GPIO_ResetBits(RST_MASTER_IO_PORT, RST_MASTER_IO_PIN);
}

/*	------------------------------------ ROBOT DELTA - SCARA FUNCTION --------------------*/
/*	--------------------------------------------------------------------------------------*/

/******************************************************************************
*						Timer1 -> Control pwm
*						TIM1	-->	PB14
*******************************************************************************/
void timer1_control_pwm_cfg() {
	GPIO_InitTypeDef GPIO_InitStructure;
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
	TIM_OCInitTypeDef TIM_OCInitStructure;

	/*Enable clock*/
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);

	/*Configure PWM Pin out TIM1 - CH2N*/
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_14;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	/*
	 * TIM_Prescaler = (Timer input clock/Counter frequency) - 1
	 * Counter frequency = Frequency Required x Pwm Resolution(Steps)
	 * Note: + Timer input clock for Timer 1, 2, 3, 4 is 72MHz
			 + TIM_Prescaler, TIM_Period, Pwm Resolution is a 16-bit resistor so their value must not exceed 65535.
	 * Ex: TIM_Prescaler = (72MHz / (1KHz * 1000(picked a random value))) - 1 = 71
		   TIM_Period (Pwm Resolution) = 999 (count 0 - 999)*/

	TIM_TimeBaseStructure.TIM_Prescaler = 72 - 1;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseStructure.TIM_Period = 1000 - 1;
	TIM_TimeBaseStructure.TIM_ClockDivision = 0x0;
	TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;
	TIM_TimeBaseInit(TIM1, &TIM_TimeBaseStructure);

	/*Configure PWM*/
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
	// TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Enable;
	TIM_OCInitStructure.TIM_Pulse = 1000;
	// TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OCInitStructure.TIM_OCNPolarity = TIM_OCNPolarity_High;
	// TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Reset;
	TIM_OCInitStructure.TIM_OCNIdleState = TIM_OCNIdleState_Reset;

	/*PWM Channel 2*/
	TIM_OC2Init(TIM1, &TIM_OCInitStructure);
	TIM_SelectOCxM(TIM1, TIM_Channel_2, TIM_OCMode_PWM1);
	// TIM_CCxCmd(TIM1, TIM_Channel_2, TIM_CCx_Enable);
	TIM_CCxNCmd(TIM1, TIM_Channel_2, TIM_CCxN_Enable);

	/*Enable PWM_TIM*/
	TIM_Cmd(TIM1, ENABLE);
	/* Main Output Enable */
	TIM_CtrlPWMOutputs(TIM1, ENABLE);
}

/******************************************************************************
* Timer2 -> 20ms interrupt for read speed and position for Encoder
*******************************************************************************/
void timer2_20ms_cfg() {
	TIM_TimeBaseInitTypeDef	timer_20ms;
	NVIC_InitTypeDef        NVIC_InitStructure;

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

	/*	TIM_Prescaler = ((SystemCoreClock/n) / F_timer) - 1
	*	T = (TIM_Period * TIM_Prescaler) / (SystemCoreClock/n)
		10ms = 0.01s = (100 * TIM_Prescaler) / 72*10^6
	=> TIM_Prescaler = 7200 => F_timer = 10KHz					*/

	TIM_DeInit(TIM2);
	timer_20ms.TIM_Prescaler = 14399;		/* prescale = 14400 -> fc = 5Khz*/
	timer_20ms.TIM_Period = 100 - 1;
	timer_20ms.TIM_ClockDivision = 0;
	timer_20ms.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM2, &timer_20ms);

	TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
	TIM_ClearFlag(TIM2, TIM_FLAG_Update);
	TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
	TIM_Cmd(TIM2,ENABLE);

	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);

	NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}

/******************************************************************************
*						Timer3 --> read encoder
*						ENCODER_CHANEL_A --> PA6
*						ENCODER_CHANEL_B --> PA7
*******************************************************************************/

void timer3_read_encoder_cfg() {
	TIM_ICInitTypeDef			TIM_ICInitStructure;
	GPIO_InitTypeDef 			GPIO_InitStructure;
	TIM_TimeBaseInitTypeDef 	TIM_TimeBaseStructure;

	/*Enable clock for timers and GPIO ports */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);

	/*Configure for Encoder interface 1*/
	GPIO_InitStructure.GPIO_Pin = ENCODER_CHANEL_A | ENCODER_CHANEL_B ;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(ENCODER_IO_PORT, &GPIO_InitStructure);

	TIM_TimeBaseStructure.TIM_Prescaler = 0x0000;
	TIM_TimeBaseStructure.TIM_Period = 0xFFFF;
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);

	/*Configure Encoder Interface 1 capture channel*/
	TIM_ICInitStructure.TIM_Channel = TIM_Channel_1 | TIM_Channel_2;
	TIM_ICInitStructure.TIM_ICPolarity = TIM_ICPolarity_Falling;
	TIM_ICInitStructure.TIM_ICFilter = 15;
	TIM_ICInitStructure.TIM_ICSelection = TIM_ICSelection_DirectTI;
	TIM_ICInit(TIM3, &TIM_ICInitStructure);

	TIM_EncoderInterfaceConfig(TIM3, TIM_EncoderMode_TI12, TIM_ICPolarity_Falling, TIM_ICPolarity_Falling);

	TIM_SetCounter(TIM3, 30000);

	TIM_Cmd(TIM3, ENABLE);

	TIM_ClearFlag(TIM3, TIM_FLAG_Update);

	TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);
}
/*****************************************************************************
 *						DC Servo direct pin
 *					MOTOR_1_IO_PIN_DIRECT --> PB3
******************************************************************************/
void motor_dir_output_init() {
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(MOTOR_IO_CLOCK_DIRECT , ENABLE);

	GPIO_InitStructure.GPIO_Pin		= MOTOR_1_IO_PIN_DIRECT;
	GPIO_InitStructure.GPIO_Mode	= GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed	= GPIO_Speed_50MHz;
	GPIO_Init(MOTOR_1_IO_PORT_DIRECT, &GPIO_InitStructure);

	// GPIO_InitStructure.GPIO_Pin		= MOTOR_2_IO_PIN_DIRECT;
	// GPIO_InitStructure.GPIO_Mode	= GPIO_Mode_Out_PP;
	// GPIO_InitStructure.GPIO_Speed	= GPIO_Speed_50MHz;
	// GPIO_Init(MOTOR_2_IO_PORT_DIRECT, &GPIO_InitStructure);
	//
	// GPIO_InitStructure.GPIO_Pin		= MOTOR_3_IO_PIN_DIRECT;
	// GPIO_InitStructure.GPIO_Mode	= GPIO_Mode_Out_PP;
	// GPIO_InitStructure.GPIO_Speed	= GPIO_Speed_50MHz;
	// GPIO_Init(MOTOR_3_IO_PORT_DIRECT, &GPIO_InitStructure);
}

/******************************************************************************
*					Func for Control Direct and Speed of DC Servo
*******************************************************************************/
void Turn_Release(int16_t speed) {

	if(speed >= 0) {
		GPIO_SetBits(MOTOR_1_IO_PORT_DIRECT, MOTOR_1_IO_PIN_DIRECT);
		TIM1->CCR2 = 1000 - speed;
	}

	if(speed < 0) {
		GPIO_ResetBits(MOTOR_1_IO_PORT_DIRECT, MOTOR_1_IO_PIN_DIRECT);
		TIM1->CCR2 = 1000 - (-speed);
	}

}

/********************************************************************************
*																				*
*				 Dir: huong cua dong co -> 1 :dem len, 0 : dem xuong			*
*																				*
*       *Cnt = TIMx->CNT;  <==> TIM_GetCounter(TIMx);  ->  dem encoder			*
*																				*
*				*Save_vitri_feedback -> con tro toi vi tri doc duoc				*
*																				*
*********************************************************************************
*/
void ENCODER_Read_Release(float* Save_vitri_feedback, uint16_t* Dir, uint16_t* Cnt, TIM_TypeDef* TIMx, uint8_t type) {

	*Dir = ((TIMx->CR1 & TIM_CR1_DIR)? (0) : 1);         // huong cua dong co //1 la dem len, 0 la dem xuong

	*Cnt = TIMx->CNT;     //TIM_GetCounter(TIMx);       // dem encoder

	switch (type) {
	case 1:
		*Save_vitri_feedback = (float)((*Cnt - 30000) * 360) / 10800;              // export to degree for JGB37-545
		break;

	case 2:
		*Save_vitri_feedback = (float)((*Cnt - 30000) * 360) / 1320;              // export to degree for JGB37-520
		break;

	case 3:
		*Save_vitri_feedback = (float)((*Cnt - 30000) * 360) / 3696;              // export to degree for JGB37-545-110rpm
		break;

	default:
		break;
	}

	//*Save_speed_feedback = (float)((((*Cnt) - (*pre_Cnt)) / 10800) * (60 / 0.01));

}

