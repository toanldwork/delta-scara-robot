/**
 ******************************************************************************
 * @Author: ThanNT
 * @Date:   05/09/2016
 ******************************************************************************
**/
#ifndef __SYSTEM_H__
#define __SYSTEM_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include "system_stm32f10x.h"
#include "stm32f10x.h"
#include "stm32f10x_conf.h"
#include "core_cm3.h"

typedef struct {
	uint32_t cpu_clock;
	uint32_t tick;
	uint32_t console_baudrate;
	uint32_t flash_used;
	uint32_t ram_used;
	uint32_t data_used;
	uint32_t stack_used;
	uint32_t heap_size;
} system_info_t;

extern system_info_t system_info;

/* PARAM PID FOR SCARA */
#define PARAM_PID_SCARA_DC_1			0
#define PARAM_PID_SCARA_DC_2			0
#define PARAM_PID_SCARA_DC_3			1
#define PARAM_PID_SCARA_DC_4			0

/* PARAM PID FOR DELTA */
#define PARAM_PID_DELTA_DC				0

#define min_cus(a, b)  ({  __typeof__ (a) _a = (a); __typeof__ (b) _b = (b); _a > _b ? _b : _a; })

#define max_cus(a, b) 	({  __typeof__ (a) _a = (a);	__typeof__ (b) _b = (b); _a > _b ? _a : _b; })

#define constrain_cus(x, lower_limit, upper_limit)   ({ __typeof__ (x) _x = (x); __typeof__ (lower_limit) _lower_limit = (lower_limit);  __typeof__ (upper_limit) _upper_limit = (upper_limit);  min_cus(max_cus(_x, _lower_limit), _upper_limit); })

#define ABS(n)     (((n) < 0) ? -(n) : (n))

#if (PARAM_PID_SCARA_DC_1 == 1)
#define kp_545		70
#define ki_545		0
#define kd_545		0
#elif (PARAM_PID_SCARA_DC_2 == 1)
#define kp_545		5
#define ki_545		0.2
#define kd_545		3
#elif (PARAM_PID_SCARA_DC_3 == 1)
#define kp_545		40
#define ki_545		0
#define kd_545		0
#elif (PARAM_PID_SCARA_DC_4 == 1)
#define kp_545		1
#define ki_545		0
#define kd_545		0.1
#endif



#if (PARAM_PID_DELTA_DC == 1)
#define kp_545		35
#define ki_545		0
#define kd_545		0
#endif





#define kp_520		0.01
#define ki_520		0
#define kd_520		3

#define DEBUG_ENCODER	1

extern __IO float pid_feeback1, pid_feeback2, pid_feeback3;

void PID_DeltaRobot(__IO float* Save_PID_Feeback, float deta_run, float t_run, float current, float kp, float ki, float kd);
void PID_DIGITAL(__IO float* Save_PID_Feeback, uint16_t Target, uint16_t Current, float kp, float ki, float kd);



#ifdef __cplusplus
}
#endif

#endif //__SYSTEM_H__
