-include sources/platform/stm32f10x/arduino/cores/stm32/Makefile.mk

# header path
CPPFLAGS	+= -I./sources/platform/stm32f10x/arduino/cores

# sources path
VPATH += sources/platform/stm32f10x/arduino/cores

# cpp files
SOURCES_CPP += sources/platform/stm32f10x/arduino/cores/wiring_digital.cpp
SOURCES_CPP += sources/platform/stm32f10x/arduino/cores/wiring_shift.cpp
SOURCES_CPP += sources/platform/stm32f10x/arduino/cores/Print.cpp
SOURCES_CPP += sources/platform/stm32f10x/arduino/cores/Stream.cpp
SOURCES_CPP += sources/platform/stm32f10x/arduino/cores/WString.cpp
SOURCES_CPP += sources/platform/stm32f10x/arduino/cores/WMath.cpp
SOURCES_CPP += sources/platform/stm32f10x/arduino/cores/itoa.cpp
SOURCES_CPP += sources/platform/stm32f10x/arduino/cores/IPAddress.cpp
