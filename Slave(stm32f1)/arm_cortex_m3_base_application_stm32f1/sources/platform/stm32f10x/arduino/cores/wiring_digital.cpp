#include "Arduino.h"
#include "io_cfg.h"
#include "sys_dbg.h"

void pinMode(uint8_t pin, uint8_t mode) {
	switch (pin) {
	case A_RS485_DIR_PIN: {
		if (mode == OUTPUT) {
			io_rs485_dir_mode_output();
		}
		else {
			io_rs485_dir_mode_input();
		}
	}
		break;

	case A_SHT_CLK_PIN: {
		if (mode == OUTPUT) {
			io_sht_clk_mode_output();
		}
		else {
			io_sht_clk_mode_input();
		}
	}
		break;

	case A_SHT_DATA1_PIN: {
		if (mode == OUTPUT) {
			io_sht_data1_mode_output();
		}
		else {
			io_sht_data1_mode_input();
		}
	}
		break;

	case A_SHT_DATA2_PIN: {
		if (mode == OUTPUT) {
			io_sht_data2_mode_output();
		}
		else {
			io_sht_data2_mode_input();
		}
	}
		break;

	case A_SHT_DATA3_PIN: {
		if (mode == OUTPUT) {
			io_sht_data3_mode_output();
		}
		else {
			io_sht_data3_mode_input();
		}
	}
		break;

	case A_SHT_DATA4_PIN: {
		if (mode == OUTPUT) {
			io_sht_data4_mode_output();
		}
		else {
			io_sht_data4_mode_input();
		}
	}
		break;

	default:
		FATAL("AR", 0xF1);
		break;
	}
}

void digitalWrite(uint8_t pin, uint8_t val) {
	switch (pin) {
	case A_RS485_DIR_PIN: {
		if (val == LOW) {
			io_rs485_dir_low();
		}
		else {
			io_rs485_dir_high();
		}
	}
		break;

	case A_SHT_CLK_PIN: {
		if (val == LOW) {
			io_sht_clk_low();
		}
		else {
			io_sht_clk_high();
		}
	}
		break;

	case A_SHT_DATA1_PIN: {
		if (val == LOW) {
			io_sht_data1_low();
		}
		else {
			io_sht_data1_high();
		}
	}
		break;

	case A_SHT_DATA2_PIN: {
		if (val == LOW) {
			io_sht_data2_low();
		}
		else {
			io_sht_data2_high();
		}
	}
		break;

	case A_SHT_DATA3_PIN: {
		if (val == LOW) {
			io_sht_data3_low();
		}
		else {
			io_sht_data3_high();
		}
	}
		break;

	case A_SHT_DATA4_PIN: {
		if (val == LOW) {
			io_sht_data4_low();
		}
		else {
			io_sht_data4_high();
		}
	}
		break;

	default:
		FATAL("AR", 0xF2);
		break;
	}
}

int digitalRead(uint8_t pin) {
	int val = 0;

	switch (pin) {
	case A_RS485_DIR_PIN: {
	}
		break;

	case A_SHT_CLK_PIN: {
		val = io_sht_clk_read();
	}
		break;

	case A_SHT_DATA1_PIN: {
		val = io_sht_data1_read();
	}
		break;

	case A_SHT_DATA2_PIN: {
		val = io_sht_data2_read();
	}
		break;

	case A_SHT_DATA3_PIN: {
		val = io_sht_data3_read();
	}
		break;

	case A_SHT_DATA4_PIN: {
		val = io_sht_data4_read();
	}
		break;

	default:
		FATAL("AR", 0xF3);
		break;
	}

	return val;
}
