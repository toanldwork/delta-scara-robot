/**
 ******************************************************************************
 * @Author: ThanNT
 * @Date:   05/09/2016
 * @Update:
 * @AnhHH: Add io function for sth11 sensor.
 ******************************************************************************
**/
#ifndef __IO_CFG_H__
#define __IO_CFG_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>

#include "stm32f10x.h"
#include "stm32f10x_conf.h"
#include "system_stm32f10x.h"
#include "core_cm3.h"

/*
 * define pin for arduino pinMode/digitalWrite/digitalRead
 * NOTE: define value MUST be deferrent
 */
/******************************************************************************
 *Pin map use arduino lib
*******************************************************************************/
#define A_RS485_DIR_PIN					(1)
#define A_SHT_CLK_PIN					(2)
#define A_SHT_DATA1_PIN					(3)
#define A_SHT_DATA2_PIN					(4)
#define A_SHT_DATA3_PIN					(5)
#define A_SHT_DATA4_PIN					(6)

/*****************************************************************************
 *Pin map led life
******************************************************************************/
#define LED_LIFE_IO_PIN					(GPIO_Pin_13)
#define LED_LIFE_IO_PORT				(GPIOC)
#define LED_LIFE_IO_CLOCK				(RCC_APB2Periph_GPIOC)

/****************************************************************************
 *Pin map SHT
*****************************************************************************/
#define SHT_SCK_CLOCK					(RCC_APB2Periph_GPIOA)
#define SHT_SCK_PORT					(GPIOA)
#define SHT_SCK_PIN						(GPIO_Pin_8)

#define SHT_DATA_CLOCK					(RCC_APB2Periph_GPIOB)
#define SHT_DATA_PORT					(GPIOB)
#define SHT_DATA1_PIN					(GPIO_Pin_15)
#define SHT_DATA2_PIN					(GPIO_Pin_14)
#define SHT_DATA3_PIN					(GPIO_Pin_13)
#define SHT_DATA4_PIN					(GPIO_Pin_12)

/****************************************************************************
 *Pin map CT sensor
*****************************************************************************/
#ifdef VER1
#define CT_ADC_CLOCK					(RCC_APB2Periph_ADC1)
#define CT_ADC_IO_CLOCK					(RCC_APB2Periph_GPIOA)
#define CT_ADC_PORT						(GPIOA)

#define CT1_ADC_CHANEL					(ADC_Channel_0)
#define CT2_ADC_CHANEL					(ADC_Channel_1)

#define CT1_ADC_PIN						(GPIO_Pin_0)
#define CT2_ADC_PIN						(GPIO_Pin_1)

#define CT1_SL_PIN						(GPIO_Pin_11)
#define CT2_SL_PIN						(GPIO_Pin_12)
#else
#define CT_ADC_CLOCK					(RCC_APB2Periph_ADC1)

#define CT1_IO_CLOCK					(RCC_APB2Periph_GPIOA)
#define CT1_IO_PORT						(GPIOA)
#define CT1_IO_PIN						(GPIO_Pin_0)
#define CT1_ADC_CHAN					(ADC_Channel_0)

#define CT2_IO_CLOCK					(RCC_APB2Periph_GPIOA)
#define CT2_IO_PORT						(GPIOA)
#define CT2_IO_PIN						(GPIO_Pin_5)
#define CT2_ADC_CHAN					(ADC_Channel_5)

#define CT3_IO_CLOCK					(RCC_APB2Periph_GPIOA)
#define CT3_IO_PORT						(GPIOA)
#define CT3_IO_PIN						(GPIO_Pin_7)
#define CT3_ADC_CHAN					(ADC_Channel_7)

#define CT4_IO_CLOCK					(RCC_APB2Periph_GPIOB)
#define CT4_IO_PORT						(GPIOB)
#define CT4_IO_PIN						(GPIO_Pin_1)
#define CT4_ADC_CHAN					(ADC_Channel_9)

#define CT5_IO_CLOCK					(RCC_APB2Periph_GPIOA)
#define CT5_IO_PORT						(GPIOA)
#define CT5_IO_PIN						(GPIO_Pin_1)
#define CT5_ADC_CHAN					(ADC_Channel_1)

#define CT6_IO_CLOCK					(RCC_APB2Periph_GPIOA)
#define CT6_IO_PORT						(GPIOA)
#define CT6_IO_PIN						(GPIO_Pin_4)
#define CT6_ADC_CHAN					(ADC_Channel_4)

#define CT7_IO_CLOCK					(RCC_APB2Periph_GPIOA)
#define CT7_IO_PORT						(GPIOA)
#define CT7_IO_PIN						(GPIO_Pin_6)
#define CT7_ADC_CHAN					(ADC_Channel_6)

#define CT8_IO_CLOCK					(RCC_APB2Periph_GPIOB)
#define CT8_IO_PORT						(GPIOB)
#define CT8_IO_PIN						(GPIO_Pin_0)
#define CT8_ADC_CHAN					(ADC_Channel_8)

#endif

/****************************************************************************
 *UART interface data register
*****************************************************************************/
#define USART_IF						(USART2)
#define USART_IF_CLK					(RCC_APB1Periph_USART2)
#define USART_IF_IRQn					(USART2_IRQn)

#define USART_IF_TX_PIN					(GPIO_Pin_2)
#define USART_IF_TX_GPIO_PORT			(GPIOA)
#define USART_IF_TX_GPIO_CLK			(RCC_APB2Periph_GPIOA)
#define USART_IF_TX_SOURCE				(GPIO_PinSource2)

#define USART_IF_RX_PIN					(GPIO_Pin_3)
#define USART_IF_RX_GPIO_PORT			(GPIOA)
#define USART_IF_RX_GPIO_CLK			(RCC_APB2Periph_GPIOA)
#define USART_IF_RX_SOURCE				(GPIO_PinSource3)

/****************************************************************************
 *UART RS485 - RS485 dir io config
*****************************************************************************/
#define USART_RS485						(USART3)
#define USART_RS485_CLK					(RCC_APB1Periph_USART3)
#define USART_RS485_IRQn				(USART3_IRQn)

#define USART_RS485_TX_PIN				(GPIO_Pin_10)
#define USART_RS485_TX_GPIO_PORT		(GPIOB)
#define USART_RS485_TX_GPIO_CLK			(RCC_APB2Periph_GPIOB)
#define USART_RS485_TX_SOURCE			(GPIO_PinSource10)

#define USART_RS485_RX_PIN				(GPIO_Pin_11)
#define USART_RS485_RX_GPIO_PORT		(GPIOB)
#define USART_RS485_RX_GPIO_CLK			(RCC_APB2Periph_GPIOB)
#define USART_RS485_RX_SOURCE			(GPIO_PinSource11)

/*RS485 dir IO*/
#define RS485_DIR_IO_PIN				(GPIO_Pin_12)
#define RS485_DIR_IO_PORT				(GPIOB)
#define RS485_DIR_IO_CLOCK				(RCC_APB2Periph_GPIOB)

/*****************************************************************************
 *Pin map led life
******************************************************************************/
#define RST_MASTER_IO_PIN				(GPIO_Pin_3)
#define RST_MASTER_IO_PORT				(GPIOB)
#define RST_MASTER_IO_CLOCK				(RCC_APB2Periph_GPIOB)

/*****************************************************************************
 *Pin map control motor - encoder
******************************************************************************/
#define MOTOR_1_IO_PIN							(GPIO_Pin_6)
#define MOTOR_1_IO_PORT							(GPIOA)
#define MOTOR_1_IO_CLOCK						(RCC_APB2Periph_GPIOA)

#define MOTOR_2_IO_PIN							(GPIO_Pin_7)
#define MOTOR_2_IO_PORT							(GPIOA)
#define MOTOR_2_IO_CLOCK						(RCC_APB2Periph_GPIOA)

#define MOTOR_3_IO_PIN							(GPIO_Pin_0)
#define MOTOR_3_IO_PORT							(GPIOB)
#define MOTOR_3_IO_CLOCK						(RCC_APB2Periph_GPIOB)

#define MOTOR_4_IO_PIN							(GPIO_Pin_1)
#define MOTOR_4_IO_PORT							(GPIOB)
#define MOTOR_4_IO_CLOCK						(RCC_APB2Periph_GPIOB)

#define MOTOR_1_IO_PIN_DIRECT					(GPIO_Pin_3)
#define MOTOR_1_IO_PORT_DIRECT					(GPIOB)

#define MOTOR_2_IO_PIN_DIRECT					(GPIO_Pin_4)
#define MOTOR_2_IO_PORT_DIRECT					(GPIOB)

#define MOTOR_3_IO_PIN_DIRECT					(GPIO_Pin_5)
#define MOTOR_3_IO_PORT_DIRECT					(GPIOB)

// #define MOTOR_4_IO_PIN_DIRECT					(GPIO_Pin_6)
// #define MOTOR_4_IO_PORT_DIRECT					(GPIOB)

#define MOTOR_IO_CLOCK_DIRECT					(RCC_APB2Periph_GPIOB)

#define ENCODER_CHANEL_A						(GPIO_Pin_6)
#define ENCODER_CHANEL_B						(GPIO_Pin_7)
#define ENCODER_IO_PORT							(GPIOA)

/*****************************************************************************
 *Pin map E18-D80NK NPN IR Sensor
******************************************************************************/
#define E18_IR_SENSOR_1_IO_PIN					(GPIO_Pin_7)
#define E18_IR_SENSOR_1_IO_PORT					(GPIOB)

#define E18_IR_SENSOR_2_IO_PIN					(GPIO_Pin_8)
#define E18_IR_SENSOR_2_IO_PORT					(GPIOB)

#define E18_IR_SENSOR_3_IO_PIN					(GPIO_Pin_9)
#define E18_IR_SENSOR_3_IO_PORT					(GPIOB)

#define E18_IR_SENSOR_4_IO_PIN					(GPIO_Pin_10)
#define E18_IR_SENSOR_4_IO_PORT					(GPIOB)

#define E18_IR_SENSOR_IO_CLOCK					(RCC_APB2Periph_GPIOB)

/*****************************************************************************
 *Pin map State Button
******************************************************************************/
#define STOP_EMERGENCY_BUTTON_IO_PIN			(GPIO_Pin_11)
#define STOP_EMERGENCY_BUTTON_IO_PORT			(GPIOB)

#define START_MODE_BUTTON_IO_PIN				(GPIO_Pin_12)
#define START_MODE_BUTTON_IO_PORT				(GPIOA)

#define STATE_BUTTON_IO_CLOCK					(RCC_APB2Periph_GPIOB)

/*****************************************************************************
 *Pin map Pump Motor
******************************************************************************/
#define PUMP_MOTOR_IO_PIN						(GPIO_Pin_13)
#define PUMP_MOTOR_IO_PORT						(GPIOB)

#define PUMP_MOTOR_IO_CLOCK						(RCC_APB2Periph_GPIOB)

/*****************************************************************************
 *Pin map Button Join
******************************************************************************/
#define BUTTON_JOIN_IO_PIN						(GPIO_Pin_14)
#define BUTTON_JOIN_IO_PORT						(GPIOB)

#define BUTTON_JOIN_IO_CLOCK					(RCC_APB2Periph_GPIOB)

/*****************************************************************************
 *Pin map Button Join
******************************************************************************/
#define LED_STATUS_IO_PIN						(GPIO_Pin_15)
#define LED_STATUS_IO_PORT						(GPIOB)

#define LED_STATUS_IO_CLOCK						(RCC_APB2Periph_GPIOB)

/*****************************************************************************
 *Pin map hc-sr04
******************************************************************************/
#define US_TIMER						(TIM3)

#define US_TRIG_PORT					(GPIOB)
#define US_TRIG_PIN						(GPIO_Pin_0)		//TIM Ch3 (trig output)

#define US_ECHO_PORT					(GPIOA)
#define US_ECHO_PIN						(GPIO_Pin_6)		//TIM Ch1 (echo input)
#define US_TIMER_TRIG_SOURCE			(TIM_TS_TI1FP1)
/******************************************************************************
* led status function
*******************************************************************************/
extern void led_life_init();
extern void led_life_on();
extern void led_life_off();

extern void led_zigbee_init();
extern void led_zigbee_on();
extern void led_zigbee_off();

/******************************************************************************
* button join function
*******************************************************************************/
extern void io_button_mode_init();
extern uint8_t io_button_mode_read();

/******************************************************************************
* hc-sr05 function
*******************************************************************************/
extern void echo_init();
extern void trig_init();
extern uint32_t hcsr05_read();
extern void initMeasureTimer();
extern void initPins();
extern int32_t HCSR04GetDistance();

/******************************************************************************
* adc function
* + themistor sensor
*
* Note: MUST be enable internal clock for adc module.
*******************************************************************************/
/* adc configure for CT sensor */
extern void io_adc_ct_cfg();
extern uint16_t io_adc_ct_read(uint8_t);
extern void io_adc_ct_select_chanel(uint8_t);

/*****************************************************************************
 *io uart for if_sl_cpu
******************************************************************************/
extern void io_uart_interface_cfg();
extern void io_uart_interface_transfer(uint8_t);
extern uint8_t io_uart_interface_receiver();

/*****************************************************************************
 *io uart for rs485-modbusRTU
******************************************************************************/
extern void io_uart_rs485_cfg(long);
extern void io_uart_rs485_transfer(uint8_t);
extern uint8_t io_uart_rs485_receiver();

extern void io_rs485_dir_mode_input();
extern void io_rs485_dir_mode_output();
extern void io_rs485_dir_low();
extern void io_rs485_dir_high();

/*****************************************************************************
 *io timer2 config
******************************************************************************/
extern void TIM2_Configuration();
extern void TIM2_IRQHandler();
extern void timer2_enable();
extern void timer2_disable();

/*****************************************************************************
 *io timer3 config
******************************************************************************/
extern void timer_1000ms_init();
extern void timer_1000ms_irq();
extern void timer_1000ms_enable();
extern void timer_1000ms_disable();

/*****************************************************************************
 *io solar panel cleaning robot config
******************************************************************************/
extern void io_pwm_timer3_control_cfg();
extern void motor_output_init();
extern void motor_1_control(uint8_t direct, uint16_t speed);
extern void motor_2_control(uint8_t direct, uint16_t speed);
extern void motor_3_control(uint8_t direct, uint16_t speed);

extern void E18_ir_sensor_input_init();
extern uint8_t read_E18_ir_sensor_1_pin();
extern uint8_t read_E18_ir_sensor_2_pin();

extern void state_button_init();
extern uint8_t read_stop_emer_button_pin();
extern uint8_t read_start_mode_button_pin();

extern void pump_motor_init();
extern void pump_motor_on();
extern void pump_motor_off();

/*****************************************************************************
 *io Robot Delta - Scara config
******************************************************************************/
extern void timer1_encoder_cfg();
extern void timer1_control_pwm_cfg();
extern void timer2_20ms_cfg();
extern void timer3_read_encoder_cfg();
extern void motor_dir_output_init();

extern void Turn_Release(int16_t speed);
extern void ENCODER_Read_Release(float* Save_vitri_feedback, uint16_t* Dir, uint16_t* Cnt, TIM_TypeDef* TIMx, uint8_t type);
/*****************************************************************************
 *io uart for SHT
******************************************************************************/
/*io sht clk config*/
void io_sht_clk_mode_input();
void io_sht_clk_mode_output();
void io_sht_clk_low();
void io_sht_clk_high();
int io_sht_clk_read();

/*io sht data 1 config*/
void io_sht_data1_mode_input();
void io_sht_data1_mode_output();
void io_sht_data1_low();
void io_sht_data1_high();
int io_sht_data1_read();

/*io sht data 2 config*/
void io_sht_data2_mode_input();
void io_sht_data2_mode_output();
void io_sht_data2_low();
void io_sht_data2_high();
int io_sht_data2_read();

/*io sht data 3 config*/
void io_sht_data3_mode_input();
void io_sht_data3_mode_output();
void io_sht_data3_low();
void io_sht_data3_high();
int io_sht_data3_read();

/*io sht data 4 config*/
void io_sht_data4_mode_input();
void io_sht_data4_mode_output();
void io_sht_data4_low();
void io_sht_data4_high();
int io_sht_data4_read();

/******************************************************************************
* internal flash function
*******************************************************************************/
extern void internal_flash_unlock();
extern void internal_flash_lock();
extern void internal_flash_erase_pages_cal(uint32_t address, uint32_t len);
extern uint8_t internal_flash_write_cal(uint32_t address, uint8_t* data, uint32_t len);

extern uint32_t sys_ctr_get_vbat_voltage();

/******************************************************************************
* master reset pin function
*******************************************************************************/
extern void rst_master_init();
extern void rst_master_on();
extern void rst_master_off();

#ifdef __cplusplus
}
#endif

#endif //__IO_CFG_H__
