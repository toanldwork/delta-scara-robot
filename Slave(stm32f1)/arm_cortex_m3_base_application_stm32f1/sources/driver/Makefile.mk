CFLAGS		+= -I./sources/driver/led
CPPFLAGS	+= -I./sources/driver/led

CPPFLAGS	+= -I./sources/driver/SHT1x
CPPFLAGS	+= -I./sources/driver/EmonLib
CPPFLAGS	+= -I./sources/driver/exor
CPPFLAGS	+= -I./sources/driver/kalman
CPPFLAGS	+= -I./sources/driver/modbus_rtu
CPPFLAGS	+= -I./sources/driver/MCP23008
CPPFLAGS	+= -I./sources/driver/button
#CPPFLAGS	+= -I./sources/driver/WEMOS_SHT3X

VPATH += sources/driver/led
VPATH += sources/driver/SHT1x
VPATH += sources/driver/EmonLib
VPATH += sources/driver/kalman
VPATH += sources/driver/exor
VPATH += sources/driver/kalman
VPATH += sources/driver/modbus_rtu
VPATH += sources/driver/MCP23008
VPATH += sources/driver/button
#VPATH += sources/driver/WEMOS_SHT3X


SOURCES += sources/driver/led/led.c

SOURCES_CPP += sources/driver/SHT1x/SHT1x.cpp
SOURCES_CPP += sources/driver/EmonLib/EmonLib.cpp
SOURCES_CPP += sources/driver/exor/exor.cpp
SOURCES_CPP += sources/driver/kalman/kalman.cpp
SOURCES_CPP += sources/driver/modbus_rtu/modbus_rtu.cpp
SOURCES_CPP += sources/driver/MCP23008/Adafruit_MCP23008.cpp
SOURCES_CPP += sources/driver/button/button.cpp
#SOURCES_CPP += sources/driver/WEMOS_SHT3X/WEMOS_SHT3X.cpp
