#ifndef __TASK_LIST_H__
#define __TASK_LIST_H__

#include "ak.h"
#include "task.h"

extern task_t app_task_table[];

/*****************************************************************************/
/*  DECLARE: Internal Task ID
 *  Note: Task id MUST be increasing order.
 */
/*****************************************************************************/

enum {
	/**
	  * SYSTEM TASKS
	  **************/
	TASK_TIMER_TICK_ID,

	/**
	  * APP TASKS
	  **************/
	SL_TASK_SHELL_ID,
	SL_TASK_LIFE_ID,
	SL_TASK_IF_ID,
	SL_TASK_SENSOR_ID,
	SL_TASK_SETTING_ID,
	SL_TASK_CPU_SERIAL_IF_ID,
	SL_TASK_FWU_ID,
	SL_TASK_KEEPALIVE_ID,
	SL_TASK_TEST_ID,
	SL_TASK_ZIGBEE_ID,
	SL_TASK_ENCODER_PID_ID,
	/**
	  * EOT task ID
	  **************/
	AK_TASK_EOT_ID
};


/*****************************************************************************/
/*  DECLARE: Task entry point
 */
/*****************************************************************************/
extern void task_shell(ak_msg_t*);
extern void task_life(ak_msg_t*);
extern void task_if(ak_msg_t*);
extern void task_ctrl_output(ak_msg_t*);
extern void task_sensor(ak_msg_t*);
extern void task_setting(ak_msg_t*);
extern void task_cpu_serial_if(ak_msg_t* msg);
extern void task_fwu(ak_msg_t* msg);
extern void task_keepalive(ak_msg_t* msg);
extern void task_test(ak_msg_t*);
extern void task_zigbee(ak_msg_t* msg);
extern void task_encoder_pid(ak_msg_t* msg);

#endif //__TASK_LIST_H__
