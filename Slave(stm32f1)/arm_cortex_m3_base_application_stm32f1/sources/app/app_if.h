/**
 ******************************************************************************
 * @author: ThanNT
 * @date:   30/1/2017
 * @brief:  app external signal define
 ******************************************************************************
**/
#ifndef __APP_IF_H__
#define __APP_IF_H__

/*****************************************************************************/
/* task MT_SYS define.
 */
/*****************************************************************************/
/* define timer */
#define MT_SYS_WATCH_DOG_REPORT_REQ_INTERVAL				(15000)

/* define signal */
enum {
	MT_SYS_WATCH_DOG_REPORT_REQ = AK_USER_DEFINE_SIG,
};

/*****************************************************************************/
/*  task MT_RF24 define.
 */
/*****************************************************************************/
/* define timer */
#define MT_RF24_IF_TIMER_PACKET_DELAY_INTERVAL		(100)

/* define signal */
enum {
	MT_RF24_IF_PURE_MSG_OUT = AK_USER_DEFINE_SIG,
	MT_RF24_IF_COMMON_MSG_OUT,
	MT_RF24_IF_TIMER_PACKET_DELAY,
};

/*****************************************************************************/
/*  task MT_CONSOLE define
 */
/*****************************************************************************/
/* define timer */

/* define signal */
enum {
	MT_CONSOLE_INTERNAL_LOGIN_CMD = AK_USER_DEFINE_SIG,
};

/*****************************************************************************/
/* task MT_IF define
 */
/*****************************************************************************/
/* define timer */
/* define signal */
enum {
	MT_IF_PURE_MSG_IN = AK_USER_DEFINE_SIG,
	MT_IF_PURE_MSG_OUT,
	MT_IF_COMMON_MSG_IN,
	MT_IF_COMMON_MSG_OUT,
	MT_IF_DYNAMIC_MSG_IN,
	MT_IF_DYNAMIC_MSG_OUT,
};

/*****************************************************************************/
/* task MT_CLOUD define.
 */
/*****************************************************************************/
/* define timer */
/* define signal */
enum {
	MT_CLOUD_DATA_COMMUNICATION = AK_USER_DEFINE_SIG,
	MT_CLOUD_MT_SYNC_SL_OK_REP,
	MT_CLOUD_MT_SYNC_SL_ERR_REP,
	MT_CLOUD_SL_AUDIO_SENSORS_REP,
	MT_CLOUD_SETTINGS_AUDIO_REP,
	MT_CLOUD_MT_SYNC_MUSIC_REP,
	MT_CLOUD_MT_UPDATE_FIRMWARE_REP,
	MT_CLOUD_MT_STATUS_REPORT,
};

/*****************************************************************************/
/* task MT_SNMP define.
 */
/*****************************************************************************/
/* define timer */
#define MT_LOG_TRANSFORMER_GET_INFO_REQ_INTERVAL	(10000)

/* define signal */
enum {
	MT_LOG_SL_SYNC_OK = AK_USER_DEFINE_SIG,
	MT_LOG_SL_SYNC_ERR,
	MT_LOG_SL_SENSOR_REPORT_REP,
};

/*****************************************************************************/
/* task MT_SM define
 */
/*****************************************************************************/
/* define timer */
#define MT_SM_SL_SYNC_REQ_INTERVAL					(3000)
#define MT_SM_SL_SYNC_REQ_TO_INTERVAL				(2000)

/* define signal */
enum {
	MT_SM_SL_SYNC_REQ = AK_USER_DEFINE_SIG,
	MT_SM_SL_SYNC_REQ_TO,
	MT_SM_SL_SYNC_RES,
	MT_SM_AU_SET_AUDIO_SETTINGS_REQ,
	MT_SM_AU_SET_AUDIO_SETTINGS_RES,
	MT_SM_SL_AUDIO_SENSOR_REPORT_REQ,
	MT_SM_SL_AUDIO_SENSOR_REPORT_RES,
	MT_SM_SL_REBOOT_SLAVE_REQ,
};

/*****************************************************************************/
/* task MT_SLAVE define.
 */
/*****************************************************************************/
/* define timer */

/* define signal */
/* define signal */

/*****************************************************************************/
/* task MT_SENSOR define.
 */
/*****************************************************************************/
/* define timer */
#define MT_AUDIO_INIT_REQ_INTERVAL							(10000)
#define MT_AUDIO_CONTROL_REQ_INTERVAL						(60000)
#define MT_AUDIO_SENSOR_REPORT_REQ_INTERVAL					(20000)

/* define signal */
/* define signal */
enum {
	MT_AUDIO_INIT_REQ = AK_USER_DEFINE_SIG,
	MT_AUDIO_SETTING_REQ,
	MT_AUDIO_CONTROL_REQ,
	MT_AUDIO_SENSOR_START_CHECK_REQ,
	MT_AUDIO_SENSOR_STOP_CHECK_REQ,
	MT_AUDIO_SENSOR_REPORT_RES,
	MT_AUDIO_PENDING_PLAYER_REQ,
	MT_AUDIO_RUNNING_PLAYER_REQ,
};

enum {
	MT_AUDIO_CHANEL_INIT_REQ = AK_USER_DEFINE_SIG,
	MT_AUDIO_CHANEL_CONTROL_REQ,
};


/*****************************************************************************/
/* task MT_SYNC_MUSIC define.
 */
/*****************************************************************************/
/* define timer */
/* define signal */
enum {
	MT_SYNC_ADD_MUSIC_REQ = AK_USER_DEFINE_SIG,
	MT_SYNC_DEL_MUSIC_REQ,
};


/*****************************************************************************/
/* task MT_UPDATE_FIRMWARE define.
 */
/*****************************************************************************/
/* define timer */
/* define signal */
enum {
	MT_UPDATE_FIRMWARE_REQ = AK_USER_DEFINE_SIG,
	MT_UPDATE_FIRMWARE_DONE,
};

/*****************************************************************************/
/* task MT_KEEPALIVE define.
 */
/*****************************************************************************/
/* define timer */
#define MT_KEEPALIVE_PING_INTERVAL					(300000) // 5s
#define MT_KEEPALIVE_PONG_MAX_TO_INTERVAL			(SL_KEEPALIVE_PING_INTERVAL * 3) // 15s
#define MT_KEEPALIVE_WAIT_SL_RESET					(SL_KEEPALIVE_PING_INTERVAL + 300000) // 10s

/* define signal */
enum {
	MT_KEEPALIVE_PING = AK_USER_DEFINE_SIG,
	MT_KEEPALIVE_PONG,
	MT_KEEPALIVE_TO,
};

/*****************************************************************************/
/*  global define variable
 */
/*****************************************************************************/
#define APP_VERSION									("v2.2")

#endif //__APP_IF_H__
