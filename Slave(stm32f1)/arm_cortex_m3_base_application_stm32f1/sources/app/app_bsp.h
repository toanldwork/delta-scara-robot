#ifndef __APP_BSP_H__
#define __APP_BSP_H__

#include "button.h"

#define BUTTON_MODE_ID					(0x01)

extern button_t btn_mode;

extern void btn_mode_callback(void*);
extern void btn_up_callback(void*);
extern void btn_down_callback(void*);

#endif //__APP_BSP_H__
