#include "fsm.h"
#include "port.h"
#include "message.h"
#include "timer.h"

#include "app.h"
#include "app_if.h"
#include "app_dbg.h"
#include "app_flash.h"

#include "task_list.h"
#include "task_list_if.h"
#include "task_setting.h"
#include "task_sensor.h"
#include "task_if.h"

#include "utils.h"

#include "sys_ctrl.h"
#include "sys_dbg.h"

sl_info_t slave_info;
gw_network_info_t gw_network_info;

#if defined(NON_CLEAR_RAM_FATAL_LOG)

#else
static uint32_t m_flash_fatal_log_address;
static uint32_t m_flash_fatal_log_index;

static uint32_t m_flash_fatal_irq_log_address;
static uint32_t m_flash_fatal_irq_log_index;
#endif

void task_setting(ak_msg_t* msg) {
	switch (msg->sig) {
	case SL_SETTING_SYNC_INFO_REQ: {
		APP_PRINT("SL_SETTING_SYNC_INFO_REQ\n");
		ak_msg_t* s_msg = get_common_msg();
		set_if_src_task_id(s_msg, SL_TASK_SETTING_ID);
		set_if_des_task_id(s_msg, MT_TASK_SM_ID);
		set_if_src_type(s_msg, IF_TYPE_CPU_SERIAL_SL);
		set_if_des_type(s_msg, IF_TYPE_CPU_SERIAL_MT);
		set_if_sig(s_msg, MT_SM_SL_SYNC_RES);
		set_if_data_common_msg(s_msg, (uint8_t*)&slave_info, sizeof(sl_info_t));

		set_msg_sig(s_msg, SL_IF_COMMON_MSG_OUT);
		task_post(SL_TASK_IF_ID, s_msg);
	}
		break;

	case SL_SETTING_REBOOT_SLAVE_DEVICE_REQ: {
		APP_PRINT("SL_SETTING_REBOOT_SLAVE_DEVICE_REQ\n");
		sys_ctrl_reset();
	}
		break;

	default:
		break;
	}
}
