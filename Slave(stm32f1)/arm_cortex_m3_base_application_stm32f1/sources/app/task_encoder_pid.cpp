#include "fsm.h"
#include "timer.h"
#include "port.h"
#include "message.h"

#include "sys_ctrl.h"
#include "sys_dbg.h"
#include "xprintf.h"

#include "app.h"
#include "app_dbg.h"

#include "task_list.h"
#include <math.h>

#include "io_cfg.h"
#include "task_encoder_pid.h"
#include "system.h"
#include "task_life.h"
#include "hal_defs.h"



float test = 0;
float speed = 0;
uint16_t Cnt_feedback_pre = 0;


//uint16_t degree = 0;
encoder_t enc_fb1;
delta_if_t delta_data;
param_rb_t param_robot_data;
float pid_degree = 0;
float pid_time = 1;

void task_encoder_pid(ak_msg_t* msg) {
	switch (msg->sig) {
	case SL_DELTA_ROBOT_AUTO_RUN_REQ: {

		Turn_Release((int16_t)pid_feeback1);
		//Turn_Release(500);
#if (JGB37_545 == 1)
		ENCODER_Read_Release(&enc_fb1.Pos_encoder_feedback, &enc_fb1.Dir_feedback, &enc_fb1.Cnt_feedback, TIM3, 1);
#elif (JGB37_520 == 1)
		ENCODER_Read_Release(&enc_fb1.Pos_encoder_feedback, &enc_fb1.Dir_feedback, &enc_fb1.Cnt_feedback, TIM3, 2);
#elif (JGB37_545_333rpm == 1)
		ENCODER_Read_Release(&enc_fb1.Pos_encoder_feedback, &enc_fb1.Dir_feedback, &enc_fb1.Cnt_feedback, TIM3, 3);
#endif
	}
		break;

	case SL_DELTA_ROBOT_IF_START_BROADCAST_REQ: {
		APP_DBG("SL_DELTA_ROBOT_IF_START_BROADCAST_REQ\n");

#if (DC1 == 1)
		xprintf("START DC1\n");
		if (param_robot_data.direction[0] == POS_DIR) {
			pid_degree = -param_robot_data.degree[0];
		}
		else if (param_robot_data.direction[0] == NEG_DIR) {
			pid_degree = param_robot_data.degree[0];
		}
		pid_time = (float)param_robot_data.setting_time[0]/param_robot_data.division_time[0];
#elif (DC2 == 1)
		xprintf("START DC2\n");
		if (param_robot_data.direction[1] == POS_DIR) {
			pid_degree = -param_robot_data.degree[1];
		}
		else if (param_robot_data.direction[1] == NEG_DIR){
			pid_degree = param_robot_data.degree[1];
		}
		pid_time = (float)param_robot_data.setting_time[1]/param_robot_data.division_time[1];
#elif (DC3 == 1)
		xprintf("START DC3\n");
		if (param_robot_data.direction[2] == POS_DIR) {
			pid_degree = -param_robot_data.degree[2];
		}
		else if (param_robot_data.direction[2] == NEG_DIR){
			pid_degree = param_robot_data.degree[2];
		}
		pid_time = (float)param_robot_data.setting_time[2]/param_robot_data.division_time[2];
#elif (DC4 == 1)
		xprintf("START DC4\n");
		if (param_robot_data.direction[3] == POS_DIR) {
			pid_degree = -param_robot_data.degree[3];
		}
		else if (param_robot_data.direction[3] == NEG_DIR){
			pid_degree = param_robot_data.degree[3];
		}
		pid_time = (float)param_robot_data.setting_time[3]/param_robot_data.division_time[3];
#endif
		xprintf("pid_degree: %d\n", (int16_t)pid_degree);
		xprintf("pid_time: %d\n", (int8_t)pid_time);
	}
		break;
	case SL_DELTA_ROBOT_IF_STOP_BROADCAST_REQ: {
		APP_DBG("SL_DELTA_ROBOT_IF_STOP_BROADCAST_REQ\n");
	}
		break;
	case SL_DELTA_ROBOT_IF_WRITE_BROADCAST_REQ: {
		APP_DBG("SL_DELTA_ROBOT_IF_WRITE_BROADCAST_REQ\n");
		memset(&delta_data, 0, sizeof(delta_if_t));
		memset(&param_robot_data, 0, sizeof(param_rb_t));
		memcpy(&delta_data, ((ak_msg_common_t*)msg)->data, sizeof(delta_if_t));

		/* take data(direction, degree, setting time and division_time) from UART Master to param_robot_data */
		/* BUILD_UINT16(loByte, hiByte) */
		/* param for motor1 */
		param_robot_data.direction[0]		= delta_data.data[0];
		param_robot_data.degree[0]			= BUILD_UINT16(delta_data.data[2], delta_data.data[1]);
		param_robot_data.setting_time[0]	= delta_data.data[3];
		param_robot_data.division_time[0]	= delta_data.data[4];
		/* param for motor2*/
		param_robot_data.direction[1]		= delta_data.data[5];
		param_robot_data.degree[1]			= BUILD_UINT16(delta_data.data[7], delta_data.data[6]);
		param_robot_data.setting_time[1]	= delta_data.data[8];
		param_robot_data.division_time[1]	= delta_data.data[9];
		/* param for motor3*/
		param_robot_data.direction[2]		= delta_data.data[10];
		param_robot_data.degree[2]			= BUILD_UINT16(delta_data.data[12], delta_data.data[11]);
		param_robot_data.setting_time[2]	= delta_data.data[13];
		param_robot_data.division_time[2]	= delta_data.data[14];
		/* param for motor4*/
		param_robot_data.direction[3]		= delta_data.data[15];
		param_robot_data.degree[3]			= BUILD_UINT16(delta_data.data[17], delta_data.data[16]);
		param_robot_data.setting_time[3]	= delta_data.data[18];
		param_robot_data.division_time[3]	= delta_data.data[19];
		for (uint8_t i = 0; i < 4; i++) {
			xprintf("param_robot_data.direction[%d]: %d	-	", i, param_robot_data.direction[i]);
			xprintf("param_robot_data.degree[%d]: %d	-	", i, param_robot_data.degree[i]);
			xprintf("param_robot_data.setting_time[%d]: %d	-	", i, param_robot_data.setting_time[i]);
			xprintf("param_robot_data.division_time[%d]: %d\n", i, param_robot_data.division_time[i]);
		}
	}
		break;
	case SL_DELTA_ROBOT_IF_RESET_DATA_REQ: {
		APP_DBG("SL_DELTA_ROBOT_IF_RESET_DATA_REQ\n");
		sys_ctrl_reset();
	}
		break;

	default:
		break;
	}
}
