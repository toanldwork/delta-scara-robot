/**
 ******************************************************************************
 * @Author: ThanNT
 * @Date:   13/08/2016
 ******************************************************************************
**/

#include <stdbool.h>

#include "fsm.h"
#include "port.h"
#include "message.h"

#include "app.h"
#include "app_dbg.h"
#include "task_shell.h"
#include "task_list.h"

#include "cmd_line.h"
#include "utils.h"
#include "xprintf.h"

#include "sys_dbg.h"
#include "sys_ctrl.h"
#include "sys_irq.h"

//struct shell_t {
//	uint8_t index;
//	uint8_t data[SHELL_BUFFER_LENGHT];
//} shell;

//void sys_irq_shell() {
//	uint8_t c = 0;
//	c = sys_ctrl_shell_get_char();
//	if (shell.index < SHELL_BUFFER_LENGHT - 1) {
//		if (c == '\r' || c == '\n') {
//			shell.data[shell.index] = c;
//			shell.data[shell.index + 1] = 0;

//			{
//				ak_msg_t* msg = get_common_msg();
//				set_msg_sig(msg, SL_SHELL_LOGIN_CMD);
//				set_data_common_msg(msg, &shell.data[0], shell.index + 2);
//				task_post(SL_TASK_SHELL_ID, msg);
//			}

//			shell.index = 0;
//		}
//		else if (c == 8) {
//			if (shell.index) {
//				shell.index--;
//			}
//		}
//		else {
//			shell.data[shell.index++] = c;
//		}
//	}
//	else {
//		APP_PRINT("\nerror: cmd too long, cmd size: %d, try again !\n", SHELL_BUFFER_LENGHT);
//		shell.index = 0;
//	}
//}

//void task_shell(ak_msg_t* msg) {
//	uint8_t fist_char = *(get_data_common_msg(msg));

//	switch (msg->sig) {
//	case SL_SHELL_LOGIN_CMD:
//		set_dymc_output_type(DYMC_UART_SHELL_TYPE);
//		break;

//	case SL_SHELL_REMOTE_CMD:
//		set_dymc_output_type(DYMC_RF_REMOTE_TYPE);
//		break;

//	default:
//		FATAL("SHELL", 0x01);
//		break;
//	}

//	switch (cmd_line_parser(lgn_cmd_table, get_data_common_msg(msg))) {
//	case CMD_SUCCESS:
//		break;

//	case CMD_NOT_FOUND:
//		if (fist_char != '\r' &&
//				fist_char != '\n') {
//			LOGIN_PRINT("cmd unknown\n");
//		}
//		break;

//	case CMD_TOO_LONG:
//		LOGIN_PRINT("cmd too long\n");
//		break;

//	case CMD_TBL_NOT_FOUND:
//		LOGIN_PRINT("cmd table not found\n");
//		break;

//	default:
//		LOGIN_PRINT("cmd error\n");
//		break;
//	}

//	LOGIN_PRINT("#");
//}


#pragma GCC optimize ("O3")

struct shell_t {
	uint8_t index;
	uint8_t data[SHELL_BUFFER_LENGHT];
};

volatile struct shell_t shell;

void sys_irq_shell() {
	volatile uint8_t c = 0;

	c = sys_ctrl_shell_get_char();

	if (shell.index < SHELL_BUFFER_LENGHT - 1) {

		if (c == '\r' || c == '\n') { /* linefeed */

			xputchar('\r');
			xputchar('\n');

			shell.data[shell.index] = c;
			shell.data[shell.index + 1] = 0;
			task_post_common_msg(SL_TASK_SHELL_ID, SL_SHELL_LOGIN_CMD, (uint8_t*)&shell.data[0], shell.index + 2);

			shell.index = 0;
		}
		else {

			xputchar(c);

			if (c == 8 && shell.index) { /* backspace */
				shell.index--;
			}
			else {
				shell.data[shell.index++] = c;
			}
		}
	}
	else {
		LOGIN_PRINT("\nerror: cmd too long, cmd size: %d, try again !\n", SHELL_BUFFER_LENGHT);
		shell.index = 0;
	}
}

void task_shell(ak_msg_t* msg) {
	uint8_t fist_char = *(get_data_common_msg(msg));

	switch (msg->sig) {
	case SL_SHELL_LOGIN_CMD:
		set_dymc_output_type(DYMC_UART_SHELL_TYPE);
		break;

	case SL_SHELL_REMOTE_CMD:
		set_dymc_output_type(DYMC_RF_REMOTE_TYPE);
		break;

	default:
		FATAL("SHELL", 0x01);
		break;
	}

	switch (cmd_line_parser(lgn_cmd_table, get_data_common_msg(msg))) {
	case CMD_SUCCESS:
		break;

	case CMD_NOT_FOUND:
		if (fist_char != '\r' &&
				fist_char != '\n') {
			LOGIN_PRINT("cmd unknown\n");
		}
		break;

	case CMD_TOO_LONG:
		LOGIN_PRINT("cmd too long\n");
		break;

	case CMD_TBL_NOT_FOUND:
		LOGIN_PRINT("cmd table not found\n");
		break;

	default:
		LOGIN_PRINT("cmd error\n");
		break;
	}

	LOGIN_PRINT("#");
}

