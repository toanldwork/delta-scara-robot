#ifndef __APP_FLASH_H__
#define __APP_FLASH_H__

#include <stdint.h>
#include <stdbool.h>

#include "app_data.h"

#define APP_FLASH_SETTING_INFO_SECTOR			(0x0000)

#define APP_FLASH_LOG_SECTOR_1					(0x3000)
#define APP_FLASH_LOG_SECTOR_2					(0x4000)
#define APP_FLASH_LOG_SECTOR_3					(0x5000)

#define APP_FLASH_DBG_SECTOR_1					(0x6000)

#define APP_FLASH_FIRMWARE_INFO_SECTOR_1		(0x7000)

#define APP_FLASH_FATAL_LOG_SECTOR				(0x8000)
#define APP_FLASH_FATAL_IRQ_LOG_SECTOR			(0x9000)

#define APP_FLASH_INTTERNAL_SHARE_DATA_SECTOR_1	(0xA000)

#define APP_FLASH_FIRMWARE_START_ADDR			(0x80000)
#define APP_FLASH_FIRMWARE_BLOCK_64K_SIZE		(8)

/**
  *****************************************************************************
  * default setting value.
  *
  *****************************************************************************
  */

#define LOCAL_SETTING_DEFAUL_VALVE_MODE					(1) /* 1: AUTO, 2: MANUAL*/

#define FLASH_WRITE_NG									(0)
#define FLASH_WRITE_OK									(1)

extern fatal_log_t non_clear_ram_fatal_log;
extern sl_control_t non_clear_ram_sl_control;

extern void flash_read_slave_info(sl_info_t*);
extern uint8_t flash_write_slave_info();

#endif //__APP_FLASH_H__
