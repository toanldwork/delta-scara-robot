/**
 ******************************************************************************
 * @Author: ThanNT
 * @Date:   13/08/2016
 ******************************************************************************
**/

#ifndef __APP_H__
#define __APP_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "ak.h"
#include "app_if.h"

/*****************************************************************************/
/*  life task define
 */
/*****************************************************************************/
/* define timer */
#define SL_LIFE_TASK_TIMER_LED_LIFE_INTERVAL		(1000)

/* define signal */
enum {
	SL_LIFE_SYSTEM_CHECK = AK_USER_DEFINE_SIG
};

/*****************************************************************************/
/*  shell task define
 */
/*****************************************************************************/
/* define timer */

/* define signal */
enum {
	SL_SHELL_LOGIN_CMD = AK_USER_DEFINE_SIG,
	SL_SHELL_REMOTE_CMD
};

/*****************************************************************************/
/*  if cpu task define
 */
/*****************************************************************************/
/* define timer */
#define SL_CPU_SERIAL_IF_KEEP_ALIVE_INTERVAL		(15000)	/* 15s */

/* define signal */
enum {
	/* interrupt signal */
	SL_CPU_SERIAL_IF_RX_SUCCESS = AK_USER_DEFINE_SIG,
	SL_CPU_SERIAL_IF_INIT,
	SL_CPU_SERIAL_IF_PURE_MSG_OUT,
	SL_CPU_SERIAL_IF_COMMON_MSG_OUT,
	SL_CPU_SERIAL_IF_DYNAMIC_MSG_OUT,
	SL_CPU_SERIAL_IF_KEEP_ALIVE,
};

/*****************************************************************************/
/* if task define
 */
/*****************************************************************************/
/* define timer */
#define SL_IF_TIMER_PACKET_TIMEOUT_INTERVAL			(500)

/* define signal */
enum {
	SL_IF_PURE_MSG_IN = AK_USER_DEFINE_SIG,
	SL_IF_PURE_MSG_OUT,
	SL_IF_COMMON_MSG_IN,
	SL_IF_COMMON_MSG_OUT,
	SL_IF_DYNAMIC_MSG_IN,
	SL_IF_DYNAMIC_MSG_OUT,
};

/*****************************************************************************/
/*  sensor task define
 */
/*****************************************************************************/
/* define timer */
#define SL_SENSOR_CHECKING_CT_REQ_INTERVAL						(2000)

/* define signal */
enum {
	SL_SENSOR_ENABLE_IR_LEARNING_REQ = AK_USER_DEFINE_SIG,
	SL_SENSOR_DISABLE_IR_LEARNING_TIMEOUT_REQ,
	SL_SENSOR_IR_DECODE_REQ,
	SL_SENSOR_IR_CONTROL_REQ,
	SL_SENSOR_SHT10_READ_REQ,
	SL_SENSOR_SHT10_TEMP_REQ,
	SL_SENSOR_SHT10_HUM_REQ,
	SL_SENSOR_SHT30_READ_REQ,
	/* signal for E18 ir sensor*/
	SL_STOP_EMER_REQ,
	SL_SENSOR_E18_IR_REQ
};
/*****************************************************************************/
/*  sensor task define
 */
/*****************************************************************************/
/* define timer */
#define SL_SENSOR_CHECKING_CT_REQ_INTERVAL						(2000)

/* define signal */
enum {
	SL_SENSOR_AUDIO_REPORT_REQ = AK_USER_DEFINE_SIG,
	SL_SENSOR_AUDIO_CHECKING_CT_REQ,
};

/*****************************************************************************/
/* setting task define
 */
/*****************************************************************************/
/* define timer */
/* define signal */
enum {
	SL_SETTING_SYNC_INFO_REQ = AK_USER_DEFINE_SIG,
	SL_SETTING_REBOOT_SLAVE_DEVICE_REQ,
};

/*****************************************************************************/
/*  life firmware uart define
 */
/*****************************************************************************/
/* define timer */
/* define signal */
enum {
	SL_FWU_INIT = AK_USER_DEFINE_SIG,
	SL_FWU_REQ
};

/*****************************************************************************/
/* keepalive task define.
 */
/*****************************************************************************/
/* define timer */
#define SL_KEEPALIVE_PING_INTERVAL					(300000)
#define SL_KEEPALIVE_PONG_MAX_TO_INTERVAL			(MT_KEEPALIVE_PING_INTERVAL * 3)
#define SL_KEEPALIVE_WAIT_MT_RESET					(MT_KEEPALIVE_PING_INTERVAL + 300000)
/* define signal */
enum {
	SL_KEEPALIVE_PING = AK_USER_DEFINE_SIG,
	SL_KEEPALIVE_PONG,
	SL_KEEPALIVE_TO,
};

/*****************************************************************************/
/*  ZIGBEE task define
 */
/*****************************************************************************/
/* define timer */
/* define signal */
enum {
	SL_ZIGBEE_INIT = AK_USER_DEFINE_SIG,
	SL_ZIGBEE_FORCE_START_COODINATOR,
	SL_ZIGBEE_START_COODINATOR,
	SL_ZIGBEE_FORCE_START_ROUTER,
	SL_ZIGBEE_START_ROUTER,
	SL_ZIGBEE_PERMIT_JOINING_REQ,
	SL_ZIGBEE_BDB_START_COMMISSIONING,
	SL_ZIGBEE_ROUTER_SEND_IR_DATA,
	SL_ZIGBEE_ROUTER_SEND_SHT_DATA,
	SL_ZIGBEE_ZCL_CMD_HANDLER,
	SL_ZIGBEE_ROUTER_SEND_SHT_TEMP_DATA,
	SL_ZIGBEE_ROUTER_SEND_SHT_HUM_DATA,
};

/*****************************************************************************/
/* solar panel cleaning robot task define.
 */
/*****************************************************************************/
/* define timer */
#define SL_SOLAR_CHECKING_REQ_INTERVAL						(100)
/* define signal */
enum {
	/* signal for E18 ir sensor*/
	SL_SOLAR_STOP_EMER_REQ = AK_USER_DEFINE_SIG,
	SL_SOLAR_AUTO_RUN_REQ,
	SL_SOLAR_STOP_MOTOR_REQ,
	SL_SOLAR_RECEIVE_PACKET_CONTROL_REQ,
	SL_SOLAR_CONTROL_REQ,
	TEST_LED
};
/*****************************************************************************/
/*  Robot Delta - Scara task define
 */
/*****************************************************************************/
/* define timer */
#define SL_DELTA_ROBOT_REQ_INTERVAL		(1000)

/* define signal */
enum {
	SL_DELTA_ROBOT_IF_START_BROADCAST_REQ = AK_USER_DEFINE_SIG,
	SL_DELTA_ROBOT_IF_STOP_BROADCAST_REQ,
	SL_DELTA_ROBOT_IF_WRITE_BROADCAST_REQ,
	SL_DELTA_ROBOT_IF_START_MOTOR_1_REQ,
	SL_DELTA_ROBOT_IF_START_MOTOR_2_REQ,
	SL_DELTA_ROBOT_IF_START_MOTOR_3_REQ,
	SL_DELTA_ROBOT_IF_START_MOTOR_4_REQ,
	SL_DELTA_ROBOT_IF_RESET_DATA_REQ,
	SL_DELTA_ROBOT_AUTO_RUN_REQ
};
/*****************************************************************************/
/*  global define variable
 */
/*****************************************************************************/
#define APP_OK										(0x00)
#define APP_NG										(0x01)

#define APP_FLAG_OFF								(0x00)
#define APP_FLAG_ON									(0x01)

/*****************************************************************************/
/*  app function declare
 */
/*****************************************************************************/
#define SL_AC_NUMBER_SAMPLE_CT_SENSOR				(2500)
#define SL_MAX_ERROR								(3)

#define SL_FIRMWARE_VERSION							("v1.0")
#define SL_HARDWARE_VERSION							("v1.0")

typedef struct {
	uint32_t magic_number;
	uint8_t version[4];
} app_info_t;

extern const app_info_t app_info;

extern void* app_get_boot_share_data();
extern int main_app();

#ifdef __cplusplus
}
#endif

#endif //APP_H
