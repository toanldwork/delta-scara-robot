#ifndef __TASK_SENSOR_H__
#define __TASK_SENSOR_H__

#include <stdint.h>

#include "app_data.h"

#include "SHT1x.h"
#include "EmonLib.h"
#include "kalman.h"
#include "modbus_rtu.h"
// #include "WEMOS_SHT3X.h"

extern KALMAN kalman_hum;
extern sl_common_sensors_t common_sensor_packet;
extern sl_audio_sensors_t audio_sensor_packet;
extern Modbus modbus_master;

extern EnergyMonitor ct_sensor[SL_TOTAL_CT_SENSOR];
extern SHT1x th_sensor[SL_TOTAL_TH_SENSOR];
// extern SHT3X sht30;


#endif //__TASK_SENSOR_H__
