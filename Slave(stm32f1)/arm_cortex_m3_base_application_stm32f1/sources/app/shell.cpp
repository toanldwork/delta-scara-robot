/**
 ******************************************************************************
 * @Author: ThanNT
 * @Date:   13/08/2016
 ******************************************************************************
**/

#include <stdint.h>
#include <stdlib.h>

#include "ak.h"
#include "task.h"
#include "timer.h"
#include "message.h"

#include "app.h"
#include "app_if.h"
#include "app_dbg.h"
#include "app_data.h"
#include "app_flash.h"

#include "task_shell.h"
#include "task_list.h"
#include "task_list_if.h"
#include "task_if.h"
#include "task_life.h"
#include "task_sensor.h"
#include "task_setting.h"
#include "task_cpu_serial_if.h"
#include "task_encoder_pid.h"

#include "cmd_line.h"
#include "utils.h"
#include "xprintf.h"

#include "led.h"
#include "EmonLib.h"

#include "sys_ctrl.h"
#include "sys_io.h"
#include "sys_dbg.h"
#include "sys_irq.h"
#include "io_cfg.h"

/*****************************************************************************/
/*  local declare
 */
/*****************************************************************************/
#define STR_LIST_MAX_SIZE		10
#define STR_BUFFER_SIZE			128

#if 0
static char cmd_buffer[STR_BUFFER_SIZE];
static char* str_list[STR_LIST_MAX_SIZE];
static uint8_t str_list_len;

static uint8_t str_parser(char* str);
static char* str_parser_get_attr(uint8_t);
#endif

/*****************************************************************************/
/*  command function declare
 */
/*****************************************************************************/
static int32_t shell_reset(uint8_t* argv);
static int32_t shell_ver(uint8_t* argv);
static int32_t shell_help(uint8_t* argv);
static int32_t shell_reboot(uint8_t* argv);
static int32_t shell_status(uint8_t* argv);
static int32_t shell_boot(uint8_t* argv);
static int32_t shell_others(uint8_t* argv);
static int32_t shell_fatal(uint8_t* argv);
static int32_t shell_fwu(uint8_t* argv);
static int32_t shell_mt(uint8_t* argv);
static int32_t shell_led(uint8_t* argv);
static int32_t shell_motor(uint8_t* argv);
static int32_t shell_znp(uint8_t* argv);
static int32_t shell_delta(uint8_t* argv);

/*****************************************************************************/
/*  command table
 */
/*****************************************************************************/
cmd_line_t lgn_cmd_table[] = {

	/*************************************************************************/
	/* system command */
	/*************************************************************************/
	{(const int8_t*)"reset",	shell_reset,		(const int8_t*)"reset terminal"		},
	{(const int8_t*)"ver",		shell_ver,			(const int8_t*)"version info"		},
	{(const int8_t*)"help",		shell_help,			(const int8_t*)"help command info"	},

	{(const int8_t*)"reboot",	shell_reboot,		(const int8_t*)"reboot system"		},

	{(const int8_t*)"stt",		shell_status,		(const int8_t*)"system status"		},
	{(const int8_t*)"boot",		shell_boot	,		(const int8_t*)"boot share"			},
	{(const int8_t*)"fatal",	shell_fatal,		(const int8_t*)"fatal info"			},
	{(const int8_t*)"other",	shell_others,		(const int8_t*)"other"				},
	{(const int8_t*)"fwu",		shell_fwu,			(const int8_t*)"app burn firmware"	},
	{(const int8_t*)"mt",		shell_mt,			(const int8_t*)"master ctrl"		},
	{(const int8_t*)"led",		shell_led,			(const int8_t*)"led"},
	{(const int8_t*)"mtr",		shell_motor,		(const int8_t*)"test control motor"	},
	{(const int8_t*)"znp",		shell_znp,			(const int8_t*)"zigbee"				},
	{(const int8_t*)"del",		shell_delta,		(const int8_t*)"Delta Robot"		},

	/*************************************************************************/
	/* debug command */
	/*************************************************************************/

	/* End Of Table */
	{(const int8_t*)0,(pf_cmd_func)0,(const int8_t*)0}
};

#if 0
uint8_t str_parser(char* str) {
	strcpy(cmd_buffer, str);
	str_list_len = 0;

	uint8_t i = 0;
	uint8_t str_list_index = 0;
	uint8_t flag_insert_str = 1;

	while (cmd_buffer[i] != 0 && cmd_buffer[i] != '\n' && cmd_buffer[i] != '\r') {
		if (cmd_buffer[i] == ' ') {
			cmd_buffer[i] = 0;
			flag_insert_str = 1;
		}
		else if (flag_insert_str) {
			str_list[str_list_index++] = &cmd_buffer[i];
			flag_insert_str = 0;
		}
		i++;
	}

	cmd_buffer[i] = 0;

	str_list_len = str_list_index;
	return str_list_len;
}

char* str_parser_get_attr(uint8_t index) {
	if (index < str_list_len) {
		return str_list[index];
	}
	return NULL;
}
#endif

/*****************************************************************************/
/*  command function definaion
 */
/*****************************************************************************/
int32_t shell_led(uint8_t* argv)
{
	led_init(&led_life, led_life_init, led_life_on, led_life_off);
	switch (*(argv + 4))	{
	case 'o': {
		timer2_disable();
		LOGIN_PRINT("LED ON\r\n");
		led_on(&led_life);
	}
		break;
	case 'f': {
		timer2_disable();
		LOGIN_PRINT("LED OFF\r\n");
		led_off(&led_life);
	}
		break;
	case 't': {
		LOGIN_PRINT("LED TOGGLE\r\n");
		TIM2_Configuration();
		TIM2_IRQHandler();
	}
		break;
	default:
		break;
	}
	return 0;
}

int32_t shell_reset(uint8_t* argv) {
	(void)argv;
	xprintf("\033[2J\r");
	return 0;
}

int32_t shell_ver(uint8_t* argv) {
	(void)argv;
	firmware_header_t firmware_header;
	sys_ctrl_get_firmware_info(&firmware_header);

	LOGIN_PRINT("kernel version    : %s\n", AK_VERSION);
	LOGIN_PRINT("firmware checksum : %04x\n", firmware_header.checksum);
	LOGIN_PRINT("firmware length   : %d\n", firmware_header.bin_len);

	return 0;
}

int32_t shell_help(uint8_t* argv) {
	uint32_t idx = 0;
	switch (*(argv + 4)) {
	default:
		LOGIN_PRINT("\nCOMMANDS INFORMATION:\n\n");
		while(lgn_cmd_table[idx].cmd != (const int8_t*)0) {
			LOGIN_PRINT("%s\n\t%s\n\n", lgn_cmd_table[idx].cmd, lgn_cmd_table[idx].info);
			idx++;
		}
		break;
	}
	return 0;
}

int32_t shell_reboot(uint8_t* argv) {
	(void)argv;
	sys_ctrl_delay_ms(10);
	sys_ctrl_reset();
	return 0;
}

int32_t shell_status(uint8_t* argv) {
	(void)argv;

	LOGIN_PRINT("\n");

	return 0;
}

int32_t shell_znp(uint8_t* argv) {

	switch (*(argv + 4)) {
	case '0': {
		task_post_pure_msg(SL_TASK_ZIGBEE_ID, SL_ZIGBEE_START_ROUTER);
	}
		break;

	case '1': {
		task_post_pure_msg(SL_TASK_ZIGBEE_ID, SL_ZIGBEE_FORCE_START_ROUTER);
	}
		break;

	case '2': {
		task_post_pure_msg(SL_TASK_ZIGBEE_ID, SL_ZIGBEE_START_COODINATOR);
	}
		break;

	case '3': {
		task_post_pure_msg(SL_TASK_ZIGBEE_ID, SL_ZIGBEE_FORCE_START_COODINATOR);
	}
		break;

	case '4': {
		task_post_pure_msg(SL_TASK_ZIGBEE_ID, SL_ZIGBEE_PERMIT_JOINING_REQ);
	}
		break;

	case '5': {
		task_post_pure_msg(SL_TASK_ZIGBEE_ID, SL_ZIGBEE_BDB_START_COMMISSIONING);
	}
		break;

	case '6': {
		task_post_pure_msg(SL_TASK_ZIGBEE_ID, SL_ZIGBEE_ROUTER_SEND_IR_DATA);
	}
		break;

	default:
		break;
	}

	return 0;
}

int32_t shell_delta(uint8_t* argv) {
	switch (*(argv + 4)) {
	case 'a': {
		xprintf("send data\n");
		delta_if_t delta_data;

		ak_msg_common_if_t app_if_msg;
		memset(&app_if_msg, 0, sizeof(ak_msg_common_if_t));
		memset(&delta_data, 0, sizeof(delta_if_t));

		delta_data.len = 12;
		for (uint8_t i = 0; i < 12; i++) {
			delta_data.data[i] = i;
		}

		/* assign if message */
		app_if_msg.header.type			= 0xC0;
		app_if_msg.header.if_src_type	= 1;
		app_if_msg.header.if_des_type	= 1;
		app_if_msg.header.sig			= 1;
		app_if_msg.header.src_task_id	= 1;
		app_if_msg.header.des_task_id	= 1;

		app_if_msg.len = sizeof(delta_if_t);
		memcpy((uint8_t*)&app_if_msg.data[0], (uint8_t*)&delta_data, app_if_msg.len);
		tx_frame_post((uint8_t*)&app_if_msg, sizeof(ak_msg_common_if_t));

	}
		break;

	default:
		break;
	}

	return 0;
}

int32_t shell_motor(uint8_t* argv) {
	switch (*(argv + 4)) {
	case '0': {
		motor_1_control(0, 0);
	}
		break;

	case '1': {
		motor_1_control(0, 200);
	}
		break;

	case '2': {
		motor_1_control(0, 500);
	}
		break;

	case '3': {
		motor_1_control(0, 1000);
	}
		break;

	case '4': {
		motor_1_control(1, 0);
	}
		break;

	case '5': {
		motor_1_control(1, 200);
	}
		break;

	case '6': {
		motor_1_control(1, 500);
	}
		break;

	case '7': {
		motor_1_control(1, 1000);
	}
		break;

	case '8': {
		motor_2_control(0, 0);
	}
		break;

	case '9': {
		motor_2_control(0, 1000);
	}
		break;

	case 'a': {
		motor_2_control(1, 1000);
	}
		break;

	case 'b': {
		motor_3_control(0, 0);
	}
		break;

	case 'c': {
		motor_3_control(0, 1000);
	}
		break;

	case 'd': {
		motor_3_control(1, 1000);
	}
		break;

	default:
		break;
	}

	return 0;
}

int32_t shell_boot(uint8_t* argv) {
	switch (*(argv + 5)) {
	case 'i': {
		sys_boot_t sys_boot;
		sys_boot_get(&sys_boot);
		LOGIN_PRINT("\n[b_fwc] psk: 0x%08X checksum: 0x%08X bin_len: %d\n", \
					sys_boot.current_fw_boot_header.psk, \
					sys_boot.current_fw_boot_header.checksum, \
					sys_boot.current_fw_boot_header.bin_len);
		LOGIN_PRINT("[b_fwu] psk: 0x%08X checksum: 0x%08X bin_len: %d\n", \
					sys_boot.update_fw_boot_header.psk, \
					sys_boot.update_fw_boot_header.checksum, \
					sys_boot.update_fw_boot_header.bin_len);
		LOGIN_PRINT("[b_cmd] cmd: %d container:%d io_driver: %d des_addr: 0x%08X src_addr: 0x%08X\n", \
					sys_boot.fw_boot_cmd.cmd, \
					sys_boot.fw_boot_cmd.container, \
					sys_boot.fw_boot_cmd.io_driver, \
					sys_boot.fw_boot_cmd.des_addr, \
					sys_boot.fw_boot_cmd.src_addr);

		LOGIN_PRINT("\n[a_fwc] psk: 0x%08X checksum: 0x%08X bin_len: %d\n", \
					sys_boot.current_fw_app_header.psk, \
					sys_boot.current_fw_app_header.checksum, \
					sys_boot.current_fw_app_header.bin_len);
		LOGIN_PRINT("[a_fwu] psk: 0x%08X checksum: 0x%08X bin_len: %d\n", \
					sys_boot.update_fw_app_header.psk, \
					sys_boot.update_fw_app_header.checksum, \
					sys_boot.update_fw_app_header.bin_len);
		LOGIN_PRINT("[a_cmd] cmd: %d container:%d io_driver: %d des_addr: 0x%08X src_addr: 0x%08X\n", \
					sys_boot.fw_app_cmd.cmd, \
					sys_boot.fw_app_cmd.container, \
					sys_boot.fw_app_cmd.io_driver, \
					sys_boot.fw_app_cmd.des_addr, \
					sys_boot.fw_app_cmd.src_addr);
	}
		break;

	case 'r': {
		sys_boot_t sys_boot;
		memset(&sys_boot, 0, sizeof(sys_boot_t));
		sys_boot_set(&sys_boot);
	}
		break;

	case 't': {
		sys_boot_t sb;
		sys_boot_get(&sb);

		/* cmd update request */
		sb.fw_app_cmd.cmd			= SYS_BOOT_CMD_UPDATE_REQ;
		sb.fw_app_cmd.container		= SYS_BOOT_CONTAINER_EXTERNAL_FLASH;
		sb.fw_app_cmd.io_driver		= SYS_BOOT_IO_DRIVER_NONE;
		sb.fw_app_cmd.des_addr		= APP_START_ADDR;
		sb.fw_app_cmd.src_addr		= APP_FLASH_FIRMWARE_START_ADDR;

		sys_boot_set(&sb);
	}
		break;

	default:
		LOGIN_PRINT("\n[HELP]\n");
		LOGIN_PRINT("1. \"boot i\"                            : boot information\n");
		LOGIN_PRINT("2. \"boot r\"                            : boot clean\n");
		LOGIN_PRINT("3. \"boot t\"                            : boot testing\n");
		break;
	}

	return 0;
}

int32_t shell_fatal(uint8_t* argv) {
#if defined(NON_CLEAR_RAM_FATAL_LOG)

#else
	fatal_log_t login_fatal_log;
	ak_msg_t t_msg;
	exception_info_t t_exception_info;
#endif

	switch (*(argv + 6)) {
	case 't':
		FATAL("TEST", 0x02);
		break;

	case '!':
		while(1);
		break;

	case '@':
		DISABLE_INTERRUPTS();
		while(1);
		break;

	case 'r':
#if defined(NON_CLEAR_RAM_FATAL_LOG)
		mem_set((uint8_t*)&non_clear_ram_fatal_log, 0, sizeof(fatal_log_t));
#else
		mem_set((uint8_t*)&login_fatal_log, 0, sizeof(fatal_log_t));
		flash_write(APP_FLASH_FATAL_LOG_SECTOR, (uint8_t*)&login_fatal_log, sizeof(fatal_log_t));
#endif
		LOGIN_PRINT("reset fatal log OK\n");
		break;

	case 'l': {
#if defined(NON_CLEAR_RAM_FATAL_LOG)
		LOGIN_PRINT("[times] fatal: %d\n",		non_clear_ram_fatal_log.fatal_times);
		LOGIN_PRINT("[times] restart: %d\n",	non_clear_ram_fatal_log.restart_times);

		LOGIN_PRINT("\n");
		LOGIN_PRINT("[fatal] type: %s\n",		non_clear_ram_fatal_log.string);
		LOGIN_PRINT("[fatal] code: 0x%02X\n",	non_clear_ram_fatal_log.code);

		LOGIN_PRINT("\n");
		LOGIN_PRINT("[task] id: %d\n",			non_clear_ram_fatal_log.current_task.id);
		LOGIN_PRINT("[task] pri: %d\n",			non_clear_ram_fatal_log.current_task.pri);
		LOGIN_PRINT("[task] entry: 0x%x\n",		non_clear_ram_fatal_log.current_task.task);

		LOGIN_PRINT("\n");
		LOGIN_PRINT("[obj] task: %d\n",			non_clear_ram_fatal_log.current_active_object.des_task_id);
		LOGIN_PRINT("[obj] sig: %d\n",			non_clear_ram_fatal_log.current_active_object.sig);
		LOGIN_PRINT("[obj] type: 0x%x\n",		get_msg_type(&non_clear_ram_fatal_log.current_active_object));
		LOGIN_PRINT("[obj] ref count: %d\n",	get_msg_ref_count(&non_clear_ram_fatal_log.current_active_object));
		LOGIN_PRINT("[obj] wait time: %d\n",	non_clear_ram_fatal_log.current_active_object.dbg_handler.start_exe - non_clear_ram_fatal_log.current_active_object.dbg_handler.start_post);

		LOGIN_PRINT("\n");
		LOGIN_PRINT("[core] ipsr: %d\n",			non_clear_ram_fatal_log.m3_core_reg.ipsr);
		LOGIN_PRINT("[core] primask: 0x%08X\n",		non_clear_ram_fatal_log.m3_core_reg.primask);
		LOGIN_PRINT("[core] faultmask: 0x%08X\n",	non_clear_ram_fatal_log.m3_core_reg.faultmask);
		LOGIN_PRINT("[core] basepri: 0x%08X\n",		non_clear_ram_fatal_log.m3_core_reg.basepri);
		LOGIN_PRINT("[core] control: 0x%08X\n",		non_clear_ram_fatal_log.m3_core_reg.control);

		LOGIN_PRINT("\n");
		LOGIN_PRINT("[irq] IRQ number: %d\n",			(int32_t)((int32_t)non_clear_ram_fatal_log.m3_core_reg.ipsr - (int32_t)SYS_IRQ_EXCEPTION_NUMBER_IRQ0_NUMBER_RESPECTIVE));
#else
		flash_read(APP_FLASH_FATAL_LOG_SECTOR, (uint8_t*)&login_fatal_log, sizeof(fatal_log_t));

		LOGIN_PRINT("[times] fatal: %d\n",		login_fatal_log.fatal_times);
		LOGIN_PRINT("[times] restart: %d\n",	login_fatal_log.restart_times);

		LOGIN_PRINT("\n");
		LOGIN_PRINT("[fatal] type: %s\n",		login_fatal_log.string);
		LOGIN_PRINT("[fatal] code: 0x%02X\n",	login_fatal_log.code);

		LOGIN_PRINT("\n");
		LOGIN_PRINT("[task] id: %d\n",			login_fatal_log.current_task.id);
		LOGIN_PRINT("[task] pri: %d\n",			login_fatal_log.current_task.pri);
		LOGIN_PRINT("[task] entry: 0x%x\n",		login_fatal_log.current_task.task);

		LOGIN_PRINT("\n");
		LOGIN_PRINT("[obj] task: %d\n",			login_fatal_log.current_active_object.des_task_id);
		LOGIN_PRINT("[obj] sig: %d\n",			login_fatal_log.current_active_object.sig);
		LOGIN_PRINT("[obj] type: 0x%x\n",		get_msg_type(&login_fatal_log.current_active_object));
		LOGIN_PRINT("[obj] ref count: %d\n",	get_msg_ref_count(&login_fatal_log.current_active_object));
		LOGIN_PRINT("[obj] wait time: %d\n",	login_fatal_log.current_active_object.dbg_handler.start_exe - login_fatal_log.current_active_object.dbg_handler.start_post);

		LOGIN_PRINT("\n");
		LOGIN_PRINT("[core] ipsr: %d\n",			login_fatal_log.m3_core_reg.ipsr);
		LOGIN_PRINT("[core] primask: 0x%08X\n",		login_fatal_log.m3_core_reg.primask);
		LOGIN_PRINT("[core] faultmask: 0x%08X\n",	login_fatal_log.m3_core_reg.faultmask);
		LOGIN_PRINT("[core] basepri: 0x%08X\n",		login_fatal_log.m3_core_reg.basepri);
		LOGIN_PRINT("[core] control: 0x%08X\n",		login_fatal_log.m3_core_reg.control);

		LOGIN_PRINT("\n");
		LOGIN_PRINT("[irq] IRQ number: %d\n",		(int32_t)((int32_t)login_fatal_log.m3_core_reg.ipsr - (int32_t)SYS_IRQ_EXCEPTION_NUMBER_IRQ0_NUMBER_RESPECTIVE));
#endif
	}
		break;

	case 'm': {
#if defined(NON_CLEAR_RAM_FATAL_LOG)
		LOGIN_PRINT("function does not supported\n");
#else
		uint32_t	flash_sys_log_address = APP_FLASH_DBG_SECTOR_1;
		for (uint32_t index = 0; index < 32; index++) {
			/* reset watchdog */
			sys_ctrl_independent_watchdog_reset();
			sys_ctrl_soft_watchdog_reset();

			flash_read(flash_sys_log_address, (uint8_t*)&t_msg, sizeof(ak_msg_t));
			flash_sys_log_address += sizeof(ak_msg_t);

			uint32_t wait_time;
			if (t_msg.dbg_handler.start_exe >= t_msg.dbg_handler.start_post) {
				wait_time = t_msg.dbg_handler.start_exe - t_msg.dbg_handler.start_post;
			}
			else {
				wait_time = t_msg.dbg_handler.start_exe + (0xFFFFFFFF - t_msg.dbg_handler.start_post);
			}

			uint32_t exe_time;
			if (t_msg.dbg_handler.stop_exe >= t_msg.dbg_handler.start_exe) {
				exe_time = t_msg.dbg_handler.stop_exe - t_msg.dbg_handler.start_exe;
			}
			else {
				exe_time = t_msg.dbg_handler.stop_exe + (0xFFFFFFFF - t_msg.dbg_handler.start_exe);
			}

			LOGIN_PRINT("index: %d\tdes_task_id: %d\tmsg_type:0x%x\tref_count:%d\tsig: %d\t\twait_time: %d\texe_time: %d\n"\
						, index										\
						, t_msg.des_task_id							\
						, (t_msg.ref_count & AK_MSG_TYPE_MASK)		\
						, (t_msg.ref_count & AK_MSG_REF_COUNT_MASK)	\
						, t_msg.sig									\
						, (wait_time)								\
						, (exe_time));
		}
#endif
	}
		break;

	case 'e': {
#if defined(NON_CLEAR_RAM_FATAL_LOG)
		LOGIN_PRINT("function does not supported\n");
#else
		uint32_t flash_irq_log_address = APP_FLASH_FATAL_IRQ_LOG_SECTOR;
		for (uint32_t index = 0; index < (LOG_QUEUE_IRQ_SIZE / sizeof(exception_info_t)); index++) {
			/* reset watchdog */
			sys_ctrl_independent_watchdog_reset();
			sys_ctrl_soft_watchdog_reset();

			flash_read(flash_irq_log_address, (uint8_t*)&t_exception_info, sizeof(exception_info_t));
			flash_irq_log_address += sizeof(exception_info_t);

			LOGIN_PRINT("index: %d\texcept_number: %d\tirq_number: %d\ttimestamp: %d\n"\
						, index										\
						, t_exception_info.except_number																				\
						, (int32_t)((int32_t)t_exception_info.except_number - (int32_t)SYS_IRQ_EXCEPTION_NUMBER_IRQ0_NUMBER_RESPECTIVE)	\
						, t_exception_info.timestamp);
		}
#endif
	}

		break;

	default:
		LOGIN_PRINT("\n[HELP]\n");
		LOGIN_PRINT("1. \"fatal r\"                           : fatal reset log \n");
		LOGIN_PRINT("2. \"fatal l\"                           : fatal view log\n");
		LOGIN_PRINT("3. \"fatal e\"                           : fatal view IRQ log\n");
		LOGIN_PRINT("4. \"fatal m\"                           : fatal info debug\n");
	}

	return 0;
}

#include "utility/twi.h"
#include "Wire.h"

int32_t shell_others(uint8_t* argv) {
	switch (*(argv + 6)) {
	default:
		LOGIN_PRINT("\n[HELP]\n");
		break;
	}

	return 0;
}

int32_t shell_fwu(uint8_t* argv) {
	(void)argv;
	sys_boot_t sb;
	sys_boot_get(&sb);

	/* cmd update request */
	sb.fw_app_cmd.cmd			= SYS_BOOT_CMD_UPDATE_REQ;
	sb.fw_app_cmd.container		= SYS_BOOT_CONTAINER_DIRECTLY;
	sb.fw_app_cmd.io_driver		= SYS_BOOT_IO_DRIVER_UART;
	sb.fw_app_cmd.des_addr		= APP_START_ADDR;
	sb.fw_app_cmd.src_addr		= 0;
	sys_boot_set(&sb);

	sys_ctrl_reset();
	return 0;
}

int32_t shell_mt(uint8_t* argv) {

	switch (*(argv + 3)) {
	case 'r': {
		rst_master_off();
		sys_ctrl_delay_ms(1000);
		rst_master_on();

		LOGIN_PRINT("master reset done\n");
	}
		break;

	default:
		break;
	}

	return 0;
}

