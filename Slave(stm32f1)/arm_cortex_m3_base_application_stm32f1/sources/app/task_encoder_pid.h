#ifndef __TASK_ENCODER_PID__
#define __TASK_ENCODER_PID__

#include <stdint.h>
#include "app.h"


#define JGB37_545							1
#define JGB37_520							0
#define JGB37_545_333rpm					0

/*
 *	DESCRIPSION FOR DC OF SCARA:
 *	DC1 - DC4: JGB37_545_333rpm
 *	DC2 - DC3: JGB37_545
 */


#define DC1									0
#define DC2									0
#define DC3									1
#define DC4									0

#define IFCPU_DELTA_ROBOT_IF_DATA_SIZE		20

#define POS_DIR								1
#define NEG_DIR								0

typedef struct {
	uint16_t Dir_feedback;
	uint16_t Cnt_feedback;
	uint16_t Cnt_feedback_pre;
	float  Pos_encoder_feedback;
	float Spe_encoder_feedback;
} encoder_t;

typedef struct {
	uint8_t cmd;
	uint8_t len;
	uint8_t data[IFCPU_DELTA_ROBOT_IF_DATA_SIZE];
} delta_if_t;

typedef struct {
	uint16_t degree[4];
	uint8_t setting_time[4];
	uint8_t division_time[4];
	uint8_t direction[4];
} param_rb_t;

extern encoder_t enc_fb1;
extern float pid_degree;
extern float pid_time;


#endif //__TASK_ENCODER_PID__
