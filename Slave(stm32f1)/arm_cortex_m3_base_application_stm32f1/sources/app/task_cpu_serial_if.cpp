#include "fsm.h"
#include "port.h"
#include "message.h"
#include "timer.h"

#include "app.h"
#include "app_data.h"
#include "app_dbg.h"
#include "task_cpu_serial_if.h"
#include "task_list.h"

#include "cmd_line.h"
#include "utils.h"
#include "xprintf.h"

#include "sys_irq.h"
#include "sys_io.h"
#include "sys_ctrl.h"
#include "sys_dbg.h"

#include "io_cfg.h"

enum {
	IFCPU_SOP_CHAR  = 0xEF,
	IFCPU_DATA_SIZE = 255
};

enum {
	SOP_STATE = 0,
	LEN_STATE,
	DATA_STATE,
	FCS_STATE,
};

enum {
	RX_FRAME_PARSER_FAILED = -1,
	RX_FRAME_PARSER_SUCCESS = 0,
	RX_FRAME_PARSER_RX_REMAIN = 1
};

struct if_cpu_serial_frame_t {
	uint8_t		frame_sop;
	uint32_t	len;
	uint8_t		data_index;
	uint8_t		data[IFCPU_DATA_SIZE];
	uint8_t		frame_fcs;
} if_cpu_serial_frame;

static uint8_t cpu_serial_rx_frame_state = SOP_STATE;
static uint8_t tx_buffer[IFCPU_DATA_SIZE];

static void rx_frame_parser(uint8_t data);
void tx_frame_post(uint8_t*, uint8_t);
static uint8_t if_cpu_serial_calcfcs(uint8_t, uint8_t*);
static void dync_msg_frame_post(ak_msg_dynamic_if_t*);

void sys_irq_uart_cpu_serial_if() {
	rx_frame_parser(io_uart_interface_receiver());
}

void task_cpu_serial_if(ak_msg_t* msg) {
	switch (msg->sig) {
	case SL_CPU_SERIAL_IF_INIT: {
		APP_DBG("SL_CPU_SERIAL_IF_INIT\n");
		io_uart_interface_cfg();
	}
		break;

	case SL_CPU_SERIAL_IF_PURE_MSG_OUT: {
		APP_DBG("[IF SL SERIAL][SEND] SL_CPU_SERIAL_IF_PURE_MSG_OUT\n");

		ak_msg_pure_if_t app_if_msg;
		memset(&app_if_msg, 0, sizeof(ak_msg_pure_if_t));

		/* assign if message */
		app_if_msg.header.type			= PURE_MSG_TYPE;
		app_if_msg.header.if_src_type	= msg->if_src_type;
		app_if_msg.header.if_des_type	= msg->if_des_type;
		app_if_msg.header.sig			= msg->if_sig;
		app_if_msg.header.src_task_id	= msg->if_src_task_id;
		app_if_msg.header.des_task_id	= msg->if_des_task_id;

		tx_frame_post((uint8_t*)&app_if_msg, sizeof(ak_msg_pure_if_t));
	}
		break;

	case SL_CPU_SERIAL_IF_COMMON_MSG_OUT: {
		APP_DBG("[IF SL SERIAL][SEND] SL_CPU_SERIAL_IF_COMMON_MSG_OUT\n");

		ak_msg_common_if_t app_if_msg;
		memset(&app_if_msg, 0, sizeof(ak_msg_common_if_t));

		/* assign if message */
		app_if_msg.header.type			= COMMON_MSG_TYPE;
		app_if_msg.header.if_src_type	= msg->if_src_type;
		app_if_msg.header.if_des_type	= msg->if_des_type;
		app_if_msg.header.sig			= msg->if_sig;
		app_if_msg.header.src_task_id	= msg->if_src_task_id;
		app_if_msg.header.des_task_id	= msg->if_des_task_id;

		app_if_msg.len = get_data_len_common_msg(msg);
		mem_cpy(app_if_msg.data, get_data_common_msg(msg), sizeof(uint8_t) * app_if_msg.len);

		tx_frame_post((uint8_t*)&app_if_msg, sizeof(ak_msg_common_if_t));
	}
		break;

	case SL_CPU_SERIAL_IF_DYNAMIC_MSG_OUT: {
		APP_DBG("[IF CPU SERIAL][SEND] DYNAMIC_MSG_TYPE\n");
		ak_msg_dynamic_if_t app_if_msg;

		/* assign if message */
		app_if_msg.header.type			= DYNAMIC_MSG_TYPE;
		app_if_msg.header.if_src_type	= msg->if_src_type;
		app_if_msg.header.if_des_type	= msg->if_des_type;
		app_if_msg.header.sig			= msg->if_sig;
		app_if_msg.header.src_task_id	= msg->if_src_task_id;
		app_if_msg.header.des_task_id	= msg->if_des_task_id;

		app_if_msg.len = get_data_len_dynamic_msg(msg);
		app_if_msg.data = (uint8_t*)malloc(app_if_msg.len);
		memcpy(app_if_msg.data, get_data_dynamic_msg(msg), app_if_msg.len);
		dync_msg_frame_post(&app_if_msg);
		free(app_if_msg.data);
	}
		break;

	case SL_CPU_SERIAL_IF_KEEP_ALIVE: {
		APP_DBG("[IF SL SERIAL] SL_CPU_SERIAL_IF_KEEP_ALIVE\n");
		cpu_serial_rx_frame_state = SOP_STATE;
		mem_set(&if_cpu_serial_frame, 0, sizeof(if_cpu_serial_frame_t));
	}
		break;

	default:
		break;
	}
}

void rx_frame_parser(uint8_t ch) {
	switch (cpu_serial_rx_frame_state) {
	case SOP_STATE: {
		if (IFCPU_SOP_CHAR == ch) {
			cpu_serial_rx_frame_state = LEN_STATE;
		}
	}
		break;

	case LEN_STATE: {
		if (ch >= IFCPU_DATA_SIZE) {
			cpu_serial_rx_frame_state = SOP_STATE;
			if_cpu_serial_frame.data_index = 0;
			if_cpu_serial_frame.len = 0;
			return;
		}
		else {
			if_cpu_serial_frame.len = ch;
			if_cpu_serial_frame.data_index = 0;
			cpu_serial_rx_frame_state = DATA_STATE;
		}
	}
		break;

	case DATA_STATE: {
		if_cpu_serial_frame.data[if_cpu_serial_frame.data_index++] = ch;

		if (if_cpu_serial_frame.data_index == if_cpu_serial_frame.len) {
			cpu_serial_rx_frame_state = FCS_STATE;
		}
	}
		break;

	case FCS_STATE: {
		cpu_serial_rx_frame_state = SOP_STATE;

		if_cpu_serial_frame.frame_fcs = ch;

		if (if_cpu_serial_frame.frame_fcs \
				== if_cpu_serial_calcfcs(if_cpu_serial_frame.len, if_cpu_serial_frame.data)) {			

			ak_msg_if_header_t* if_msg_header = (ak_msg_if_header_t*)if_cpu_serial_frame.data;
			APP_DBG("->type: %d\n", if_msg_header->type);
			APP_DBG("->src_task_id: %d\n", if_msg_header->src_task_id);
			APP_DBG("->des_task_id: %d\n", if_msg_header->des_task_id);
			APP_DBG("->if_src_type: %d\n", if_msg_header->if_src_type);
			APP_DBG("->if_des_type: %d\n", if_msg_header->if_des_type);
			APP_DBG("->sig: %d\n", if_msg_header->sig);

			switch (if_msg_header->type) {
			case PURE_MSG_TYPE: {
				APP_DBG("[IF CPU SERIAL][REV] PURE_MSG_TYPE\n");

				timer_set(SL_TASK_CPU_SERIAL_IF_ID, SL_CPU_SERIAL_IF_KEEP_ALIVE, SL_CPU_SERIAL_IF_KEEP_ALIVE_INTERVAL, TIMER_ONE_SHOT);

				ak_msg_t* s_msg = get_pure_msg();

				set_if_src_task_id(s_msg, if_msg_header->src_task_id);
				set_if_des_task_id(s_msg, if_msg_header->des_task_id);
				set_if_src_type(s_msg, IF_TYPE_CPU_SERIAL_MT);
				set_if_des_type(s_msg, if_msg_header->if_des_type);
				set_if_sig(s_msg, if_msg_header->sig);

				set_msg_sig(s_msg, SL_IF_PURE_MSG_IN);
				task_post(SL_TASK_IF_ID, s_msg);
			}
				break;

			case COMMON_MSG_TYPE: {
				APP_DBG("[IF CPU SERIAL][REV] COMMON_MSG_TYPE\n");

				timer_set(SL_TASK_CPU_SERIAL_IF_ID, SL_CPU_SERIAL_IF_KEEP_ALIVE, SL_CPU_SERIAL_IF_KEEP_ALIVE_INTERVAL, TIMER_ONE_SHOT);

				ak_msg_t* s_msg = get_common_msg();

				set_if_src_task_id(s_msg, if_msg_header->src_task_id);
				set_if_des_task_id(s_msg, if_msg_header->des_task_id);
				set_if_src_type(s_msg, IF_TYPE_CPU_SERIAL_MT);
				set_if_des_type(s_msg, if_msg_header->if_des_type);
				set_if_sig(s_msg, if_msg_header->sig);
				set_if_data_common_msg(s_msg, ((ak_msg_common_if_t*)if_msg_header)->data, ((ak_msg_common_if_t*)if_msg_header)->len);

				set_msg_sig(s_msg, SL_IF_COMMON_MSG_IN);
				task_post(SL_TASK_IF_ID, s_msg);
			}
				break;

			case DYNAMIC_MSG_TYPE: {
				APP_DBG("[IF CPU SERIAL][REV] DYNAMIC_MSG_TYPE\n");

				timer_set(SL_TASK_CPU_SERIAL_IF_ID, SL_CPU_SERIAL_IF_KEEP_ALIVE, SL_CPU_SERIAL_IF_KEEP_ALIVE_INTERVAL, TIMER_ONE_SHOT);

				ak_msg_t* s_msg = get_dynamic_msg();

				set_if_src_task_id(s_msg, if_msg_header->src_task_id);
				set_if_des_task_id(s_msg, if_msg_header->des_task_id);
				set_if_src_type(s_msg, if_msg_header->if_src_type);
				set_if_des_type(s_msg, if_msg_header->if_des_type);
				set_if_sig(s_msg, if_msg_header->sig);

				uint8_t* data_msg = ((uint8_t*)if_msg_header + sizeof(ak_msg_if_header_t) + sizeof(uint32_t));
				set_if_data_dynamic_msg(s_msg, data_msg, ((ak_msg_dynamic_if_t*)if_msg_header)->len);

				set_msg_sig(s_msg, SL_IF_DYNAMIC_MSG_IN);
				task_post(SL_TASK_IF_ID, s_msg);
			}

			default:
				break;
			}
		}
		else {
			/* TODO: handle checksum incorrect */
		}
	}
		break;

	default:
		break;
	}
}

void tx_frame_post(uint8_t* data, uint8_t len) {
	tx_buffer[0] = IFCPU_SOP_CHAR;
	tx_buffer[1] = len;
	mem_cpy(&tx_buffer[2], data, len);
	tx_buffer[2 + len] = if_cpu_serial_calcfcs(len, data);

	for (uint8_t i = 0; i < len + 3; i++) {
		io_uart_interface_transfer(tx_buffer[i]);
		xprintf("%d:", tx_buffer[i]);
	}
	xprintf("\n");
}

uint8_t if_cpu_serial_calcfcs(uint8_t len, uint8_t *data_ptr) {
	uint8_t xor_result = len;

	for (int i = 0; i < len; i++, data_ptr++) {
		xor_result = xor_result ^ *data_ptr;
	}

	return xor_result;
}

void dync_msg_frame_post(ak_msg_dynamic_if_t* ak_msg_dynamic) {
	uint32_t dynamic_message_len = sizeof(ak_msg_if_header_t) + sizeof(uint32_t) + ak_msg_dynamic->len;
	uint32_t dynamic_message_header_len = sizeof(ak_msg_if_header_t) + sizeof(uint32_t);

	if (dynamic_message_len > 255) {
		FATAL("IF_UART", 0x04);
	}

	/* calculate checksum */
	uint8_t xor_result = dynamic_message_len;
	uint8_t* raw_data = (uint8_t*)ak_msg_dynamic;

	/* checksum header + len */
	for (uint32_t i = 0; i < dynamic_message_header_len; i++, raw_data++) {
		xor_result = xor_result ^ *raw_data;
	}

	/* checksum data */
	for (uint32_t i = 0; i < ak_msg_dynamic->len; i++) {
		xor_result = xor_result ^ ak_msg_dynamic->data[i];
	}

	tx_buffer[0] = IFCPU_SOP_CHAR; /* start of frame */
	tx_buffer[1] = dynamic_message_len; /* len of frame */
	memcpy(&tx_buffer[2], ak_msg_dynamic, dynamic_message_header_len); /* copy header to tx frame */
	memcpy(&tx_buffer[2 + dynamic_message_header_len], ak_msg_dynamic->data, ak_msg_dynamic->len); /* copy message data */
	tx_buffer[2 + dynamic_message_len] = xor_result; /* checksum */

	/* put frame through physic layer */
	for (uint8_t i = 0; i < dynamic_message_len + 3; i++) {
		io_uart_interface_transfer(tx_buffer[i]);
	}
}
