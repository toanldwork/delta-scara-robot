# README #
This repository have 3 resource for Delta and Scara Robot:
- Delta Robot uses:
 + Master(stm32f1)
 + Slave(stm32f1)
- Scara Robot uses:
 + Master(stm32f1) 
 + Slave(stm32f1)
 + Slave_for_vitme(stm32f4) => used to lead screw axis
 
### What have this repository updated? ###

- 19/12/2020:
 + Porting from stm32f1 to stm32f4 for lead screw axis dc.
 + Update concept for Scara Robot
 + Modify PID feature
 

